package com.aTeam.beasy.Util

enum class ValidateStatus {
    VALID,
    INVALID
}
