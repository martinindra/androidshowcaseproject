package com.aTeam.beasy.Util

data class ValidateResult(val status: ValidateStatus,val id:Int?,val message: String?) {

    companion object {

        fun valid(): ValidateResult {
            return ValidateResult(ValidateStatus.VALID, null,"")
        }

        fun  invalid(id:Int,msg: String): ValidateResult {
            return ValidateResult(ValidateStatus.INVALID,id,msg)
        }

    }

}

