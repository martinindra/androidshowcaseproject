package com.aTeam.beasy.Resource.dagger

import com.aTeam.beasy.View.HomeFeature.Transfer.TransferConfirmationPageActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


//TODO create activity builder module
@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributesTransferConfirmationPageActivity() : TransferConfirmationPageActivity
}