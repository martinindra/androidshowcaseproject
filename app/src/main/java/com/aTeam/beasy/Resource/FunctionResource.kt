package com.aTeam.beasy.Resource

import java.text.NumberFormat
import java.util.*

class FunctionResource {
    fun rupiah(number: Double): String{
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        val result = numberFormat.format(number).toString()
        return result.replace("Rp", "IDR ")
    }

    fun rupiahInput(number: Double): String{
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        val result = numberFormat.format(number).toString()
        return result.replace("Rp", "")
    }

}