package com.aTeam.beasy.Resource.dagger

import com.aTeam.beasy.Resource.FunctionResource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {


    @Singleton
    @Provides
    fun provideFunctionResource() : FunctionResource{
        return FunctionResource()
    }
}