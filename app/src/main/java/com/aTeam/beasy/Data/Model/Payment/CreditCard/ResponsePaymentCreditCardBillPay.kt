package com.aTeam.beasy.Data.Model.Payment.CreditCard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePaymentCreditCardBillPay(

	@field:SerializedName("data")
	val data: DataResponsePaymentCreditCardPay? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataResponsePaymentCreditCardPay(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("credit_card_number")
	val creditCardNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("on")
	val on: String? = null
) : Parcelable
