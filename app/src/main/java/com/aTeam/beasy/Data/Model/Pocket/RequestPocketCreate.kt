package com.aTeam.beasy.Data.Model.Pocket

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestPocketCreate(
    val name: String? = null,
    val picture: String? = null,
    val target: String? = null
) : Parcelable