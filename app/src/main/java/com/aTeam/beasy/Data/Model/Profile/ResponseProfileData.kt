package com.aTeam.beasy.Data.Model.Profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseProfileData(

	@field:SerializedName("data")
	val data: DataProfileUser? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataProfileUser(

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("card_number")
	val cardNumber: String? = null,

	@field:SerializedName("profile_picture")
	val profilePicture: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
