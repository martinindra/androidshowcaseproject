package com.aTeam.beasy.Data.Repository.Payment

import android.util.Log
import com.aTeam.beasy.Data.Model.Payment.CreditCard.RequestPaymentCreditCardBillPay
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBill
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBillPay
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class PaymentCreditCardRepository {

    fun creditCardGetBill(
        tokenUser : String,
        ccNumber : String,
        responseHandler : (ResponsePaymentCreditCardBill) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getCreditBill(token = tokenUser, ccNumber = ccNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun creditCardPayBill(
        tokenUser: String,
        creditCardNumber : String,
        amount : Int,
        pin : Int,
        responseHandler: (ResponsePaymentCreditCardBillPay) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        val dataCreditCardPay = RequestPaymentCreditCardBillPay(amount = amount, creditCardNumber = creditCardNumber, pin = pin)
        ConfigNetwork.instance.postCreditBill(
            token = tokenUser,
            creditCardPayBill = dataCreditCardPay
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },{
                    responseError(it)
                }
            )
    }
}