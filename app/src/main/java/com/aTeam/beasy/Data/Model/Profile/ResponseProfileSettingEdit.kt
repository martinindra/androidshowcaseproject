package com.aTeam.beasy.Data.Model.Profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseProfileSettingEdit(

	@field:SerializedName("data")
	val data: DataResponseSettingEdit? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataResponseSettingEdit(

	@field:SerializedName("pin")
	val pin: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
