package com.aTeam.beasy.Data.Model.Gamification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseMissionGammification(

	@field:SerializedName("ResponseMissionGammification")
	val responseMissionGammification: List<ResponseMissionGammificationItem?>? = null
) : Parcelable

@Parcelize
data class DataMissionItem(

	@field:SerializedName("titleMission")
	val titleMission: String? = null,

	@field:SerializedName("isSuccessMission")
	val isSuccessMission: Boolean? = null,

	@field:SerializedName("idMission")
	val idMission: Int? = null
) : Parcelable

@Parcelize
data class ResponseMissionGammificationItem(

	@field:SerializedName("dataMission")
	val dataMission: List<DataMissionItem?>? = null,

	@field:SerializedName("rocketCode")
	val rocketCode: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Parcelable
