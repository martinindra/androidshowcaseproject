package com.aTeam.beasy.Data.Model.Ewallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestEwalletConfirmation(
	val message: String? = null,
	val amount: Int? = null,
	val pin: Int? = null,
	@SerializedName("account_id")
	val accountId: String? = null
) : Parcelable
