package com.aTeam.beasy.Data.Repository.Ewallet

import android.util.Log
import com.aTeam.beasy.Data.Model.Ewallet.*
import com.aTeam.beasy.Data.Model.Transfer.*
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class WalletContactRepository {

    //get contact all
    fun getAccount(
        tokenUser : String,
        responseHandler : (ResponseEwalletContact) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getAccount(tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    //get account recent
    fun getAccountRecent(
        tokenUser : String,
        responseHandlerRecent : (ResponseEwalletContact) -> Unit,
        errorHandlerRecent : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getRecentAccount(tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerRecent(it)
                },
                {
                    errorHandlerRecent(it)
                }
            )
    }

    //get account by id
    fun getAccountById(
        tokenUser : String,
        idUser : String,
        responseHandlerId : (ResponseEwalletContact) -> Unit,
        errorHandlerId : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getAccountById(token = tokenUser, id = idUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerId(it)
                },
                {
                    errorHandlerId(it)
                }
            )
    }

    //add contact
    fun addAccount(
        tokenUser : String,
        name : String,
        eWalletId : String,
        accountNumberWallet : String,
        responseAddHandler : (ResponseAddAccount) -> Unit,
        errorAddHandler : (Throwable) -> Unit
    )
    {
        val addAccount = RequestAddAccount(name = name, accountNumberWallet = accountNumberWallet, eWalletId = eWalletId)
        ConfigNetwork.instance.addAccount(tokenUser, addAccount)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseAddHandler(it)
                    Log.d("AccountLogin", it.data.toString())
                },
                {
                    errorAddHandler(it)
                }
            )
    }


    //edit account
    fun editAccount(
        tokenUser: String,
        idUser: String,
        name: String,
        eWalletId: String,
        accountNumberWallet: String,
        responseEditHandler : (ResponseEditAccount) -> Unit,
        errorEditHandler : (Throwable) -> Unit
    ){
        val editAccount = RequestEditAccount(name = name, accountNumberWallet = accountNumberWallet, eWalletId = eWalletId)

        ConfigNetwork.instance.editAccountID(
            token = tokenUser,
            id = idUser,
            editAccount = editAccount
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseEditHandler(it)
                },
                {
                    errorEditHandler(it)
                }
            )
    }

    //delete account
    fun deleteAccountById(
        tokenUser : String,
        idUser : String,
        responseHandlerId : (ResponseDeleteAccount) -> Unit,
        errorHandlerId : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.deleteAccountId(token = tokenUser, id = idUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerId(it)
                },
                {
                    errorHandlerId(it)
                }
            )
    }
}