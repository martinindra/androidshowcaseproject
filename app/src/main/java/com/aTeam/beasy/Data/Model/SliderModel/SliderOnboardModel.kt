package com.aTeam.beasy.Data.Model.SliderModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SliderOnboardModel(
    var title: String,
    var description: String,
    var image : Int
) : Parcelable
