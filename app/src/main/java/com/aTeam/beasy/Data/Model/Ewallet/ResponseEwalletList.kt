package com.aTeam.beasy.Data.Model.Ewallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseEwalletList(
    val data: List<DataItemEwallet>?,
    val success: Boolean?,
    val message: String
) : Parcelable

@Parcelize
data class DataItemEwallet(
    @SerializedName("id")
    val ewalletId: String?,

    @SerializedName("name")
    val ewalletName: String?
) : Parcelable
