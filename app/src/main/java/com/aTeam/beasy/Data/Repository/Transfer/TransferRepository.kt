package com.aTeam.beasy.Data.Repository.Transfer

import android.util.Log
import com.aTeam.beasy.Data.Model.Transfer.RequestTransferConfirmation
import com.aTeam.beasy.Data.Model.Transfer.ResponseTransferConfirmation
import com.aTeam.beasy.Network.ConfigNetwork
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException


class TransferRepository {

    fun transferToContact(
        tokenUser : String,
        contactId : String,
        amount : Int,
        note : String,
        pin : Int,
        responseAddHandler : (ResponseTransferConfirmation) -> Unit,
        errorAddHandler : (String) -> Unit
    )
    {
        val transferContact = RequestTransferConfirmation(note = note, amount = amount, pin = pin, contactId = contactId)
        ConfigNetwork.instance.transferToContact(tokenUser, transferContact)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseAddHandler(it)
                    Log.d("ContactLogin", it.data.toString())
                },
                {
                    if (it is HttpException){
                        val type = object : TypeToken<ResponseTransferConfirmation>() {}.type
                        var errorResponse: ResponseTransferConfirmation? = null
                        try {
                            it.response()?.errorBody()?.let {responseBody ->
                                errorResponse = Gson().fromJson(it.response()?.errorBody()?.charStream(), type)

                            }
                            errorAddHandler(
                                errorResponse?.message ?: "Server Error"
                            )

                        } catch (error: Exception) {
                            errorAddHandler(
                                error.message?:"Error"
                            )
                        }


                    }
                }
            )
    }
}