package com.aTeam.beasy.Data.Model.Ewallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestEditAccount(

	@field:SerializedName("account_number")
	val accountNumberWallet: String? = null,

	@field:SerializedName("ewallet_id")
	val eWalletId: String? = null,

	@field:SerializedName("name")
	val name: String? = null
) : Parcelable