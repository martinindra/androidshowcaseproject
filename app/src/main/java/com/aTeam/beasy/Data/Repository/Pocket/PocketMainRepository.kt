package com.aTeam.beasy.Data.Repository.Pocket

import android.graphics.Bitmap
import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Data.Model.Pocket.*
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Headers
import java.io.File

class PocketMainRepository {

    fun pocketMainList(
        tokenUser : String,
        with_primary : Boolean,
        responseHandler : (ResponsePocketList) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getPocketList(token = tokenUser, with_primary)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun pocketById(
        tokenUser : String,
        pocketId : String,
        responseHandler : (ResponsePocketById) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getPocketById(token = tokenUser, pocketId = pocketId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun pocketByIdHistory(
        tokenUser : String,
        pocketId : String,
        responseHandler : (ResponsePocketHistory) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getPocketByIdHistory(token = tokenUser, pocketId = pocketId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }
    fun pocketByIdHistoryFilter(
        tokenUser : String,
        pocketId : String,
        sort : String,
        type : String,
        responseHandler : (ResponsePocketHistory) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getPocketFilter(token = tokenUser, pocketId = pocketId, sort_order = sort, type = type)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun pocketTopUp(
        tokenUser : String,
        pocketId : String,
        amount : Int,
        responseHandler : (ResponsePocketTopUp) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        val amounts = RequestPocketTopUp(amount = amount)
        ConfigNetwork.instance.postPocketTopUpById(token = tokenUser, pocketId = pocketId, requestPocketTopUp = amounts)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun pocketMove(
        tokenUser : String,
        pocketId : String,
        destination : String,
        amount : Int,
        responseHandler : (ResponsePocketMove) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        val move = RequestPocketMove(amount = amount, destination = destination)
        ConfigNetwork.instance.postPocketMoveById(token = tokenUser, pocketId = pocketId, requestPocketMove = move)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun pocketEdit(
        tokenUser: String,
        pocketId: String,
        dueDate: String,
        namee: String,
        target: Int,
        responsePocketEdit: (ResponsePocketEdit) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        val reqDueDate : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), dueDate)
        val reqName : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), namee)
        val reqTarget : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), target.toString())

        ConfigNetwork.instance.pocketEdit(
            token = tokenUser, pocketId = pocketId, dueDate = reqDueDate, name = reqName, target = reqTarget
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responsePocketEdit(it)
                },{
                    responseError(it)
                }
            )
    }

    fun pocketCreate(
        tokenUser: String,
        dueDate : String,
        namee : String,
        picture : File,
        target : Int,
        responseHandler : (ResponsePocketCreate) -> Unit,
        responseError : (Throwable) -> Unit
    ){
        val reqFile : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), picture)
        val bodyFile : MultipartBody.Part = MultipartBody.Part.createFormData("picture", picture.name, reqFile)

        val reqDueDate : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), dueDate)
        val reqName : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), namee)
        val reqTarget : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), target.toString())
//        val reqTarget : RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), target)
        ConfigNetwork.instance.pocketCreate(
            token = tokenUser,
            dueDate = reqDueDate,
            name = reqName,
            picture = bodyFile,
            target = reqTarget
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },{
                    responseError(it)
                }
            )
    }

    fun pocketDelete(
        tokenUser: String,
        pocketId: String,
        responseHandler: (ResponsePocketDelete) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.pocketDelete(
            token = tokenUser,
            pocketId = pocketId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    responseError(it)
                }
            )
    }
}