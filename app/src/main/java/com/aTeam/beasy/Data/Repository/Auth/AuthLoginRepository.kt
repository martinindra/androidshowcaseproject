package com.aTeam.beasy.Data.Repository.Auth

import android.util.Log
import com.aTeam.beasy.Data.Model.Auth.RequestAuthLogin
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthLoginSuccess
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import org.json.JSONObject

class AuthLoginRepository {

    fun login(
              email : String,
              password : String,
              responseHandler : (ResponseAuthLoginSuccess) -> Unit,
              errorHandler : (Throwable) -> Unit
    ){
        val login = RequestAuthLogin(email, password)
        ConfigNetwork.instance.login(login)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                    Log.d("ContactLogin", it.data.toString())
                },
                {
                    errorHandler(it)

                }
            )
    }
}