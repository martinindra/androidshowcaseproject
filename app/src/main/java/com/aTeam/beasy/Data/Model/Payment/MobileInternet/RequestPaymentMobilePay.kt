package com.aTeam.beasy.Data.Model.Payment.MobileInternet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestPaymentMobilePay(

	@field:SerializedName("pin")
	val pin: Int? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Parcelable
