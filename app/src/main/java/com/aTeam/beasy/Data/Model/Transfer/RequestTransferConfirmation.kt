package com.aTeam.beasy.Data.Model.Transfer

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestTransferConfirmation(
	val note: String? = null,
	val amount: Int? = null,
	val pin: Int? = null,

	@SerializedName("contact_id")
	val contactId: String? = null
) : Parcelable
