package com.aTeam.beasy.Data.Model.User

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseUserBalance(

    @field:SerializedName("data")
    val data: DataItemUserBalance? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
) : Parcelable

@Parcelize
data class DataItemUserBalance(
    @field:SerializedName("balance")
    val balance: Int? = null,

    @field:SerializedName("user_id")
    val userId: String? = null
) : Parcelable