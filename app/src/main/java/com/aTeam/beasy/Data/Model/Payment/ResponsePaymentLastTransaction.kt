package com.aTeam.beasy.Data.Model.Payment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePaymentLastTransaction(

	@field:SerializedName("data")
	val data: List<DataItemLastTransaction?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataItemLastTransaction(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("transaction_type")
	val transactionType: String? = null,

	@field:SerializedName("on")
	val on: String? = null
) : Parcelable
