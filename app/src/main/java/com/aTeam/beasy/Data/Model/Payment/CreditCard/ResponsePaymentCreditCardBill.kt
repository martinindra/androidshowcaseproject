package com.aTeam.beasy.Data.Model.Payment.CreditCard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePaymentCreditCardBill(

	@field:SerializedName("data")
	val data: DataCreditCardBill? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataCreditCardBill(

	@field:SerializedName("bank")
	val bank: String? = null,

	@field:SerializedName("credit_card_number")
	val creditCardNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("minimum_payment")
	val minimumPayment: Int? = null,

	@field:SerializedName("bill_payment")
	val billPayment: Int? = null,

	@field:SerializedName("on")
	val on: String? = null
) : Parcelable
