package com.aTeam.beasy.Data.Model.Transfer

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestEditContact(

	@field:SerializedName("account_number")
	val accountNumber: String? = null,

	@field:SerializedName("bank_id")
	val bankId: String? = null,

	@field:SerializedName("name")
	val name: String? = null
) : Parcelable