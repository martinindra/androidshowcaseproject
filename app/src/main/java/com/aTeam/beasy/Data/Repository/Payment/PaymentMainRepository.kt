package com.aTeam.beasy.Data.Repository.Payment

import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class PaymentMainRepository {

    fun paymentLastTransaction(
        tokenUser : String,
        responseHandler : (ResponsePaymentLastTransaction) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getPaymentLastTransaction(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }
}