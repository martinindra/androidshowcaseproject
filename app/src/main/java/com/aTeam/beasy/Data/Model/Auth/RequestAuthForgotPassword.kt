package com.aTeam.beasy.Data.Model.Auth

import com.google.gson.annotations.SerializedName

data class RequestAuthForgotPassword(
    @SerializedName("email") var email : String,
    @SerializedName("accountNumber") var accountNumber : String
)
