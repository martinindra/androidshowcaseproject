package com.aTeam.beasy.Data.Model.Payment.CreditCard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestPaymentCreditCardBillPay(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("credit_card_number")
	val creditCardNumber: String? = null,

	@field:SerializedName("pin")
	val pin: Int? = null
) : Parcelable
