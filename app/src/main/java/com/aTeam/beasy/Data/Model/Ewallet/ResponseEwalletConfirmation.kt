package com.aTeam.beasy.Data.Model.Ewallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseEwalletConfirmation(
	val data: DataResponseEwallet? = null,
	val success: Boolean? = null,
	val message: String? = null
) : Parcelable

@Parcelize
data class DataResponseEwallet(
	val amount: Int? = null,

	@SerializedName("account_name")
	val accountName: String? = null,

	@SerializedName("total_transfer")
	val totalTransfer: Int? = null,

	@SerializedName("beneficiary_account_number")
	val beneficiaryAccountNumber: String? = null,

	val message: String? = null,

	val status: String? = null,

	val on: String? = null,

	@SerializedName("account_acc_name")
	val accountAccName : String? = null,

	@SerializedName("refCode")
	val refCode : String? = null
) : Parcelable
