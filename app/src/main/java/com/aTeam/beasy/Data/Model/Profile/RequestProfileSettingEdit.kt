package com.aTeam.beasy.Data.Model.Profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestProfileSettingEdit(

	@field:SerializedName("pin")
	val pin: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
