package com.aTeam.beasy.Data.Repository.Ewallet

import android.util.Log
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletList
import com.aTeam.beasy.Network.ConfigNetwork
import retrofit2.Call
import retrofit2.Response

class WalletListRepository {
    fun getWalletList(
        tokenUser : String,
        responseHandler : (ResponseEwalletList) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getEwallet(tokenUser).enqueue(object : retrofit2.Callback<ResponseEwalletList> {
            override fun onResponse(
                call: Call<ResponseEwalletList>,
                response: Response<ResponseEwalletList>
            ) {
                if (response.isSuccessful){
                    responseHandler(response.body()!!)
                    Log.d("WalletAccountRepository", response.body().toString())
                }
            }

            override fun onFailure(call: Call<ResponseEwalletList>, t: Throwable) {
                errorHandler(t)
            }
        })
    }
}