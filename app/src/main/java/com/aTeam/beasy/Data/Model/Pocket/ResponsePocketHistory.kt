package com.aTeam.beasy.Data.Model.Pocket

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePocketHistory(

	@field:SerializedName("data")
	val data: List<DataPocketHistory?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataPocketHistory(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("pocketBalanceStatus")
	val pocketBalanceStatus: String? = null,

	@field:SerializedName("pocketTransactionType")
	val pocketTransactionType: String? = null
) : Parcelable
