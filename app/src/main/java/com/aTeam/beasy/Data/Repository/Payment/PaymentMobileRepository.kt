package com.aTeam.beasy.Data.Repository.Payment

import android.util.Log
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.*
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class PaymentMobileRepository {

    fun paymentMobileAirtimeReload(
        tokenUser : String,
        phoneNumber : String,
        responseHandler : (ResponsePaymentMobileCreditAirtimeReload) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getAirtimeReload(token = tokenUser, phoneNumber = phoneNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun paymentMobileInternetData(
        tokenUser: String,
        phoneNumber: String,
        responseHandler: (ResponsePaymentMobileInternetData) -> Unit,
        errorHandler: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getInternetData(token = tokenUser, phoneNumber = phoneNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },{
                    errorHandler(it)
                }
            )
    }

    fun postMobilePay(
        tokenUser: String,
        phoneNumber: String,
        id : String,
        pin : Int,
        responseHandler: (ResponsePaymentMobilePay) -> Unit,
        errorHandler: (Throwable) -> Unit
    ){
        val mobilePay = RequestPaymentMobilePay(pin = pin, phoneNumber = phoneNumber, id = id)
        ConfigNetwork.instance.postMobilePay(token = tokenUser, mobilePay = mobilePay)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },{
                    errorHandler(it)
                }
            )
    }
}