package com.aTeam.beasy.Data.Model.Payment.MobileInternet

data class ResponsePaymentMobilePay(
	val data: Data? = null,
	val success: Boolean? = null,
	val message: String? = null
)

data class Data(
	val amount: Int? = null,
	val name: String? = null,
	val description: String? = null,
	val phoneNumber: String? = null,
	val id: String? = null,
	val on: String? = null
)

