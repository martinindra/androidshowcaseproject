package com.aTeam.beasy.Data.Model.Gamification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGamificationPlanetListStatus(

	@field:SerializedName("data")
	val data: List<PlanetStatusDataItem?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class PlanetStatusDataItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("sequence")
	val sequence: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("reward_id")
	val rewardId: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("wording")
	val wording: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
