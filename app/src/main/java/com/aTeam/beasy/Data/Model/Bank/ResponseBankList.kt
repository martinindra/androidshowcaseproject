package com.aTeam.beasy.Data.Model.Bank

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseBankList(
	val data: List<DataItemBank>? ,
	val success: Boolean?,
	val message: String
) : Parcelable

@Parcelize
data class DataItemBank(
	@SerializedName("bank_id")
	val bankId: String?,

	@SerializedName("bank_name")
	val bankName: String?
) : Parcelable
