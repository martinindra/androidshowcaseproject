package com.aTeam.beasy.Data.Model.Pocket

import com.google.gson.annotations.SerializedName

data class ResponsePocketCreate(

	@field:SerializedName("data")
	val data: DataItemPocketCreate? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataItemPocketCreate(

	@field:SerializedName("balance")
	val balance: Int? = null,

	@field:SerializedName("pocket_name")
	val pocketName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("dueDate")
	val dueDate: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("delete")
	val delete: Boolean? = null,

	@field:SerializedName("picture")
	val picture: String? = null,

	@field:SerializedName("target")
	val target: Int? = null,

	@field:SerializedName("primary")
	val primary: Boolean? = null
)
