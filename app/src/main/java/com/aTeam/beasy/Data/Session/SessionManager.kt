package com.aTeam.beasy.Data.Session

import android.content.Context
import android.content.SharedPreferences

class SessionManager (var context: Context) {

    lateinit var pref : SharedPreferences
    lateinit var editor : SharedPreferences.Editor
    var SESSION_NAME = "BEASYSESSION"

    var TOKENLOGIN = "logintoken"
    var USERID = "logintoken"
    var ISGETSTARTED = "isgetstarted"

    init {
        pref = context.getSharedPreferences(SESSION_NAME, 0)
        editor = pref.edit()
    }

    var login : String?
        get() = pref.getString(TOKENLOGIN, "logintoken")
        set(login) {
            editor.putString(TOKENLOGIN, login)
            editor.commit()
        }

    var userId : String?
        get() = pref.getString(USERID, "USERID")
        set(userId) {
            editor.putString(USERID, userId)
            editor.commit()
        }

    var getStarted : Boolean?
        get() = pref.getBoolean(ISGETSTARTED, false)
        set(getStarted) {
            editor.putBoolean(ISGETSTARTED, true)
            editor.commit()
        }

    fun deleteDataToken(){
        login = ""
        editor.remove(login)
        editor.commit()
    }
}