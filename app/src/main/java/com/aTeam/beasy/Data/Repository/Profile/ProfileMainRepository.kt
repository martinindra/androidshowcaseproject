package com.aTeam.beasy.Data.Repository.Profile

import com.aTeam.beasy.Data.Model.Profile.RequestProfileSettingEdit
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileData
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSetting
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSettingEdit
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class ProfileMainRepository {

    fun getUserProfile(
        tokenUser : String,
        responseHandler : (ResponseProfileData) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getProfileData(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun getUserSetting(
        tokenUser : String,
        responseHandler : (ResponseProfileSetting) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getProfileSettingData(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    fun putUserSetting(
        email : String,
        pin : Int,
        tokenUser: String,
        responseHandler: (ResponseProfileSettingEdit) -> Unit,
        errorHandler: (Throwable) -> Unit
    ){
        val requestProfileSettingEdit = RequestProfileSettingEdit(pin = pin, email = email)
        ConfigNetwork.instance.putProfileSettingData(token = tokenUser, requestProfileSettingEdit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

}