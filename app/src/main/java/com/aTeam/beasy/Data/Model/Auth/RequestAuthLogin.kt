package com.aTeam.beasy.Data.Model.Auth

import com.google.gson.annotations.SerializedName

data class RequestAuthLogin(
    @SerializedName("email") var email : String,
    @SerializedName("password") var password : String
)
