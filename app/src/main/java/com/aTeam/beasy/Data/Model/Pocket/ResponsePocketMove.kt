package com.aTeam.beasy.Data.Model.Pocket

import com.google.gson.annotations.SerializedName

data class ResponsePocketMove(

	@field:SerializedName("data")
	val data: DataResponseMove? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataResponseMove(

	@field:SerializedName("pocket_destination_balance")
	val pocketDestinationBalance: Int? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("pocket_source_name")
	val pocketSourceName: String? = null,

	@field:SerializedName("pocket_source_balance")
	val pocketSourceBalance: Int? = null,

	@field:SerializedName("pocket_destination_name")
	val pocketDestinationName: String? = null,

	@field:SerializedName("pocket_destination_id")
	val pocketDestinationId: String? = null,

	@field:SerializedName("pocket_source_id")
	val pocketSourceId: String? = null
)
