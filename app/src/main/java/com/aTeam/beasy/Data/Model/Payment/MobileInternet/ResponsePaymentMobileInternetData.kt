package com.aTeam.beasy.Data.Model.Payment.MobileInternet

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePaymentMobileInternetData(
	val data: List<DataItemInternetData?>? = null,
	val success: Boolean? = null,
	val message: String? = null
) : Parcelable

@Parcelize
data class DataItemInternetData(
	val price: Int? = null,
	val name: String? = null,
	val description: String? = null,
	val id: String? = null
) : Parcelable
