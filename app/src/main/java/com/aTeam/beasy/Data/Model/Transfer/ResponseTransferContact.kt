package com.aTeam.beasy.Data.Model.Transfer

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseTransferContact(

	@field:SerializedName("data")
	val data: List<DataItemContact?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataItemContact(

	@field:SerializedName("account_number")
	val accountNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("cost")
	val cost: String? = null,

	@field:SerializedName("bank_id")
	val bankId: String? = null,

	@field:SerializedName("bank_name")
	val bankName: String? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Parcelable
