package com.aTeam.beasy.Data.Model.Gamification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGamificationStatusUser(

	@field:SerializedName("data")
	val data: DataGamificationItemUserStatus? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataGamificationItemUserStatus(

	@field:SerializedName("next_planet_wording")
	val nextPlanetWording: String? = null,

	@field:SerializedName("planet_wording")
	val planetWording: String? = null,

	@field:SerializedName("planet_id")
	val planetId: String? = null,

	@field:SerializedName("recent_planet_completion_delay_finished")
	val recentPlanetCompletionDelayFinished: String? = null,

	@field:SerializedName("planet_sequence")
	val planetSequence: Int? = null,

	@field:SerializedName("next_planet_name")
	val nextPlanetName: String? = null,

	@field:SerializedName("is_completed_gamification")
	val isCompletedGamification: Boolean? = null,

	@field:SerializedName("gamification_animation")
	val gamificationAnimation: String? = null,

	@field:SerializedName("is_on_completion_delay")
	val isOnCompletionDelay: Boolean? = null,

	@field:SerializedName("is_on_last_planet")
	val isOnLastPlanet: Boolean? = null,

	@field:SerializedName("next_planet_image")
	val nextPlanetImage: String? = null,

	@field:SerializedName("is_started_gamification")
	val isStartedGamification: Boolean? = null,

	@field:SerializedName("next_planet_id")
	val nextPlanetId: String? = null,

	@field:SerializedName("planet_image")
	val planetImage: String? = null,

	@field:SerializedName("next_planet_sequence")
	val nextPlanetSequence: Int? = null,

	@field:SerializedName("planet_name")
	val planetName: String? = null
) : Parcelable
