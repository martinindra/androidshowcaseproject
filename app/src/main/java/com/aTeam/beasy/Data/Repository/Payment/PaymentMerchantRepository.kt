package com.aTeam.beasy.Data.Repository.Payment

import com.aTeam.beasy.Data.Model.Payment.Merchant.RequestMerchantPayBill
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponseMerchantPaybill
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponsePaymentMerchantList
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class PaymentMerchantRepository {

    fun paymentMerchantList(
        tokenUser : String,
        responsemerchantList : (ResponsePaymentMerchantList) -> Unit,
        responseError : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getMerchantList(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responsemerchantList(it)
                },{
                    responseError(it)
                }
            )
    }

    fun paymentMerchantPayBill(
        tokenUser: String,
        id : String,
        pin : Int,
        amount : Int,
        responseMerchantPaybill : (ResponseMerchantPaybill) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        val payBill = RequestMerchantPayBill(amount = amount, pin = pin, id = id)
        ConfigNetwork.instance.postMerchantBill(token = tokenUser, merchantPayBill = payBill)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseMerchantPaybill(it)
                },{
                    responseError(it)
                }
            )
    }

}