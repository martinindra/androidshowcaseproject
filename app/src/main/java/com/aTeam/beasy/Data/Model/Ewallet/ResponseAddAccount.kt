package com.aTeam.beasy.Data.Model.Ewallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseAddAccount(

	@field:SerializedName("data")
	val data: DataAccountEWallet? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataAccountEWallet(

	@field:SerializedName("account_acc_number")
	val accountAccNumber: String? = null,

	@field:SerializedName("admin_fee")
	val adminFee: String? = null,

	@field:SerializedName("ewallet_id")
	val eWalletId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("account_name")
	val accountName: String? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Parcelable
