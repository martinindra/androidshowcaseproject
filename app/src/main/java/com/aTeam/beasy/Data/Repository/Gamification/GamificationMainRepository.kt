package com.aTeam.beasy.Data.Repository.Gamification

import com.aTeam.beasy.Data.Model.Gamification.*
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponsePaymentMerchantList
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class GamificationMainRepository {

    fun gamificationUserMission(
        tokenUser : String,
        responseGamificationStatusUser : (ResponseGamificationStatusUser) -> Unit,
        responseError : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getGamificationUserStatus(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationStatusUser(it)
                },{
                    responseError(it)
                }
            )
    }

    fun gamificationUserStart(
        tokenUser: String,
        responseGamificationStart : (ResponseGamificationStart) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.postGamificationStart(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationStart(it)
                },
                {
                    responseError(it)
                }
            )
    }

    fun gamificationDetailPlanet(
        tokenUser : String,
        idPlanet : String,
        responseGamificationPlanetDetail : (ResponseGamificationDetailPlanet) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getGamificationPlanetDetailWithid(
            token = tokenUser,
            planetId = idPlanet
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationPlanetDetail(it)
                },
                {
                    responseError(it)
                }
            )
    }

    fun gamificationDetailRewardPlanet(
        tokenUser: String,
        idReward: String,
        responseGamificationRewardPlanetDetail: (ResponseGamificationDetailRewardPlanet) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getGamificationDetailRewardPlanet(
            token = tokenUser,
            rewardId = idReward
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationRewardPlanetDetail(it)
                },
                {
                    responseError(it)
                }
            )
    }

    fun gamificationGetListPlanet(
        tokenUser: String,
        queryStatus : String,
        responseGamificationPlanetListStatus: (ResponseGamificationPlanetListStatus) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getGamificationPlanetListStatus(
            token = tokenUser,
            status = queryStatus
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationPlanetListStatus(it)
                },
                {
                    responseError(it)
                }
            )
    }


    fun gamificationPostClaimReward(
        tokenUser: String,
        rewardId : String,
        responseGamificationClaimReward : (ResponseGamificationClaimReward) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.postGamificationClaimReward(
            token = tokenUser,
            rewardId = rewardId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseGamificationClaimReward(it)
                },{
                    responseError(it)
                }
            )
    }

    fun gamificationMyRewardList(
        tokenUser: String,
        responseGamificationMyRewardList: (ResponseGamificationMyRewardList) -> Unit,
        responseError: (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getGamificationMyRewardList(
            token = tokenUser
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    responseGamificationMyRewardList(response)
                },{ error ->
                    responseError(error)
                }
            )
    }
}