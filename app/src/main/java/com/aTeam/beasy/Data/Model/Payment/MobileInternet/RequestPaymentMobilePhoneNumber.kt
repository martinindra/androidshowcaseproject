package com.aTeam.beasy.Data.Model.Payment.MobileInternet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestPaymentMobilePhoneNumber(
	@field:SerializedName("phone_number")
	val phoneNumber: String? = null
) : Parcelable
