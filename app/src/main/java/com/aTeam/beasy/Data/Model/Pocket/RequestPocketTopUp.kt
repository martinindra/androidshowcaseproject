package com.aTeam.beasy.Data.Model.Pocket

import com.google.gson.annotations.SerializedName

data class RequestPocketTopUp(

	@field:SerializedName("amount")
	val amount: Int? = null
)
