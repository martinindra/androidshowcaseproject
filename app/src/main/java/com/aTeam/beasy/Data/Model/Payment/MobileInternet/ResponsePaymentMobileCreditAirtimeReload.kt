package com.aTeam.beasy.Data.Model.Payment.MobileInternet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePaymentMobileCreditAirtimeReload(

	@field:SerializedName("data")
	val data: List<DataItemAirtimeReload?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataItemAirtimeReload(

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("denom")
	val denom: String? = null
) : Parcelable
