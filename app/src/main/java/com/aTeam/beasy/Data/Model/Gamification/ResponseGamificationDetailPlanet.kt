package com.aTeam.beasy.Data.Model.Gamification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGamificationDetailPlanet(

	@field:SerializedName("data")
	val data: DataGamificationDetailPlanet? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataGamificationDetailPlanet(

	@field:SerializedName("sequence")
	val sequence: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("storytelling")
	val storytelling: String? = null,

	@field:SerializedName("mission")
	val mission: List<MissionItem?>? = null,

	@field:SerializedName("rewardId")
	val rewardId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("wording")
	val wording: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class MissionItem(

	@field:SerializedName("missionType")
	val missionType: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("passed")
	val passed: Boolean? = null,

	@field:SerializedName("wording")
	val wording: String? = null
) : Parcelable
