package com.aTeam.beasy.Data.Model.Transfer

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseTransferConfirmation(
	val data: DataResponseTransfer? = null,
	val success: Boolean? = null,
	val message: String? = null
) : Parcelable

@Parcelize
data class DataResponseTransfer(
	val amount: Int? = null,

	@SerializedName("bank_name")
	val bankName: String? = null,

	@SerializedName("total_transfer")
	val totalTransfer: Int? = null,

	@SerializedName("beneficiary_account_number")
	val beneficiaryAccountNumber: String? = null,

	val message: String? = null,

	val status: String? = null,

	val on: String? = null,

	@SerializedName("account_name")
	val accountName : String? = null,

	@SerializedName("refCode")
	val refCode : String? = null
) : Parcelable
