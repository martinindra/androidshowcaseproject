package com.aTeam.beasy.Data.Model.Payment.Merchant

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestMerchantPayBill(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("pin")
	val pin: Int? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Parcelable
