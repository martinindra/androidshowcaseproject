package com.aTeam.beasy.Data.Repository.Bank

import android.util.Log
import com.aTeam.beasy.Data.Model.Bank.ResponseBankList
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class BankListRepository {
    fun getBankList(
        tokenUser : String,
        responseHandler : (ResponseBankList) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getBank(tokenUser).enqueue(object : retrofit2.Callback<ResponseBankList>{
            override fun onResponse(
                call: Call<ResponseBankList>,
                response: Response<ResponseBankList>
            ) {
                if (response.isSuccessful){
                    responseHandler(response.body()!!)
                    Log.d("BankContactRepository", response.body().toString())
                }
            }

            override fun onFailure(call: Call<ResponseBankList>, t: Throwable) {
                errorHandler(t)
            }
        })
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                {
//                    responseHandler(it)
//                    Log.d("BankContactRepository", it.toString())
//                },
//                {
//                    errorHandler(it)
//                }
//            )
    }
}