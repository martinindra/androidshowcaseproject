package com.aTeam.beasy.Data.Repository.Auth

import com.aTeam.beasy.Data.Model.Auth.RequestAuthForgotPassword
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthForgotPassword
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class ForgotPasswordRepository {

    fun forgotPassword(
        email : String,
        accountNumber : String,
        responseHandler : (ResponseAuthForgotPassword) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        val forgetPassword = RequestAuthForgotPassword(email, accountNumber)

        ConfigNetwork.instance.forgotPassword(forgetPassword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                }
                ,
                {
                    errorHandler(it)
                }
            )
    }
}