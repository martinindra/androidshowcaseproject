package com.aTeam.beasy.Data.Repository.User

import com.aTeam.beasy.Data.Model.User.ResponseUserBalance
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class UserMainRepository {

    fun getUserBalance(
        tokenUser : String,
        responseHandler : (ResponseUserBalance) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getUserBalance(token = tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }
}