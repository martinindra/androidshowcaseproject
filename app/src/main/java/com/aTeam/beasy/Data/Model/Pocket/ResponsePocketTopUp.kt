package com.aTeam.beasy.Data.Model.Pocket

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponsePocketTopUp(

	@field:SerializedName("data")
	val data: DataPocketTopUp? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataPocketTopUp(

	@field:SerializedName("pocket_destination_balance")
	val pocketDestinationBalance: Int? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("pocket_destination_name")
	val pocketDestinationName: String? = null,

	@field:SerializedName("pocket_destination_id")
	val pocketDestinationId: String? = null
) : Parcelable
