package com.aTeam.beasy.Data.Model.Pocket

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestPocketMove(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("destination")
	val destination: String? = null
) : Parcelable
