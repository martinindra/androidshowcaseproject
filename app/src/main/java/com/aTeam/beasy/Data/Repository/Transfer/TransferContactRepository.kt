package com.aTeam.beasy.Data.Repository.Transfer

import android.util.Log
import com.aTeam.beasy.Data.Model.Auth.RequestAuthLogin
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthLoginSuccess
import com.aTeam.beasy.Data.Model.Transfer.*
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.Network.ConfigNetwork
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class TransferContactRepository {

    //get contact all
    fun getContact(
        tokenUser : String,
        responseHandler : (ResponseTransferContact) -> Unit,
        errorHandler : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getContact(tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandler(it)
                },
                {
                    errorHandler(it)
                }
            )
    }

    //get contact recent
    fun getContactRecent(
        tokenUser : String,
        responseHandlerRecent : (ResponseTransferContact) -> Unit,
        errorHandlerRecent : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getRecentContact(tokenUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerRecent(it)
                },
                {
                    errorHandlerRecent(it)
                }
            )
    }

    //get contact by id
    fun getContactById(
        tokenUser : String,
        idUser : String,
        responseHandlerId : (ResponseTransferContact) -> Unit,
        errorHandlerId : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.getContactById(token = tokenUser, id = idUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerId(it)
                },
                {
                    errorHandlerId(it)
                }
            )
    }


    //add contact
    fun addContact(
        tokenUser : String,
        name : String,
        bankid : String,
        accountNumber : String,
        responseAddHandler : (ResponseAddContact) -> Unit,
        errorAddHandler : (Throwable) -> Unit
    )
    {
        val addContact = RequestAddContact(name = name, accountNumber = accountNumber, bankId = bankid)
        ConfigNetwork.instance.addContact(tokenUser, addContact)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseAddHandler(it)
                    Log.d("ContactLogin", it.data.toString())
                },
                {
                    errorAddHandler(it)
                }
            )
    }


    //edit contact
    fun editContact(
        tokenUser: String,
        idUser: String,
        name: String,
        bankid: String,
        accountNumber: String,
        responseEditHandler : (ResponseEditContact) -> Unit,
        errorEditHandler : (Throwable) -> Unit
    ){
        val editContact = RequestEditContact(
            name = name,
            bankId = bankid,
            accountNumber = accountNumber)

        ConfigNetwork.instance.editContactID(
            token = tokenUser,
            id = idUser,
            editContact = editContact
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseEditHandler(it)
                },
                {
                    errorEditHandler(it)
                }
            )
    }

    //delete contact
    fun deleteContactById(
        tokenUser : String,
        idUser : String,
        responseHandlerId : (ResponseDeleteContact) -> Unit,
        errorHandlerId : (Throwable) -> Unit
    ){
        ConfigNetwork.instance.deleteContactId(token = tokenUser, id = idUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseHandlerId(it)
                },
                {
                    errorHandlerId(it)
                }
            )
    }
}