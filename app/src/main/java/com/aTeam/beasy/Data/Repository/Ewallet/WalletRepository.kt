package com.aTeam.beasy.Data.Repository.Ewallet

import android.util.Log
import com.aTeam.beasy.Data.Model.Ewallet.RequestEwalletConfirmation
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletConfirmation
import com.aTeam.beasy.Network.ConfigNetwork
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.HttpException

class WalletRepository {

    fun walletToAccount(
        tokenUser: String,
        accountId: String,
        amount: Int,
        message: String,
        pin: Int,
        responseAddHandler: (ResponseEwalletConfirmation) -> Unit,
        errorAddHandler: (String) -> Unit
    ) {
        val walletAccount = RequestEwalletConfirmation(
            message = message,
            amount = amount,
            pin = pin,
            accountId = accountId
        )
        ConfigNetwork.instance.walletToAccount(tokenUser, walletAccount)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    responseAddHandler(it)
                    Log.d("AccountLogin", it.data.toString())
                },
                {
                    if (it is HttpException) {
                        val type = object : TypeToken<ResponseEwalletConfirmation>() {}.type
                        var errorResponse: ResponseEwalletConfirmation? = null
                        try {
                            it.response()?.errorBody()?.let { responseBody ->
                                errorResponse =
                                    Gson().fromJson(it.response()?.errorBody()?.charStream(), type)
                            }
                            errorAddHandler(
                                errorResponse?.message ?: "Error on Server!"
                            )
                        } catch (error: Exception) {
                            errorAddHandler(
                                error.message ?: "Error!"
                            )
                        }
                    }
                }
            )
    }
}