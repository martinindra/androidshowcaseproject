package com.aTeam.beasy.Data.Model.Gamification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGamificationDetailRewardPlanet(

	@field:SerializedName("data")
	val data: DataDetailRewardPlanet? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class DataDetailRewardPlanet(

	@field:SerializedName("tnc")
	val tnc: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("wording")
	val wording: String? = null,

	@field:SerializedName("is_claimed")
	val isClaimed: Boolean? = null
) : Parcelable
