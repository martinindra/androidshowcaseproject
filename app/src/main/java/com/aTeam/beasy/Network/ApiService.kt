package com.aTeam.beasy.Network

import com.aTeam.beasy.Data.Model.Auth.RequestAuthForgotPassword
import com.aTeam.beasy.Data.Model.Auth.RequestAuthLogin
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthForgotPassword
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthLoginSuccess
import com.aTeam.beasy.Data.Model.Bank.ResponseBankList
import com.aTeam.beasy.Data.Model.Ewallet.*
import com.aTeam.beasy.Data.Model.Gamification.*
import com.aTeam.beasy.Data.Model.Payment.CreditCard.RequestPaymentCreditCardBillPay
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBill
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBillPay
import com.aTeam.beasy.Data.Model.Payment.Merchant.RequestMerchantPayBill
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponseMerchantPaybill
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponsePaymentMerchantList
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.*
import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Data.Model.Pocket.*
import com.aTeam.beasy.Data.Model.Profile.RequestProfileSettingEdit
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileData
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSetting
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSettingEdit
import com.aTeam.beasy.Data.Model.Transfer.*
import com.aTeam.beasy.Data.Model.User.ResponseUserBalance
import io.reactivex.rxjava3.core.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    //auth
    //login
    @POST("api/auth/signin")
    fun login(
        @Body login: RequestAuthLogin
    ) : Flowable<ResponseAuthLoginSuccess>

    //forgot password
    @POST("api/auth/forgot-password")
    fun forgotPassword(
        @Body forgot : RequestAuthForgotPassword
    ) : Flowable<ResponseAuthForgotPassword>

    //end auth

    //transfer
    //contact all
    @GET("api/contacts")
    fun getContact(
        @Header("Authorization") token: String
    ) : Flowable<ResponseTransferContact>

    //contact recent
    @GET("api/contacts/recent")
    fun getRecentContact(
        @Header("Authorization") token: String
    ) : Flowable<ResponseTransferContact>

    //addContact
    @POST("api/contacts")
    fun addContact(
        @Header("Authorization") token: String,
        @Body addContact : RequestAddContact
    ) : Flowable<ResponseAddContact>

    //getContactById
    @GET("api/contacts/{id}")
    fun getContactById(
        @Header("Authorization") token: String,
        @Path("id") id : String
    ) : Flowable<ResponseTransferContact>

    //editContact
    @PUT("api/contacts/{id}")
    fun editContactID(
        @Header("Authorization") token: String,
        @Path("id") id : String,
        @Body editContact : RequestEditContact
    ) : Flowable<ResponseEditContact>

    //deleteContact
    @DELETE("api/contacts/{id}")
    fun deleteContactId(
        @Header("Authorization") token: String,
        @Path("id") id : String,
    ) : Flowable<ResponseDeleteContact>

    //end contact




    //transfer to contact
    @POST("api/transfer")
    fun transferToContact(
        @Header("Authorization") token: String,
        @Body transferToContact : RequestTransferConfirmation
    ) : Flowable<ResponseTransferConfirmation>
    //end transfer

    //bank
    @GET("api/bank")
    fun getBank(
        @Header("Authorization") token: String
    ) : Call<ResponseBankList>
    //end bank



    //payment

    //payment last transaction
    @GET("api/payment/recent")
    fun getPaymentLastTransaction(
        @Header("Authorization") token: String
    ) : Flowable<ResponsePaymentLastTransaction>

    //Mobile
    //phone pulsa or credit
    @GET("api/payment/mobilecredit/{numberPhone}")
    fun getAirtimeReload(
        @Header("Authorization") token: String,
        @Path("numberPhone") phoneNumber : String
    ) : Flowable<ResponsePaymentMobileCreditAirtimeReload>

    //internet data
    @GET("api/payment/mobiledata/{numberPhone}")
    fun getInternetData(
        @Header("Authorization") token: String,
        @Path("numberPhone") phoneNumber : String
    ) : Flowable<ResponsePaymentMobileInternetData>

    //pay mobile
    @POST("api/payment/mobile")
    fun postMobilePay(
        @Header("Authorization") token: String,
        @Body mobilePay : RequestPaymentMobilePay
    ) : Flowable<ResponsePaymentMobilePay>
    //End Mobile

    //credit card
    //bill
    @GET("api/payment/creditcardbill/{ccNumber}")
    fun getCreditBill(
        @Header("Authorization") token: String,
        @Path("ccNumber") ccNumber : String
    ) : Flowable<ResponsePaymentCreditCardBill>

    //pay bill
    @POST("api/payment/creditcard")
    fun postCreditBill(
        @Header("Authorization") token: String,
        @Body creditCardPayBill : RequestPaymentCreditCardBillPay
    ) : Flowable<ResponsePaymentCreditCardBillPay>
    //end credit card


    //merchant
    //list merchant
    @GET("api/payment/merchant")
    fun getMerchantList(
        @Header("Authorization") token: String,
    ) : Flowable<ResponsePaymentMerchantList>

    //pay merchant bill
    @POST("api/payment/merchant")
    fun postMerchantBill(
        @Header("Authorization") token: String,
        @Body merchantPayBill : RequestMerchantPayBill
    ) : Flowable<ResponseMerchantPaybill>
    //end merchant
    //end payment


    //gamification

    //user gamification start
    @POST("api/gamification/start")
    fun postGamificationStart(
        @Header("Authorization") token: String,
    ) : Flowable<ResponseGamificationStart>

    //user gamification status
    @GET("api/gamification/me")
    fun getGamificationUserStatus(
        @Header("Authorization") token: String,
    ) : Flowable<ResponseGamificationStatusUser>

    //gamification planet detail
    @GET("api/gamification/planets/{planetId}")
    fun getGamificationPlanetDetailWithid(
        @Header("Authorization") token: String,
        @Path("planetId") planetId : String
    ) : Flowable<ResponseGamificationDetailPlanet>


    //gamification detail reward detail
    @GET("/api/gamification/rewards/{rewardId}")
    fun getGamificationDetailRewardPlanet(
        @Header("Authorization") token: String,
        @Path("rewardId") rewardId : String
    ) : Flowable<ResponseGamificationDetailRewardPlanet>

    //gamification planet list
    @GET("api/gamification/planets/")
    fun getGamificationPlanetListStatus(
        @Header("Authorization") token: String,
        @Query("status") status : String
    ) : Flowable<ResponseGamificationPlanetListStatus>

    //post claim reward
    @POST("api/gamification/rewards/{rewardId}/claim")
    fun postGamificationClaimReward(
        @Header("Authorization") token: String,
        @Path("rewardId") rewardId : String
    ) : Flowable<ResponseGamificationClaimReward>

    //gamification my reward list
    @GET("api/gamification/rewards")
    fun getGamificationMyRewardList(
        @Header("Authorization") token: String
    ) : Flowable<ResponseGamificationMyRewardList>
    //end gamification

    //User Balance
    @GET("api/user/balance")
    fun getUserBalance(
        @Header("Authorization") token: String
    ) : Flowable<ResponseUserBalance>
    //End User Balance

    // pocket
    //Pocket List
    @GET("api/pocket")
    fun getPocketList(
        @Header("Authorization") token: String,
        @Query("with_primary") with_primary : Boolean,
    ) : Flowable<ResponsePocketList>

    //Pocket By Id
    @GET("api/pocket/{pocketId}")
    fun getPocketById(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String
    ) : Flowable<ResponsePocketById>

    //Pocket By Id History
    @GET("api/pocket/{pocketId}/history")
    fun getPocketByIdHistory(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String
    ) : Flowable<ResponsePocketHistory>

    //Pocket Create
    @Multipart
    @POST("api/pocket")
    fun pocketCreate(
        @Header("Authorization") token: String,
        @Part("dueDate") dueDate : RequestBody,
        @Part("name") name : RequestBody,
        @Part picture : MultipartBody.Part,
        @Part("target") target : RequestBody
    ) : Flowable<ResponsePocketCreate>

    @Multipart
    @PUT("api/pocket/{pocketId}")
    fun pocketEdit(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String,
        @Part("dueDate") dueDate : RequestBody,
        @Part("name") name : RequestBody,
        @Part("target") target : RequestBody
    ) : Flowable<ResponsePocketEdit>


    @DELETE("api/pocket/{pocketId}")
    fun pocketDelete(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String
    ) : Flowable<ResponsePocketDelete>


    //pocket topup
    @POST("api/pocket/{pocketId}/topup")
    fun postPocketTopUpById(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String,
        @Body requestPocketTopUp : RequestPocketTopUp
    ) : Flowable<ResponsePocketTopUp>

    //pocket move
    @POST("api/pocket/{pocketId}/move")
    fun postPocketMoveById(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String,
        @Body requestPocketMove: RequestPocketMove
    ) : Flowable<ResponsePocketMove>
    //End

    //pocket filter
    @GET("api/pocket/{pocketId}/history")
    fun getPocketFilter(
        @Header("Authorization") token: String,
        @Path("pocketId") pocketId : String,
        @Query("sort_order") sort_order : String,
        @Query("type") type : String
    ) : Flowable<ResponsePocketHistory>

    //e-wallet
    //account all
    @GET("api/accounts")
    fun getAccount(
        @Header("Authorization") token: String
    ) : Flowable<ResponseEwalletContact>

    //account recent
    @GET("api/accounts/recent")
    fun getRecentAccount(
        @Header("Authorization") token: String
    ) : Flowable<ResponseEwalletContact>

    //add account
    @POST("api/accounts")
    fun addAccount(
        @Header("Authorization") token: String,
        @Body addContact : RequestAddAccount
    ) : Flowable<ResponseAddAccount>

    //getAccountById
    @GET("api/accounts/{id}")
    fun getAccountById(
        @Header("Authorization") token: String,
        @Path("id") id : String
    ) : Flowable<ResponseEwalletContact>

    //edit account
    @PUT("api/accounts/{id}")
    fun editAccountID(
        @Header("Authorization") token: String,
        @Path("id") id : String,
        @Body editAccount : RequestEditAccount
    ) : Flowable<ResponseEditAccount>

    //delete account
    @DELETE("api/accounts/{id}")
    fun deleteAccountId(
        @Header("Authorization") token: String,
        @Path("id") id : String,
    ) : Flowable<ResponseDeleteAccount>

    //end account

    //transfer to e-wallet
    @POST("api/ewallet")
    fun walletToAccount(
        @Header("Authorization") token: String,
        @Body transferToContact: RequestEwalletConfirmation
    ) : Flowable<ResponseEwalletConfirmation>
    //end transfer

    //e-wallet
    @GET("api/ewallet")
    fun getEwallet(
        @Header("Authorization") token: String
    ) : Call<ResponseEwalletList>
    //end e-wallet


    // Profiel section
    @GET("api/profile/mobile")
    fun getProfileData(
        @Header("Authorization") token: String,
    ) : Flowable<ResponseProfileData>

    @GET("api/profile/mobile/setting")
    fun getProfileSettingData(
        @Header("Authorization") token: String,
    ) : Flowable<ResponseProfileSetting>

    @PUT("api/profile/mobile/setting")
    fun putProfileSettingData(
        @Header("Authorization") token: String,
        @Body requestProfileSettingEdit: RequestProfileSettingEdit
    ) : Flowable<ResponseProfileSettingEdit>
    // End Profile
}