package com.aTeam.beasy.View.HomeFeature.Ewallet

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletConfirmationViewModel
import kotlinx.android.synthetic.main.wallet_confirmation_cancel_dialog.view.*
import kotlinx.android.synthetic.main.wallet_confirmation_otp_dialog.view.*
import kotlinx.android.synthetic.main.wallet_confirmation_page_activity.*

class WalletConfirmationPageActivity : AppCompatActivity() {


    lateinit var functionResourceWallet : FunctionResource

    companion object{
        const val CONTACT_DATA_WALLET = "extra_data_wallet"
        const val TRANSFER_AMOUNT_WALLET = "transfer_amount_wallet"
        const val TRANSFER_NOTE_WALLET = "transfer_note_wallet"
    }

    //viewmodel
    private lateinit var walletConfirmationViewModel: WalletConfirmationViewModel

    //session
    private lateinit var sessionWallet: SessionManager


    override fun onBackPressed() {
        super.onBackPressed()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallet_confirmation_page_activity)
        supportActionBar?.hide()

        val window : Window = this@WalletConfirmationPageActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@WalletConfirmationPageActivity, R.color.status_bar_blue)

        //viewmodel
        walletConfirmationViewModel = ViewModelProviders.of(this).get(WalletConfirmationViewModel::class.java)

        //session
        sessionWallet = SessionManager(this)

        functionResourceWallet = FunctionResource()

        val accountData = intent.getParcelableExtra<DataItemAccount>(CONTACT_DATA_WALLET) as DataItemAccount
        val walletAmount = intent.getStringExtra(TRANSFER_AMOUNT_WALLET)
        val walletMassage = intent.getStringExtra(TRANSFER_NOTE_WALLET)
        val walletCost = accountData.adminFee
        Log.d("tampilData", accountData.toString())
        Log.d("tampilData2", walletAmount.toString())
        Log.d("tampilData3", walletMassage.toString())
        Log.d("tampilData4", walletCost.toString())
        val walletAmountDebited = walletAmount?.toInt()!! + walletCost?.toInt()!!

        avWalletConfirmationAvatar.setText(accountData.name.toString())
        tvWalletConfirmationName.text = accountData.name.toString()
        tvWalletConfirmationAccount.text = accountData.accountName.toString()
        tvWalletConfirmationAmount.text = functionResourceWallet.rupiah(walletAmount.toDouble())
        tvWalletConfirmationCost.text = accountData.adminFee?.let { functionResourceWallet.rupiah(it.toDouble()) }
        tvWalletConfirmationAmountDebited.text = functionResourceWallet.rupiah(walletAmountDebited.toDouble())
        tvWalletConfirmationNote.text = walletMassage

        btnWalletConfirmationNextButton.setOnClickListener {
            showCreateCategoryDialogWallet(walletAmount, walletMassage, accountData)
        }

        btnWalletConfirmationCancelButton.setOnClickListener {
            showCancelDialogWallet()
        }

        ivBackWalletConfirmation.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showCancelDialogWallet() {
        val contextWallet = this
        val builderWallet = AlertDialog.Builder(contextWallet)

        val view = layoutInflater.inflate(R.layout.wallet_confirmation_cancel_dialog, null)

        var dialog = builderWallet.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnWalletConfirmationCancelDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnWalletConfirmationYesDialogSubmit.setOnClickListener {
            val intent = Intent(contextWallet, WalletMainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        dialog.show()
    }

    fun showCreateCategoryDialogWallet(
        walletAmount: String,
        walletNote: String?,
        accountData: DataItemAccount) {

        val contextWallet = this
        val builderWallet = AlertDialog.Builder(contextWallet)

        val view = layoutInflater.inflate(R.layout.wallet_confirmation_otp_dialog, null)
        var pinError = false

        var dialog = builderWallet.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnWalletConfirmationDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnWalletConfirmationDialogSubmit.setOnClickListener {

            val pin = view.pvWalletConfirmationOtp.text.toString()
            if (pin.length != 6){
                pinError = true
                Toast.makeText(this, "your pin is less than 6", Toast.LENGTH_SHORT).show()
            }else if (pin.isEmpty()){
                pinError = true
                Toast.makeText(this, "your pin is empty", Toast.LENGTH_SHORT).show()
            }else{
                sessionWallet.login?.let { it1 -> walletConfirmationViewModel.addAccountViewModel(it1,
                    accountId = accountData.id.toString(),
                    amount = walletAmount.toInt(),
                    message = walletNote.toString(),
                    pin = pin.toInt()
                ) }

                checkWallet(view, walletAmount, walletNote, accountData)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun checkWallet(
        view: View,
        walletAmount: String,
        walletMassage: String?,
        accountData: DataItemAccount
    ) {
        walletConfirmationViewModel.responseData.observe(this, androidx.lifecycle.Observer {
            val moveConfirmationIntent = Intent(this@WalletConfirmationPageActivity, WalletResultActivity::class.java)
            finish()
            moveConfirmationIntent.putExtra(WalletResultActivity.WALLET_RESULT, it.data)
            startActivity(moveConfirmationIntent)
            Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
        })

        walletConfirmationViewModel.errorData.observe(this, androidx.lifecycle.Observer {
            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
        })

        walletConfirmationViewModel.isLoading.observe(this, androidx.lifecycle.Observer {
            if (it == true) loadingWaitWalletConfirmation.visibility = View.VISIBLE
            else {
                loadingWaitWalletConfirmation.visibility = View.GONE
                showCreateCategoryDialogWallet(walletAmount, walletMassage, accountData)
            }
        })
    }

}