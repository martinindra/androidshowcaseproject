package com.aTeam.beasy.View.HomeFeature.Payment.Merchants.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponseMerchantPaybill
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponsePaymentMerchantList
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.ResponsePaymentMobilePay
import com.aTeam.beasy.Data.Repository.Payment.PaymentMerchantRepository

class PaymentMerchantViewModel : ViewModel() {
    val paymentMerchantRepository = PaymentMerchantRepository()

    val responseMerchantList = MutableLiveData<ResponsePaymentMerchantList>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getMerchantListData(
        header : String
    ){
        isLoading.value = true

        paymentMerchantRepository.paymentMerchantList(
            tokenUser = header,
            {
                isLoading.value = false
                responseMerchantList.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseMerchantPayBill = MutableLiveData<ResponseMerchantPaybill>()
    fun postMerchantPayBill(
        header: String,
        id : String,
        pin : Int,
        amount : Int
    ){
        isLoading.value = true

        paymentMerchantRepository.paymentMerchantPayBill(
            tokenUser = header,
            id = id,
            pin = pin,
            amount = amount,
            {
                isLoading.value = false
                responseMerchantPayBill.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }
}