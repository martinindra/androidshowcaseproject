package com.aTeam.beasy.View.HomeFeature.Profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListView
import android.widget.Toast
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Profile.Adapter.ProfileFaqExpandableListAdapter
import com.aTeam.beasy.View.HomeFeature.Profile.Adapter.ProfileFaqExpandableListData.data
import kotlinx.android.synthetic.main.profile_faq_activity.*

class ProfileFaqActivity : AppCompatActivity() {
    private var expandableListView: ExpandableListView? = null
    private var adapter: ProfileFaqExpandableListAdapter? = null
    private var titleList: List<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_faq_activity)
        expandableListView = findViewById(R.id.elProfileFaqContent)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = ProfileFaqExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }

        ivProfileFAQBack.setOnClickListener {
            onBackPressed()
            finish()
        }
    }
}