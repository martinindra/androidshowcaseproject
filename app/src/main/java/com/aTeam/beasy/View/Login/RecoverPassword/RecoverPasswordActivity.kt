package com.aTeam.beasy.View.Login.RecoverPassword

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.ViewModel.PaymentMobileViewModel
import com.aTeam.beasy.View.Login.LoginMainActivity
import com.aTeam.beasy.View.Login.ViewModel.ForgotPasswordViewModel
import com.aTeam.beasy.View.Login.ViewModel.LoginMainViewModel
import kotlinx.android.synthetic.main.login_recover_password_activity.*
import kotlinx.android.synthetic.main.login_dialog_recover_password.view.*
import kotlinx.android.synthetic.main.login_main_activity.*

class RecoverPasswordActivity : AppCompatActivity(), View.OnClickListener {

    //view model
    private lateinit var forgotPasswordViewModel : ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_recover_password_activity)

        tvCustomer.setOnClickListener(this)

        btnNextForgotPassword.setOnClickListener(this)

        //viewmodel
        forgotPasswordViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)

    }

    override fun onClick(v: View?) {
        if (v != null) {
            when(v.id){
                R.id.btnNextForgotPassword ->{

                    var wrongAccount = false
                    val accountNumber = etAccountNumber.text.toString()
                    val accountEmail = etEmailRecover.text.toString()

                    if (accountEmail.isEmpty()){
                        wrongAccount = true
                        etEmailRecover.setBackgroundResource(R.drawable.bg_form_wrong)
                        tvMessageForgot.text = "Please fill the form"

                    }
                    else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(accountEmail).matches()) {
                        wrongAccount = true
                        etEmailRecover.setBackgroundResource(R.drawable.bg_form_wrong)
                    }

                    if (accountNumber.isEmpty()){
                        wrongAccount = true
                        etAccountNumber.setBackgroundResource(R.drawable.bg_form_wrong)
                        tvMessageForgot.text = "Please fill the form"
                    }

                    if (!wrongAccount) {
                        Log.d("forgotResponse", "$accountEmail $accountNumber")
                        forgotPasswordViewModel.forgotPassword(email = accountEmail, accountNumber = accountNumber)
                        forgotComponent()
                    }
                }
                R.id.tvCustomer ->{
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                android.Manifest.permission.CALL_PHONE)) {
                            ActivityCompat.requestPermissions(this,
                                arrayOf(android.Manifest.permission.CALL_PHONE), 1)
                        } else {
                            ActivityCompat.requestPermissions(this,
                                arrayOf(android.Manifest.permission.CALL_PHONE), 1)
                        }
                    }else{
                        intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:1500777")
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun forgotComponent() {
        forgotPasswordViewModel.responseData.observe(this, Observer {
            forgotDialog()
        })

        forgotPasswordViewModel.errorData.observe(this, Observer {
            tvMessageForgot.text = "You have entered wrong account number/email "
            etEmailRecover.setBackgroundResource(R.drawable.bg_form_wrong)
            etAccountNumber.setBackgroundResource(R.drawable.bg_form_wrong)
        })

        forgotPasswordViewModel.isLoading.observe(this, Observer {
            if (it) skForgotLoading.visibility = View.VISIBLE
            else skForgotLoading.visibility = View.GONE
        })

    }

    private fun forgotDialog() {
        val view = View.inflate(
            this@RecoverPasswordActivity,
            R.layout.login_dialog_recover_password,
            null
        )

        val builder = AlertDialog.Builder(this@RecoverPasswordActivity)
        builder.setView(view)

        val dialog = builder.create()
        dialog.show()
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        val recoverPassword =
            Intent(this@RecoverPasswordActivity, LoginMainActivity::class.java)
        view.okBtn.setOnClickListener {
            startActivity(recoverPassword)

        }
    }
}