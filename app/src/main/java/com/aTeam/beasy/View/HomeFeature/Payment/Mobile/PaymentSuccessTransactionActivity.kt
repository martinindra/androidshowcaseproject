package com.aTeam.beasy.View.HomeFeature.Payment.Mobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import kotlinx.android.synthetic.main.payment_mobile_success_transaction_activity.*

class PaymentSuccessTransactionActivity : AppCompatActivity() {
    companion object{
        const val ID = "ID"
        const val TYPE = "TYPE"
        const val NUMBERPHONE = "NUMBERPHONE"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_mobile_success_transaction_activity)

        val id = intent.getStringExtra(ID)
        val type = intent.getStringExtra(TYPE)
        val numberPhone = intent.getStringExtra(NUMBERPHONE)

        btnPaymentDone.setOnClickListener {
            val intent = Intent(this, MainBaseActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}