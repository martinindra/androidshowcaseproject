package com.aTeam.beasy.View.HomeFeature.Payment.Merchants

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import kotlinx.android.synthetic.main.payment_merchant_main_qrcode_activity.*

class PaymentMerchantMainQRCodeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_merchant_main_qrcode_activity)

        ivPaymentMerchantsQRCodeBack.setOnClickListener {
            onBackPressed()
        }

        ivPaymentMerchantsMainQRCode.setOnClickListener {
            onBackPressed()
        }

        ibPaymentMerchantsMainQRCodeTakePhoto.setOnClickListener {
            startActivity(Intent(this, PaymentMerchantQrCodeActivity::class.java))
        }

        btnPaymentMerchantsMainQRCodeUploadGallery.setOnClickListener {
            startActivity(Intent(this, UnderConstructionActivity::class.java))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}