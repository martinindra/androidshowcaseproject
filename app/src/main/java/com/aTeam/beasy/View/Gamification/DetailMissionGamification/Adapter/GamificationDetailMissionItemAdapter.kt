package com.aTeam.beasy.View.Gamification.DetailMissionGamification.Adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Gamification.MissionItem
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.PaymentMerchantSeeAllActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gamification_detail_mission_planet_mission_litem_list.view.*

class GamificationDetailMissionItemAdapter : RecyclerView.Adapter<GamificationDetailMissionItemAdapter.ViewHolder>() {
    var dataMissionGamificationDetailPlanet : ArrayList<MissionItem> = arrayListOf()

    fun setData (dataGamification : ArrayList<MissionItem>){
        this.dataMissionGamificationDetailPlanet = dataGamification
        notifyDataSetChanged()
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val checkStatus = itemView.ivGamificationDetailMissionCheckFinished
        val wordingMission = itemView.tvGamificationDetailMissionWordingDetail
        val seeMore = itemView.tvGamificationDetailMissionSeeMerchant
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.gamification_detail_mission_planet_mission_litem_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataMissionGamificationDetailPlanetPosition =
            dataMissionGamificationDetailPlanet.get(position)

        Log.d("DataGamificationMission", dataMissionGamificationDetailPlanetPosition.toString())

        if (dataMissionGamificationDetailPlanetPosition.passed == true)
            Glide.with(holder.itemView.context).load(R.drawable.gamification_plot_oke)
                .into(holder.checkStatus)
        else
            Glide.with(holder.itemView.context).load(R.drawable.gamification_plot_circle)
                .into(holder.checkStatus)

        holder.wordingMission.text = dataMissionGamificationDetailPlanetPosition.wording

        if (dataMissionGamificationDetailPlanetPosition.missionType == "PAYMENT_MERCHANT"){
            holder.seeMore.visibility = View.VISIBLE
            holder.seeMore.setOnClickListener {
                val intent = Intent(holder.itemView.context, PaymentMerchantSeeAllActivity::class.java)
                holder.itemView.context.startActivities(arrayOf(intent))
            }
        }else
            holder.seeMore.visibility = View.GONE

    }

    override fun getItemCount(): Int = dataMissionGamificationDetailPlanet.size
}