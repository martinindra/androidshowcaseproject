package com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Bank.DataItemBank
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.TransferBankListViewModel
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.TransferEditContactViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.transfer_add_contact_bottom_sheet_fragment.*

class TransferEditContactBottomSheetFragment(
    var arrayContactData: DataItemContact,
    var parentMenuFragment: BottomSheetDialogFragment,
    var callbackEdit : () -> Unit
    ) : BottomSheetDialogFragment() {

    //token
    private lateinit var session: SessionManager

    //viewModel
    private lateinit var transferBankListViewModel: TransferBankListViewModel
    private lateinit var transferEditContactViewModel: TransferEditContactViewModel

    private lateinit var bankData : ArrayList<DataItemBank>
    private lateinit var bankName : ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.transfer_add_contact_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvContactDialogTitle.text = "Edit Account"
        transfer_add_recipient_button.setText("Done")
        view.setBackgroundResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        //viewmodel
        transferBankListViewModel = ViewModelProviders.of(this).get(TransferBankListViewModel::class.java)
        transferEditContactViewModel = ViewModelProviders.of(this).get(TransferEditContactViewModel::class.java)

        bankName = arrayListOf()
        //get session data
        session = SessionManager(requireContext())

        //bank data
        bankList()

        transfer_edt_name_contact.setText("${arrayContactData.name}")
        transfer_edt_account_number_contact.setText("${arrayContactData.accountNumber}")
        var bankNameSelected : String? = null
        var bankIdSelectd : String? = null
        val selection = findIndex(bankName, arrayContactData.bankName.toString())
        Log.d("BankSelection", selection.toString())
        transfer_edt_select_bank_contact.setSelection(selection)
        transfer_edt_select_bank_contact.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                bankNameSelected = bankData[selection].bankName
                bankIdSelectd = bankData[selection].bankId
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                bankNameSelected = bankData[p2].bankName
                bankIdSelectd = bankData[p2].bankId
            }

        }

        transfer_add_recipient_button.setOnClickListener {
            editContact(arrayContactData.id!!, transfer_edt_name_contact.text.toString(),
                bankIdSelectd!!, transfer_edt_account_number_contact.text.toString())
        }
    }

    private fun bankList() {
        session.login?.let { transferBankListViewModel.getBankData(header = it) }
        transferBankListViewModel.responseData.observe(viewLifecycleOwner, Observer {
            bankData = it.data as ArrayList<DataItemBank>
            for (i in bankData.indices){
                bankName.add(bankData.get(i).bankName.toString())
            }
            transfer_edt_select_bank_contact.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, bankName)
        })

        transferBankListViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlTransferAddBottomConten),  "Bank data is null", Snackbar.LENGTH_LONG).show()
        })

        transferBankListViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            transfer_add_recipient_button.isEnabled = it != true
        })
    }

    private fun editContact(
        id: String,
        nameContact: String,
        bankIdSelectd: String,
        accountNumber: String
    ) {
        session.login?.let { transferEditContactViewModel.editContact(
            header = it,
            name = nameContact,
            bankId = bankIdSelectd,
            accountNumber = accountNumber,
            id = id
        ) }

        transferEditContactViewModel.responseData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlMainHomeBaseContent),  nameContact.toString() + " Success updated", Snackbar.LENGTH_LONG).show()
            dismiss()
            parentMenuFragment.dismiss()
            callbackEdit()
        })
        transferEditContactViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), nameContact.toString() + " Gagal Di Update ", Toast.LENGTH_SHORT).show()
        })

        transferEditContactViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) skAddEditLoading.visibility = View.VISIBLE
            else skAddEditLoading.visibility = View.GONE
        })
    }

    fun findIndex(arr: ArrayList<String>, item: String) : Int {
        for (i in arr.indices)
        {
            if (arr[i] == item) {
                return i
            }
        }
        return -1
    }

    override fun dismiss() {
        super.dismiss()
    }
}