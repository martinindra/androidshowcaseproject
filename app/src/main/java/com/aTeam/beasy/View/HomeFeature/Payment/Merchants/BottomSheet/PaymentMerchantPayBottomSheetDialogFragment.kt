package com.aTeam.beasy.View.HomeFeature.Payment.Merchants.BottomSheet

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.ViewModel.PaymentMerchantViewModel
import com.aTeam.beasy.View.HomeFeature.Payment.PaymentSuccessTransfer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.payment_merchant_qr_payment_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.transfer_confirmation_otp_dialog.view.*

class PaymentMerchantPayBottomSheetDialogFragment(
    var id : String
) : BottomSheetDialogFragment() {

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var paymentMerchantViewModel: PaymentMerchantViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.payment_merchant_qr_payment_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        paymentMerchantViewModel = ViewModelProviders.of(this).get(PaymentMerchantViewModel::class.java)

        if (id.isNotEmpty() || id.isNullOrBlank()){
            edtPaymentMerchantQrMerchantId.setText(id.toString())
        }

        edtPaymentMerchantQrBill.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! >= 4){
                    btnPayEnabled(true)
                }else{
                    btnPayEnabled(false)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s?.length!! >= 4){
                    btnPayEnabled(true)
                }else{
                    btnPayEnabled(false)
                }
            }
        })

        btnPaymentMerchantQrPay.setOnClickListener {
            showPinDialog()
        }
    }

    private fun btnPayEnabled(b: Boolean) {
        if (b) {
            btnPaymentMerchantQrPay.isEnabled = true
            btnPaymentMerchantQrPay.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
            btnPaymentMerchantQrPay.setTextColor(Color.parseColor("#ffffff"))
        } else {
            btnPaymentMerchantQrPay.isEnabled = false
            btnPaymentMerchantQrPay.setBackgroundResource(R.drawable.all_resource_bg_grey_fillform)
            btnPaymentMerchantQrPay.setTextColor(Color.parseColor("#000000"))
        }
    }

    fun showPinDialog(
    ) {

        val builder = AlertDialog.Builder(requireContext())

        val view = layoutInflater.inflate(R.layout.transfer_confirmation_otp_dialog, null)
        var pinError = false

        var dialog = builder.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferConfirmationDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnTransferConfirmationDialogSubmit.setOnClickListener {

            val pin = view.pvTransferConfirmationOtp.text.toString()
            if (pin.length != 6){
                pinError = true
                Toast.makeText(requireContext(), "your pin is less than 6", Toast.LENGTH_SHORT).show()
            }else if (pin.isEmpty()){
                pinError = true
                Toast.makeText(requireContext(), "your pin is empty", Toast.LENGTH_SHORT).show()
            }else{
                sessionManager.login?.let { it1 ->
                    paymentMerchantViewModel.postMerchantPayBill(
                        header = it1,
                        id = edtPaymentMerchantQrMerchantId.text.toString(),
                        pin = pin.toInt(),
                        amount = edtPaymentMerchantQrBill.text.toString().toInt()
                    )
                }
                
                paymentMerchantViewModel.responseMerchantPayBill.observe(viewLifecycleOwner, Observer {
                    Toast.makeText(requireContext(), "Success Pay", Toast.LENGTH_SHORT).show()
                    val intent = Intent(requireContext(), PaymentSuccessTransfer::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                })
                paymentMerchantViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
                    Toast.makeText(requireContext(), "Failed to pay", Toast.LENGTH_SHORT).show()
                })
            }
        }


        dialog.show()
    }
}