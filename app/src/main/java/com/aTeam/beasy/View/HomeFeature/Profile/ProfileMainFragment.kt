package com.aTeam.beasy.View.HomeFeature.Profile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Profile.DataProfileUser
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Profile.ViewModel.ProfileMainViewModel
import com.aTeam.beasy.View.Login.LoginMainActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.profile_main_fragment.*


class ProfileMainFragment : Fragment() {

    //token
    private lateinit var sessionManager: SessionManager
    //viewmodel
    private lateinit var profileMainViewModel: ProfileMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.profile_main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //declare
        sessionManager = SessionManager(requireContext())
        profileMainViewModel = ViewModelProviders.of(this).get(ProfileMainViewModel::class.java)

        tvProfileContentFAQ.setOnClickListener {
            startActivity(Intent(context, ProfileFaqActivity::class.java))
        }

        tvProfileContentHelpCenter.setOnClickListener {
            startActivity(Intent(context, ProfileHelpCenterActivity::class.java))
        }

        ivProfileTopbarTitleSetting.setOnClickListener {
            startActivity(Intent(context, ProfileSettingActivity::class.java))
        }

        btnProfileLogout.setOnClickListener {
            sessionManager.deleteDataToken()
            val intent = Intent(context, LoginMainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        profileProcess()
    }

    private fun profileProcess() {
        sessionManager.login?.let { profileMainViewModel.getUserMainProfile(it) }
        resultProcess()
    }

    private fun resultProcess() {
        profileMainViewModel.responseProfile.observe(viewLifecycleOwner, Observer {
            bindView(it.data)
        })

        profileMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Log.d("dataResponse", it.localizedMessage)
            Toast.makeText(requireContext(), "Cannot Get Profile Data", Toast.LENGTH_SHORT).show()
            profileMainErrorFound.visibility = View.VISIBLE
        })

        profileMainViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) profileMainLoading.visibility = View.VISIBLE
            else profileMainLoading.visibility = View.GONE
        })
    }

    private fun bindView(data: DataProfileUser?) {
        if (data?.profilePicture != null) Glide.with(requireContext()).load(data.profilePicture).into(ivProfileUserInfoAvatar)
        tvProfileUserInfoName.text = data?.fullName
        tvProfileUserInfoEmail.text = data?.email
        tvProfileUserInfoPhone.text = data?.phoneNumber

        val cardOne = arrayListOf<String>()
        val cardTwo = arrayListOf<String>()
        val cardThree = arrayListOf<String>()
        val cardFour = arrayListOf<String>()

        for (i in data?.cardNumber?.indices!!){
            when {
                i < 4 -> {
                    cardOne.add(data.cardNumber[i].toString())
                }
                i in 4..7 -> {
                    cardTwo.add(data.cardNumber[i].toString())
                }
                i in 8..11 -> {
                    cardThree.add(data.cardNumber[i].toString())
                }
                i >= 12 -> {
                    cardFour.add(data.cardNumber[i].toString())
                }
            }
        }

        val cardOneString = cardOne.joinToString(
            prefix = "",
            separator = "",
            postfix = "",
            limit = -1,
            truncated = "",
            transform = { it.toUpperCase() })

        val cardTwoString = cardTwo.joinToString(
            prefix = "",
            separator = "",
            postfix = "",
            limit = -1,
            truncated = "",
            transform = { it.toUpperCase() })

        val cardThreeString = cardThree.joinToString(
            prefix = "",
            separator = "",
            postfix = "",
            limit = -1,
            truncated = "",
            transform = { it.toUpperCase() })

        val cardFourString = cardFour.joinToString(
            prefix = "",
            separator = "",
            postfix = "",
            limit = -1,
            truncated = "",
            transform = { it.toUpperCase() })

        tvProfileContentCardAccountNumber.text = "$cardOneString  $cardTwoString  $cardThreeString  $cardFourString"
        tvProfileContentCardAccountName.text = data.fullName
    }

}