package com.aTeam.beasy.View.HomeFeature.Payment.Merchants

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.Merchant.DataItemMerchantList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.Adapter.PaymentAllMerchantSearchMainListAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.ViewModel.PaymentMerchantViewModel
import kotlinx.android.synthetic.main.payment_merchant_see_all_activity.*

class PaymentMerchantSeeAllActivity : AppCompatActivity() {

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var paymentMerchantViewModel: PaymentMerchantViewModel
    private lateinit var dataMerchantList : ArrayList<DataItemMerchantList>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_merchant_see_all_activity)

        sessionManager = SessionManager(this)
        paymentMerchantViewModel = ViewModelProviders.of(this).get(PaymentMerchantViewModel::class.java)


        ivBackPaymentSearchSearch.setOnClickListener {
            onBackPressed()
        }

        rvBind()


    }

    private fun rvBind() {
        edtPaymentMerchantSearchSearch.isEnabled = true
        dataMerchantList = arrayListOf()

        sessionManager.login?.let {
            paymentMerchantViewModel.getMerchantListData(
                header = it
            )
        }

        paymentMerchantViewModel.responseMerchantList.observe(this, Observer {
            dataMerchantList = it.data as ArrayList<DataItemMerchantList>
            val adapterPaymentSearch = PaymentAllMerchantSearchMainListAdapter(supportFragmentManager)
            adapterPaymentSearch.setData(dataMerchantList = dataMerchantList)

            rvPaymentMerchantSearchContact.apply {
                adapter = adapterPaymentSearch
                setHasFixedSize(true)
            }
            edtPaymentMerchantSearchSearch.isEnabled = true
            edtPaymentMerchantSearchSearch.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    adapterPaymentSearch.filter.filter(s.toString())
                }
            })
        })

        paymentMerchantViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Fail Load Merchant Data", Toast.LENGTH_SHORT).show()
            edtPaymentMerchantSearchSearch.isEnabled = false
        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}