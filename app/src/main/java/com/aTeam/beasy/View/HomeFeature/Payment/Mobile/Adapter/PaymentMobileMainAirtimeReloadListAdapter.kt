package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.DataItemAirtimeReload
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import kotlinx.android.synthetic.main.payment_mobile_airtime_reload_list_item.view.*

class PaymentMobileMainAirtimeReloadListAdapter(var callback : (id : String?) -> Unit) :
    RecyclerView.Adapter<PaymentMobileMainAirtimeReloadListAdapter.ViewHolder>() {

    var dataAirtimeReload : ArrayList<DataItemAirtimeReload> = arrayListOf()

    private var lastChecked: ConstraintLayout? = null
    private var nominalChecked : TextView? = null
    private var totalChecked : TextView? = null
    private var lastCheckedPos = -1

    private var functionResource : FunctionResource = FunctionResource()
    fun setData (dataAirtime : ArrayList<DataItemAirtimeReload>){
        this.dataAirtimeReload = dataAirtime
        notifyDataSetChanged()
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val nominal = itemView.tvPaymentMainMobileAirtimeNominal
        val totalPayment = itemView.tvPaymentMainMobileAirtimeTotal
        val layout = itemView.clPaymentmobileAirtimeItemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.payment_mobile_airtime_reload_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataAirtimeReload.get(position)
        Log.d("phoneNumberData", data.toString())
        holder.nominal.text = data.denom
        holder.totalPayment.text = "Pay : " + functionResource.rupiah(data.price?.toInt()?.toDouble()!!).toString()

        holder.itemView.setOnClickListener{
            val id = data.id.toString()
            callback(id)
            if (lastCheckedPos == -1){
                lastCheckedPos = position
                lastChecked = holder.layout
                nominalChecked = holder.nominal
                totalChecked = holder.totalPayment
                holder.layout.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                holder.nominal.setTextColor(Color.parseColor("#ffffff"))
                holder.totalPayment.setTextColor(Color.parseColor("#ffffff"))
            }else {
                lastChecked?.setBackgroundResource(R.drawable.all_resource_payment_white_bg)
                nominalChecked?.setTextColor(Color.parseColor("#000000"))
                totalChecked?.setTextColor(Color.parseColor("#000000"))
                holder.layout.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                holder.nominal.setTextColor(Color.parseColor("#ffffff"))
                holder.totalPayment.setTextColor(Color.parseColor("#ffffff"))
                lastChecked = holder.layout
                nominalChecked = holder.nominal
                totalChecked = holder.totalPayment
                lastCheckedPos = position
            }
        }
    }

    override fun getItemCount(): Int = this.dataAirtimeReload.size

}