package com.aTeam.beasy.View.Gamification.DetailRewardGamification.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Gamification.DataItemGamificationRewardList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter.GamificationDetailRewardMyRewardItemAdapter
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.ViewModel.GamificationDetailRewardViewModel
import kotlinx.android.synthetic.main.gamification_detail_reward_mission_my_reward_fragment.*


class GamificationDetailRewardMissionMyReward : Fragment() {
    //token
    private lateinit var sessionManager: SessionManager

    //view model
    private lateinit var gamificationDetailRewardViewModel: GamificationDetailRewardViewModel

    private lateinit var dataMyReward : ArrayList<DataItemGamificationRewardList>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.gamification_detail_reward_mission_my_reward_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        gamificationDetailRewardViewModel = ViewModelProviders.of(this).get(GamificationDetailRewardViewModel::class.java)

        myRewardList()


    }

    private fun myRewardList() {
        sessionManager.login?.let {
            gamificationDetailRewardViewModel.getGamificationMyRewardList(
                header = it
            )
        }

        processResponse()
    }

    private fun processResponse() {
        gamificationDetailRewardViewModel.responseGamificationMyRewardList.observe(viewLifecycleOwner, Observer {
            bindDataView(it.data)
        })
        gamificationDetailRewardViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get your reward list", Toast.LENGTH_SHORT).show()
            gamificationMyRewardErrorFound.visibility = View.VISIBLE
        })
        gamificationDetailRewardViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) gamificationMyRewardLoading.visibility = View.VISIBLE
            else gamificationMyRewardLoading.visibility = View.GONE
        })
    }

    private fun bindDataView(data: List<DataItemGamificationRewardList?>?) {
        dataMyReward = arrayListOf()
        dataMyReward = data as ArrayList<DataItemGamificationRewardList>
        if (dataMyReward.isNotEmpty()) {
            val adapterGamifikasi = GamificationDetailRewardMyRewardItemAdapter()
            adapterGamifikasi.setData(dataGamification = dataMyReward)
            rvGamificationDetailRewardMissionMyRewards.apply {
                adapter = adapterGamifikasi
                setHasFixedSize(true)
            }
        }
        if (dataMyReward.isEmpty()){
            llGamificationMyReward.visibility = View.VISIBLE
        }
    }
}