package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseTransferConfirmation
import com.aTeam.beasy.Data.Repository.Transfer.TransferRepository

class TransferConfirmationViewModel : ViewModel() {
    val transferRepository = TransferRepository()

    val responseData = MutableLiveData<ResponseTransferConfirmation>()
    val errorData = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()

    fun addContactViewModel(header:String, contactId : String, amount : Int, note : String, pin : Int){
        isLoading.value = true
        transferRepository.transferToContact(
            tokenUser = header,
            contactId = contactId,
            amount = amount,
            note = note,
            pin = pin,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                Log.d("errorData", it)
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}