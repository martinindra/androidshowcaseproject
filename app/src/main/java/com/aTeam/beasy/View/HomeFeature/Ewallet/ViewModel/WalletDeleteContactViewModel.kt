package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseDeleteAccount
import com.aTeam.beasy.Data.Repository.Ewallet.WalletContactRepository

class WalletDeleteAccountViewModel : ViewModel() {
    val walletAccountRepository = WalletContactRepository()

    val responseDataWallet = MutableLiveData<ResponseDeleteAccount>()
    val errorDataWallet = MutableLiveData<Throwable>()
    val isLoadingWallet = MutableLiveData<Boolean>()

    fun deleteDataWallet(header:String, idUser : String){
        isLoadingWallet.value = true
        walletAccountRepository.deleteAccountById(tokenUser = header, idUser = idUser,
            {
                isLoadingWallet.value = false
                responseDataWallet.value = it
            },
            {
                isLoadingWallet.value = false
                errorDataWallet.value = it
            }
        )
    }
}