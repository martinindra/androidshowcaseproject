package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseTransferContact
import com.aTeam.beasy.Data.Repository.Transfer.TransferContactRepository

class TransferMainContactViewModel : ViewModel() {
    val transferContactRepository = TransferContactRepository()

    val responseData = MutableLiveData<ResponseTransferContact>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    val responseDataRecent = MutableLiveData<ResponseTransferContact>()
    val errorDataRecent = MutableLiveData<Throwable>()
    val isLoadingRecent = MutableLiveData<Boolean>()

    fun getContactData(header:String){
        isLoading.value = true
        transferContactRepository.getContact(header,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }

    fun getContactRecent(header: String){
        isLoadingRecent.value = true
        transferContactRepository.getContactRecent(header,
            {
                responseDataRecent.value = it
                Log.d("dataRecent", it.toString())
                isLoadingRecent.value = false
            },
            {
                errorDataRecent.value = it
                isLoadingRecent.value = false
            }
        )
    }
}