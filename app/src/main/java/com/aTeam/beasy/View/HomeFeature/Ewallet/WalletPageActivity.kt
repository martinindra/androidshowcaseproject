package com.aTeam.beasy.View.HomeFeature.Ewallet

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.R
import kotlinx.android.synthetic.main.wallet_page_activity.*

class WalletPageActivity : AppCompatActivity() {
    companion object{
        const val EXTRA_DATA_WALlET = "extra_data_wallet"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallet_page_activity)
        supportActionBar?.hide()

        val window : Window = this@WalletPageActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@WalletPageActivity, R.color.status_bar_blue)

        val walletReference = intent.getParcelableExtra<DataItemAccount>(EXTRA_DATA_WALlET) as DataItemAccount
        Log.d("tampilData5", walletReference.toString())
        avWalletPageAvatar.setText(walletReference.name.toString())
        tvWalletPageName.text = walletReference.name.toString()
        tvWalletPageAccount.text = walletReference.accountName.toString() + " " + walletReference.accountAccNumber

        btnWalletPageNextButton.setOnClickListener {
            nextWalletConfirmation(walletReference)
        }

        ivBackWalletPage.setOnClickListener {
            onBackPressed()
        }
    }

    private fun nextWalletConfirmation(walletReference: DataItemAccount) {
        var isEmptyField = false
        var note : String? = null
        if (edWalletPageAmount.text!!.isEmpty()){
            isEmptyField = true
            edWalletPageAmount.setBackgroundResource(R.drawable.bg_form_wrong)
        }
        else if(edWalletPageAmount.text.toString().toInt() < 10000){
            isEmptyField = true
            edWalletPageAmount.setBackgroundResource(R.drawable.bg_form_wrong)
            tvMessageEdWalletPageAmount.setTextColor(Color.parseColor("#FF0000"))
        }
        if (edWalletPageNote.text.toString().isEmpty()){
            note = "-"
        }else{
            note = edWalletPageNote.text.toString()
        }

        if (!isEmptyField){
            val moveConfirmationWalletIntent = Intent(this@WalletPageActivity, WalletConfirmationPageActivity::class.java)
            moveConfirmationWalletIntent.putExtra(WalletConfirmationPageActivity.CONTACT_DATA_WALLET, walletReference)
            moveConfirmationWalletIntent.putExtra(WalletConfirmationPageActivity.TRANSFER_AMOUNT_WALLET, edWalletPageAmount.text.toString())
            moveConfirmationWalletIntent.putExtra(WalletConfirmationPageActivity.TRANSFER_NOTE_WALLET, note.toString())
            startActivity(moveConfirmationWalletIntent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}