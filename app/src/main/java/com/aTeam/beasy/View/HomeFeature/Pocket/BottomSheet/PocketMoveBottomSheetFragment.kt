package com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Pocket.DataItemPocketList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pocket_move_bottom_sheet_fragment.*

class PocketMoveBottomSheetFragment(var pocketId : String?, var pocketNames : String?, var stateChange : () -> Unit) : BottomSheetDialogFragment() {
    //session
    private lateinit var sessionManager: SessionManager

    // viewModel
    private lateinit var pocketMainViewModel: PocketMainViewModel

    //pocket Data
    private lateinit var pocketData : ArrayList<DataItemPocketList>
    private lateinit var pocketName : ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pocket_move_bottom_sheet_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //init
        sessionManager = SessionManager(requireContext())
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)
        pocketName = arrayListOf()
        pocketDataList()

        //spinner
        var pocketNameSelected : String? = null
        var pocketIdSelected : String? = null
        val selection = findIndex(pocketName, pocketNames.toString())
        spinnerSelectPocketOnMove.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                pocketNameSelected = pocketData[selection].pocketName
                pocketIdSelected = pocketData[selection].id
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                pocketNameSelected = pocketData[p2].pocketName
                pocketIdSelected = pocketData[p2].id
            }
        }

        btnPocketMoveDone.setOnClickListener {
            var errorInput = false
            if (etEnterAmountOnMove.text.toString().isEmpty()) {
                errorInput = true
                etEnterAmountOnMove.error = "Please input amount number"
                etEnterAmountOnMove.setBackgroundResource(R.drawable.all_resource_bg_white_fillform_error)
            }

            if (pocketIdSelected == pocketId){
                errorInput = true
                tvPocketErrorMove.visibility = View.VISIBLE
            }

            if (!errorInput) {
                val amount = etEnterAmountOnMove.text.toString()

                movePocketAmount(pocketId, pocketIdSelected, amount.toInt())
            }
        }
    }

    private fun movePocketAmount(pocketId: String?, pocketIdSelected: String?, amount: Int) {
        sessionManager.login?.let {
            pocketMainViewModel.postMovePocket(
                header = it,
                pocketId = pocketId.toString(),
                destination = pocketIdSelected.toString(),
                amount = amount
            )
        }

        responseMove()
    }

    private fun responseMove() {
        pocketMainViewModel.responsePocketMove.observe(viewLifecycleOwner, Observer {
            stateChange()
            dismiss()
        })

        pocketMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to move pocket", Toast.LENGTH_SHORT).show()
        })
    }


    private fun pocketDataList() {
        pocketData = arrayListOf()
        sessionManager.login?.let {
            pocketMainViewModel.getPocketList(
                header = it,
                with_primary = true
            )
        }

        responsePocketList()
    }

    private fun responsePocketList() {
        pocketMainViewModel.responsePocketList.observe(viewLifecycleOwner, Observer {
            pocketData = it.data as ArrayList<DataItemPocketList>
            for (i in pocketData.indices){
                pocketName.add(pocketData.get(i).pocketName.toString())
            }
            spinnerSelectPocketOnMove.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, pocketName)
        })

        pocketMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            btnPocketMoveDone.isEnabled = false
            Toast.makeText(requireContext(), "Failed to get Pocket Data", Toast.LENGTH_SHORT).show()
        })
    }

    fun findIndex(arr: ArrayList<String>, item: String) : Int {
        for (i in arr.indices)
        {
            if (arr[i] == item) {
                return i
            }
        }
        return -1
    }

    override fun dismiss() {
        super.dismiss()
    }
}