package com.aTeam.beasy.View.OnBoarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.aTeam.beasy.Data.Model.SliderModel.SliderOnboardModel
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.Login.LoginMainActivity
import com.aTeam.beasy.View.OnBoarding.Adapter.OnBoardSliderAdapter
import kotlinx.android.synthetic.main.gamification_main_activity.*
import kotlinx.android.synthetic.main.onboard_main_activity.*

class OnBoardMainActivity : AppCompatActivity() {

    private lateinit var dataList : ArrayList<SliderOnboardModel>
    private lateinit var sliderAdapter : OnBoardSliderAdapter
    private lateinit var session : SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.onboard_main_activity)
        session = SessionManager(this)
        checkSession()
        loadsData()

        vpOnboard.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
        vpOnboard.currentItem = 0

        btnOnboardGetStarted.setOnClickListener {
            session.getStarted = true
            val intent = Intent(this@OnBoardMainActivity, LoginMainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun checkSession() {
        if (session.getStarted ?: true){
            val intent = Intent(this@OnBoardMainActivity, LoginMainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun loadsData() {
        // init list
        dataList = ArrayList()

        //add data
        dataList.add(SliderOnboardModel(
            "Create your Beasy account easily, anytime and anywhere",
            "The echnology will enable customers to open an account and carry out all standard banking activities from their smartphone or wearable device, setting a new standard in the banking sector.",
            R.drawable.onboard_main_first_slider)
        )
        dataList.add(SliderOnboardModel(
            "Get reward after adventure with DOGE in BEASY Galaxy",
            "Get reward when you complete missions on each planet and win attractive prizes through the mystery box in each level Beasy with the City of a Thousand Planets.",
            R.drawable.onboard_main_second_slider)
        )
        dataList.add(
            SliderOnboardModel(
                "Make your dreams come true with pocket feature",
                "hola",
                R.drawable.onboard_main_third_slider)
        )

        // adapter setup
        sliderAdapter = OnBoardSliderAdapter(this, dataList)
        vpOnboard.adapter = sliderAdapter
        diPagerIndicator.setViewPager(vpOnboard)
    }
}