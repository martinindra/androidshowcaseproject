package com.aTeam.beasy.View.HomeFeature.Pocket.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Pocket.DataItemPocketList
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Pocket.PocketPlanningActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.pocket_grid_planning_item.view.*

class PocketMainListItemAdapter(
    private var fragmentManager: FragmentManager,
    private var context : Context
    ) : RecyclerView.Adapter<PocketMainListItemAdapter.ViewHolder>() {
    private var dataPocketList : ArrayList<DataItemPocketList> = arrayListOf()
    private var functionResource : FunctionResource = FunctionResource()
    fun setData(dataPocket : ArrayList<DataItemPocketList>){
        this.dataPocketList = dataPocket
        notifyDataSetChanged()
    }

    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var imagePocket = itemView.ivImageProfileGrid
        var titlePocket = itemView.tvTitleSmallPocketLeft
        var moneyTotal = itemView.tvSumMoneySmallPocketLeft
        var progressBar = itemView.pbSmallPocketLeft
        val gridPocket = itemView.llGridLeft
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.pocket_grid_planning_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataPocketPosition = dataPocketList.get(position)
        Glide.with(holder.itemView.context).load(dataPocketPosition.picture).into(holder.imagePocket)
        holder.titlePocket.text = dataPocketPosition.pocketName
        if (dataPocketPosition.target != null || dataPocketPosition.target != 0){
            holder.moneyTotal.text = functionResource.rupiah(dataPocketPosition.target?.toDouble()!!).toString()
        }else{
            val uang = 0
            holder.moneyTotal.text = functionResource.rupiah(uang.toDouble()).toString()
        }

        val percent : Double? = (dataPocketPosition.balance?.toDouble()!! / dataPocketPosition.target.toDouble()) * 100
        holder.progressBar.setProgress(percent?.toInt()!!)

        holder.gridPocket.setOnClickListener {
            val intent = Intent(context, PocketPlanningActivity::class.java)
            intent.putExtra(PocketPlanningActivity.POCKETID, dataPocketPosition.id)
            intent.putExtra(PocketPlanningActivity.POCKETNAME, dataPocketPosition.pocketName)
            intent.putExtra(PocketPlanningActivity.DUEDATE, dataPocketPosition.dueDate)
            context.startActivities(arrayOf(intent))
        }

    }

    override fun getItemCount(): Int = dataPocketList.size
}