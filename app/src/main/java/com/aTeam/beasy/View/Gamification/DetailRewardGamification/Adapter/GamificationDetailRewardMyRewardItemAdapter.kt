package com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Gamification.DataItemGamificationRewardList
import com.aTeam.beasy.Data.Model.Gamification.PlanetStatusDataItem
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.GamificationDetailMissionRewardActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gamification_detail_mission_reward_activity.*
import kotlinx.android.synthetic.main.gamification_detail_reward_mission_item_list.view.*

class GamificationDetailRewardMyRewardItemAdapter : RecyclerView.Adapter<GamificationDetailRewardMyRewardItemAdapter.ViewHolder>() {
    var dataGamificationMyReward : ArrayList<DataItemGamificationRewardList> = arrayListOf()

    fun setData (dataGamification : ArrayList<DataItemGamificationRewardList>){
        this.dataGamificationMyReward = dataGamification
        notifyDataSetChanged()
    }

    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var image = itemView.ivGamificationDetailMissionIcon
        var name = itemView.tvGamificationDetailMissionTitle
        var button = itemView.btnGamificationDetailMissionButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.gamification_detail_reward_mission_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var dataGamificationDetailRewardMyRewardPosition = dataGamificationMyReward.get(position)
        when (dataGamificationDetailRewardMyRewardPosition.type) {
            "TRANSFER" -> {
                Glide.with(holder.itemView.context).load(R.drawable.gamification_reward_money_transaction).into(holder.image)
            }

            "DISCOUNT" -> {
                Glide.with(holder.itemView.context).load(R.drawable.gamification_reward_voucher).into(holder.image)
            }

            "CASHBACK" -> {
                Glide.with(holder.itemView.context).load(R.drawable.gamification_reward_money_bag).into(holder.image)
            }
        }

        holder.name.text = dataGamificationDetailRewardMyRewardPosition.wording
        holder.button.text = "Details"
        holder.button.setOnClickListener {
            val intent = Intent(it.context, GamificationDetailMissionRewardActivity::class.java)
            intent.putExtra(GamificationDetailMissionRewardActivity.IDREWARD, dataGamificationDetailRewardMyRewardPosition.id)
            intent.putExtra(GamificationDetailMissionRewardActivity.ISCLAIMABLE, "yes")
            it.context.startActivities(arrayOf(intent))
        }
    }

    override fun getItemCount(): Int = dataGamificationMyReward.size
}