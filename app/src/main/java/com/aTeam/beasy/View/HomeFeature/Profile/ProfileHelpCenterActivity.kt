package com.aTeam.beasy.View.HomeFeature.Profile

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.aTeam.beasy.R
import com.aTeam.beasy.R.color.*
import kotlinx.android.synthetic.main.profile_help_center_activity.*

class ProfileHelpCenterActivity : AppCompatActivity() {
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_help_center_activity)

        ivProfileHelpBack.setOnClickListener {
            onBackPressed()
        }

        btnProfileHelpEmailSend.setOnClickListener {
            var errorSend = false

            if (edtProfileHelpEmailName.text.toString().trim() == "" || edtProfileHelpEmailName.text.toString().trim().isEmpty()){
                errorSend = true
                edtProfileHelpEmailName.error = "Please Fill this name field"
            }

            if (edtProfileHelpEmailMessage.text.toString().trim() == "" || edtProfileHelpEmailMessage.text.toString().trim().isEmpty()){
                errorSend = true
                edtProfileHelpEmailMessage.error = "Please Fill this Message field"
            }
            if (edtProfileHelpEmailPhone.text.toString().trim() == "" || edtProfileHelpEmailPhone.text.toString().trim().isEmpty()){
                errorSend = true
                edtProfileHelpEmailPhone.error = "Please Fill this Message field"
            }
            if (edtProfileHelpEmailAddress.text.toString().trim() == "" || edtProfileHelpEmailAddress.text.toString().trim().isEmpty()){
                errorSend = true
                edtProfileHelpEmailAddress.error = "Please Fill this Email Address field"
            }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtProfileHelpEmailAddress.text.toString().trim()).matches()){
                errorSend = true
                edtProfileHelpEmailAddress.error = "Please Fill this Email Address field With Email Pattern"
            }

            if (!errorSend){
                Toast.makeText(this, "Success Send Email", Toast.LENGTH_SHORT).show()
            }
        }
    }
}