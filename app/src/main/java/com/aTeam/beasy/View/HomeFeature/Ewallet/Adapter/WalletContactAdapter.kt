package com.aTeam.beasy.View.HomeFeature.Ewallet.Adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Ewallet.BottomSheet.WalletMenuBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Ewallet.WalletPageActivity
import kotlinx.android.synthetic.main.wallet_contact_item.view.*

class WalletContactAdapter(private var fragmentManager: FragmentManager, var callBackAdapter : () -> Unit) : RecyclerView.Adapter<WalletContactAdapter.ViewHolder>(), Filterable {
    var dataEwallet : ArrayList<DataItemAccount> = arrayListOf()
    var dataEwalletFilter : ArrayList<DataItemAccount> = arrayListOf()

    fun setData (dataEwallet : ArrayList<DataItemAccount>){
        this.dataEwallet = dataEwallet
        this.dataEwalletFilter = dataEwallet
        notifyDataSetChanged()
    }

    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var imageAvatarWallet = itemView.avWalletItemAvatar
        var nameAvatarWallet = itemView.tvWalletItemName
        var walletNameAvatar = itemView.tvWalletName
        var moreButtonWallet = itemView.ibWalletItemMoreButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletContactAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.wallet_contact_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: WalletContactAdapter.ViewHolder, position: Int) {
        var dataEwallet = this.dataEwallet.get(position)
        holder.imageAvatarWallet.setText(dataEwallet.name.toString())
        holder.nameAvatarWallet.text = dataEwallet.name
        holder.walletNameAvatar.text = dataEwallet.accountName + " " + dataEwallet.accountAccNumber

        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, WalletPageActivity::class.java)
            intent.putExtra(WalletPageActivity.EXTRA_DATA_WALlET, dataEwallet)
            it.context.startActivities(arrayOf(intent))
        }
        holder.moreButtonWallet.setOnClickListener {
            val walletBottomSheetFragment = WalletMenuBottomSheetFragment(dataEwallet) {
                callBackAdapter()
            }
            walletBottomSheetFragment.show(fragmentManager, "BottomSheetDialog")
        }
    }

    override fun getItemCount(): Int = dataEwallet.size
    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
                var filterResults = Filter.FilterResults()
                Log.d("HasilFilterQuery", constraint.toString())

                if (constraint == null || constraint.length == 0) {
                    Log.d("HasilFilterQuery", "here")
                    Log.d("HasilFilterQuery", dataEwalletFilter.toString())
                    filterResults.count = dataEwalletFilter.size
                    filterResults.values = dataEwalletFilter
                } else {
                    var newData = ArrayList<DataItemAccount>()
                    var queryStringWallet = constraint.toString().toLowerCase()
                    for (row in dataEwalletFilter) {
                        if (row.name?.toLowerCase()?.contains(queryStringWallet)!!
                        ) {
                            newData.add(row)
                        }
                    }
                    filterResults.count = newData.size
                    filterResults.values = newData
                }

                Log.d("HasilFilterData", filterResults.values.toString())
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataEwallet = results!!.values as ArrayList<DataItemAccount>
                notifyDataSetChanged()
            }
        }
    }
}