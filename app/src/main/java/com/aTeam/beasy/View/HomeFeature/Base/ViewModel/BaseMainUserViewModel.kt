package com.aTeam.beasy.View.HomeFeature.Base.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Data.Model.User.ResponseUserBalance
import com.aTeam.beasy.Data.Repository.Payment.PaymentMainRepository
import com.aTeam.beasy.Data.Repository.User.UserMainRepository

class BaseMainUserViewModel : ViewModel() {

    val userMainRepository = UserMainRepository()

    val responseUserViewModel = MutableLiveData<ResponseUserBalance>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getUserMainBalance(
        header : String
    ){
        isLoading.value = true

        userMainRepository.getUserBalance(
            tokenUser = header,
            {
                isLoading.value = false
                responseUserViewModel.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

}