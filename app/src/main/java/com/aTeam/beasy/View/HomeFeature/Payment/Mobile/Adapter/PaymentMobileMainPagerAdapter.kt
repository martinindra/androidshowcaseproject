package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Fragment.PaymentMainMobileAirtimeReloadFragment
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Fragment.PaymentMobileMainInternetDataFragment


class PaymentMobileMainPagerAdapter (fm : FragmentManager?,var phoneNumber : String, var callback : (id : String?) -> Unit,
var type : (type : Int) -> Unit) :
    FragmentPagerAdapter(fm!!)
{

    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                PaymentMainMobileAirtimeReloadFragment(phoneNumber
                ) {
                    callback(it)
                    type(1)
                }
            }
            1 -> {
                PaymentMobileMainInternetDataFragment(
                    phoneNumber
                )
                {
                    callback(it.toString())
                    type(2)
                }
            }
            else -> {
                PaymentMainMobileAirtimeReloadFragment(phoneNumber
                ) {
                    callback(it)
                    type(1)
                }
            }
        }
    }
}