package com.aTeam.beasy.View.HomeFeature.Base

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.aTeam.beasy.Data.Model.User.DataItemUserBalance
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.Adapter.MainBasePagerAdapter
import com.aTeam.beasy.View.HomeFeature.Base.ViewModel.BaseMainUserViewModel
import com.aTeam.beasy.View.SplashScreen.SplashScreenActivity
import kotlinx.android.synthetic.main.base_main_activity.*

class MainBaseActivity : AppCompatActivity() {

    //token
    private lateinit var sessionManager: SessionManager

    companion object{
        const val CAMERA_REQ = 101
        const val CALL_PHONE = 102
        const val READ_CONTACTS = 103
        const val READ_STORAGE = 104
        const val WRITE_STORAGE = 105
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_main_activity)
        supportActionBar?.hide()

        val window : Window = this@MainBaseActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@MainBaseActivity, R.color.status_bar_blue)

        viewPagerBind()

        setupPermission()
    }

    private fun viewPagerBind() {
        base_viewpager.adapter = MainBasePagerAdapter(supportFragmentManager)
        changingTabs(0)
        base_viewpager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

                changingTabs(position)
            }

            override fun onPageSelected(position: Int) {
                changingTabs(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        bottomNavOnClick()
    }

    private fun bottomNavOnClick() {
        bottom_navigation_home.setOnClickListener {
            base_viewpager.currentItem = 0
        }
        bottom_navigation_pocket.setOnClickListener {
            base_viewpager.currentItem = 1
        }
        bottom_navigation_payment.setOnClickListener {
            base_viewpager.currentItem = 2
        }
        bottom_navigation_transfer.setOnClickListener {
            base_viewpager.currentItem = 3
        }
        bottom_navigation_profile.setOnClickListener {
            base_viewpager.currentItem = 4
        }
    }

    private fun changingTabs(position: Int) {
        if (position == 0){
            bottom_navigation_home.setImageResource(R.drawable.bottom_nav_home_selected)
            bottom_navigation_pocket.setImageResource(R.drawable.bottom_nav_pocket)
            bottom_navigation_payment.setImageResource(R.drawable.bottom_nav_payment)
            bottom_navigation_transfer.setImageResource(R.drawable.bottom_nav_transfer)
            bottom_navigation_profile.setImageResource(R.drawable.bottom_nav_profile)
        } else if (position == 1){
            bottom_navigation_home.setImageResource(R.drawable.bottom_nav_home)
            bottom_navigation_pocket.setImageResource(R.drawable.bottom_nav_pocket_selected)
            bottom_navigation_payment.setImageResource(R.drawable.bottom_nav_payment)
            bottom_navigation_transfer.setImageResource(R.drawable.bottom_nav_transfer)
            bottom_navigation_profile.setImageResource(R.drawable.bottom_nav_profile)
        } else if (position == 2){
            bottom_navigation_home.setImageResource(R.drawable.bottom_nav_home)
            bottom_navigation_pocket.setImageResource(R.drawable.bottom_nav_pocket)
            bottom_navigation_payment.setImageResource(R.drawable.bottom_nav_payment_selected)
            bottom_navigation_transfer.setImageResource(R.drawable.bottom_nav_transfer)
            bottom_navigation_profile.setImageResource(R.drawable.bottom_nav_profile)
        }
        else if (position == 3){
            bottom_navigation_home.setImageResource(R.drawable.bottom_nav_home)
            bottom_navigation_pocket.setImageResource(R.drawable.bottom_nav_pocket)
            bottom_navigation_payment.setImageResource(R.drawable.bottom_nav_payment)
            bottom_navigation_transfer.setImageResource(R.drawable.bottom_nav_transfer_selected)
            bottom_navigation_profile.setImageResource(R.drawable.bottom_nav_profile)
        }
        else if (position == 4){
            bottom_navigation_home.setImageResource(R.drawable.bottom_nav_home)
            bottom_navigation_pocket.setImageResource(R.drawable.bottom_nav_pocket)
            bottom_navigation_payment.setImageResource(R.drawable.bottom_nav_payment)
            bottom_navigation_transfer.setImageResource(R.drawable.bottom_nav_transfer)
            bottom_navigation_profile.setImageResource(R.drawable.bottom_nav_profile_selected)
        }
    }

    private var backPressedTime:Long = 0
    lateinit var backToast: Toast
    override fun onBackPressed() {
        backToast = Toast.makeText(this, "Press back again to leave the app.", Toast.LENGTH_LONG)
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel()
            super.onBackPressed()
            return
        } else {
            backToast.show()
        }
        backPressedTime = System.currentTimeMillis()
    }


    private fun setupPermission() {
        val permissionCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        val permissionCall = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE)
        val permissionContact = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
        val permissionWriteExternalStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permissionReadExternalStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)

        if (permissionCamera != PackageManager.PERMISSION_GRANTED){
            makeRequest(1)
        }
        if (permissionCall != PackageManager.PERMISSION_GRANTED){
            makeRequest(2)
        }
        if (permissionContact != PackageManager.PERMISSION_GRANTED){
            makeRequest(3)
        }
        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED){
            makeRequest(4)
        }
        if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
            makeRequest(5)
        }
    }

    private fun makeRequest(id: Int) {
        when(id){
            1 -> {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.CAMERA),
                    MainBaseActivity.CAMERA_REQ
                )
            }

            2 -> {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.CALL_PHONE),
                    MainBaseActivity.CALL_PHONE
                )
            }
            3 -> {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.READ_CONTACTS),
                    MainBaseActivity.READ_CONTACTS
                )
            }
            4 -> {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MainBaseActivity.WRITE_STORAGE
                )
            }
            5 -> {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    MainBaseActivity.READ_STORAGE
                )
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            MainBaseActivity.CAMERA_REQ -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You need the camera permission to use this app", Toast.LENGTH_SHORT).show()
                }
                makeRequest(2)
            }
            MainBaseActivity.CALL_PHONE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        this,
                        "You need the Call Phone Permission to use this app",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                makeRequest(3)

            }
            MainBaseActivity.READ_CONTACTS -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You need the Read Contacts permission to use this app", Toast.LENGTH_SHORT).show()
                }
                makeRequest(4)

            }
            MainBaseActivity.WRITE_STORAGE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You need the Write Storage permission to use this app", Toast.LENGTH_SHORT).show()
                }
                makeRequest(5)

            }
            MainBaseActivity.READ_STORAGE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You need the Read Contact permission to use this app", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}