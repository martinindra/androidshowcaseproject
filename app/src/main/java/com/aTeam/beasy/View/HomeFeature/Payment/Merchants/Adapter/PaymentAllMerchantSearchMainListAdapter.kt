package com.aTeam.beasy.View.HomeFeature.Payment.Merchants.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.Merchant.DataItemMerchantList
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.BottomSheet.PaymentMerchantPayBottomSheetDialogFragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.payment_merchant_list_item.view.*

class PaymentAllMerchantSearchMainListAdapter(private var fragmentManager: FragmentManager) :
    RecyclerView.Adapter<PaymentAllMerchantSearchMainListAdapter.ViewHolder>(),
    Filterable {
    var dataMerchantList: ArrayList<DataItemMerchantList> = arrayListOf()
    var dataMerchantListFilter: ArrayList<DataItemMerchantList> = arrayListOf()

    fun setData(dataMerchantList: ArrayList<DataItemMerchantList>) {
        this.dataMerchantList = dataMerchantList
        this.dataMerchantListFilter = dataMerchantList
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageMerchant = itemView.civPaymentMerchantSearchItemIconAvatar
        var nameMerchant = itemView.tvPaymentMerchantSearchItemTitle
        var idMerchant = itemView.tvPaymentMerchantSearchItemMerchantId

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.payment_merchant_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var dataMerchantPosition = this.dataMerchantList.get(position)

        Glide.with(holder.itemView.context).load(dataMerchantPosition.logo)
            .into(holder.imageMerchant)
        holder.nameMerchant.text = dataMerchantPosition.name
        holder.idMerchant.text = dataMerchantPosition.id

        holder.itemView.setOnClickListener {
            val paymentMerchantPayBottomSheetDialogFragment =
                PaymentMerchantPayBottomSheetDialogFragment(dataMerchantPosition.id.toString())
            paymentMerchantPayBottomSheetDialogFragment.show(fragmentManager, "BottomSheet")
        }
    }

    override fun getItemCount(): Int = dataMerchantList.size
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
                var filterResults = Filter.FilterResults()

                if (constraint == null || constraint.length == 0) {
                    filterResults.count = dataMerchantListFilter.size
                    filterResults.values = dataMerchantListFilter
                } else {
                    var dataBaru = ArrayList<DataItemMerchantList>()
                    var queryString = constraint.toString().toLowerCase()
                    for (row in dataMerchantListFilter) {
                        if (row.name?.toLowerCase()?.contains(queryString)!!
                        ) {
                            dataBaru.add(row)
                        }
                    }
                    filterResults.count = dataBaru.size
                    filterResults.values = dataBaru
                }
                Log.d("filterMerchant", filterResults.values.toString())
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataMerchantList = results!!.values as ArrayList<DataItemMerchantList>
                notifyDataSetChanged()
            }
        }
    }
}