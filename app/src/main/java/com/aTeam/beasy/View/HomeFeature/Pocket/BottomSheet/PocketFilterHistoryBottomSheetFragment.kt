package com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Pocket.DataItemPocketList
import com.aTeam.beasy.Data.Model.Pocket.DataPocketHistory
import com.aTeam.beasy.Data.Model.Pocket.ResponsePocketHistory
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pocket_filter_history_bottom_sheet_fragment.*

class PocketFilterHistoryBottomSheetFragment (var pocketId : String, var filterCallback : (response : ArrayList<DataPocketHistory>) -> Unit) : BottomSheetDialogFragment() {

    private lateinit var rbFilter : RadioButton
    private lateinit var rbSort : RadioButton

    private lateinit var sessionManager: SessionManager
    private lateinit var pocketMainViewModel: PocketMainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pocket_filter_history_bottom_sheet_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sessionManager = SessionManager(requireContext())
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)

        rgFilterByPocket.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                rbFilter = view.findViewById<RadioButton>(checkedId)
            }
        )

        rgSortByPocket.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                rbSort = view.findViewById<RadioButton>(checkedId)
            }
        )

        buttonDoneFilterHistory.setOnClickListener{
            var canSortFilter : Boolean = false
            val idFilter : Int = rgFilterByPocket.checkedRadioButtonId
            var filter : String? = null
            if (idFilter != -1){
                if (rbFilter.text == "Top Up"){
                    filter = "TOP_UP"
                }
                if (rbFilter.text == "Move"){
                    filter = "MOVE"
                }
                canSortFilter = true
            }else{
                Toast.makeText(requireContext(), "Nothing Checked on filter", Toast.LENGTH_SHORT).show()
                canSortFilter = false
            }

            val idSort : Int = rgSortByPocket.checkedRadioButtonId
            var sort : String? = null
            if (idSort != -1){
//                if (rbSort.text == "Newer"){
//                    sort = "asc"
//                }
//                if (rbSort.text == "Older"){
//                    sort = "desc"
//                }
            }

            if (canSortFilter) {
                sessionManager.login?.let { login ->
                    pocketMainViewModel.getPocketHistoryByIdFilter(
                        header = login,
                        pocketId = pocketId,
                        type = filter.toString(),
                        sort = "desc"
                    )
                }
                responseProcess()
            }


        }
    }

    private fun responseProcess() {
        pocketMainViewModel.responsePocketByIdHistory.observe(viewLifecycleOwner, Observer {
            filterCallback(it.data as ArrayList<DataPocketHistory>)
            Toast.makeText(requireContext(), "Success Filter", Toast.LENGTH_SHORT).show()
            dismiss()
        })
        pocketMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer{
            Toast.makeText(requireContext(), "Failed to Filter", Toast.LENGTH_SHORT).show()
        })
    }

    override fun dismiss() {
        super.dismiss()
    }
}