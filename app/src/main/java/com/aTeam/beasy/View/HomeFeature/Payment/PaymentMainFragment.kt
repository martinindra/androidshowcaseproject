package com.aTeam.beasy.View.HomeFeature.Payment

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.DataItemLastTransaction
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Base.ViewModel.BaseMainUserViewModel
import com.aTeam.beasy.View.HomeFeature.Payment.CreditCard.PaymentCreditCardMainActivity
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.PaymentMerchantMainActivity
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.PaymentMobileMainActivity
import com.aTeam.beasy.View.HomeFeature.Payment.ViewModel.PaymentMainViewModel
import com.aTeam.beasy.View.HomeFeature.Pocket.PocketAddActivity
import com.aTeam.beasy.View.HomeFeature.Transfer.Adapter.PaymentTransactionItemAdapter
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import kotlinx.android.synthetic.main.payment_main_fragment.*
import kotlinx.android.synthetic.main.pocket_main_fragment.*


class PaymentMainFragment : Fragment(), View.OnClickListener {
    private lateinit var dataPaymentTransaction : ArrayList<DataItemLastTransaction>

    //token
    private lateinit var sessionManager: SessionManager

    //view model
    private lateinit var paymentMainViewModel : PaymentMainViewModel
    //main user
    private lateinit var userMainBalance : BaseMainUserViewModel

    //function resource
    private var functionResource : FunctionResource = FunctionResource()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.payment_main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dataPaymentTransaction = arrayListOf()

        ivPaymentToggleEyeMainBalance.setOnClickListener(this)

        sessionManager = SessionManager(requireContext())
        paymentMainViewModel = ViewModelProviders.of(this).get(PaymentMainViewModel::class.java)
        userMainBalance = ViewModelProviders.of(this).get(BaseMainUserViewModel::class.java)

        sessionManager.login?.let { paymentMainViewModel.getLastTransactionPayment(header = it) }

        paymentMainViewModel.responsePaymentLastTransaction.observe(viewLifecycleOwner, Observer {
            dataPaymentTransaction = it.data as ArrayList<DataItemLastTransaction>
            val adapterPayment = PaymentTransactionItemAdapter(parentFragmentManager)
            adapterPayment.setData(dataPaymentTransaction = dataPaymentTransaction)

            rvPaymentLastTransaction.apply {
                adapter = adapterPayment
                setHasFixedSize(true)
            }

        })
        paymentMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed Load Last Transaction", Toast.LENGTH_SHORT).show()
            paymentMainErrorFound.visibility = View.VISIBLE
        })

        paymentMainViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if(it == true){
                paymentMainLoading.visibility = View.VISIBLE
            }else{
                paymentMainLoading.visibility = View.GONE
            }
        })

        paymentMenu()

        userBalance()
    }

    private fun userBalance() {
        sessionManager.login?.let { userMainBalance.getUserMainBalance(header = it) }
        userMainBalance.responseUserViewModel.observe(viewLifecycleOwner, Observer {
            Log.d("coba", it.data.toString())
            val userBalance = it.data?.balance
            tvPaymentUserBalance.text = functionResource.rupiah(userBalance?.toDouble()!!).toString()
        })

        userMainBalance.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get user balance", Toast.LENGTH_SHORT).show()
            tvPaymentUserBalance.text = "IDR -"
        })

        userMainBalance.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) paymentMainLoading.visibility = View.VISIBLE
            else paymentMainLoading.visibility = View.GONE
        })
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ivPaymentToggleEyeMainBalance -> {
                if(tvPaymentUserBalance.transformationMethod is PasswordTransformationMethod){
                    tvPaymentUserBalance.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    ivPaymentToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_24)
                } else{
                    tvPaymentUserBalance.transformationMethod = PasswordTransformationMethod.getInstance()
                    ivPaymentToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_off_24)
                }
            }
        }
    }


    private fun paymentMenu() {
        ivPaymentTopBarNotification.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        tvPaymentLastTransactionSeeMore.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }


        llPaymentMerchantsMenu.setOnClickListener{
            val intent = Intent(requireContext(), PaymentMerchantMainActivity::class.java)
            startActivity(intent)
        }

        llPaymentMobileMenu.setOnClickListener{
            val intent = Intent(requireContext(), PaymentMobileMainActivity::class.java)
            startActivity(intent)
        }

        llPaymentCreditCardMenu.setOnClickListener{
            val intent = Intent(requireContext(), PaymentCreditCardMainActivity::class.java)
            startActivity(intent)
        }

        llPaymentElectricMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        llPaymentTvMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        llPaymentTicketMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        llPaymentPdamMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        llPaymentInsuranceMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }
    }

}