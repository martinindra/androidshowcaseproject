package com.aTeam.beasy.View.Maintenance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aTeam.beasy.R

class UnderConstructionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.all_resource_under_construction)
    }
}