package com.aTeam.beasy.View.HomeFeature.Pocket.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.DataItemLastTransaction
import com.aTeam.beasy.Data.Model.Pocket.DataPocketHistory
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import kotlinx.android.synthetic.main.pocket_history_planning_item.view.*

class PocketByIdHistoryItemListAdapter : RecyclerView.Adapter<PocketByIdHistoryItemListAdapter.ViewHoder>() {

    var dataPocketHistory : ArrayList<DataPocketHistory> = arrayListOf()
    private var functionResource : FunctionResource = FunctionResource()

    fun setData (dataPocketHistory : ArrayList<DataPocketHistory>){
        this.dataPocketHistory = dataPocketHistory
        notifyDataSetChanged()
    }
    inner class ViewHoder(view : View) : RecyclerView.ViewHolder(view) {
        val title = itemView.tvTitleHistoryPocket
        val typeAndAmount = itemView.tvPlusMinusHistoryPocket
        val dueDate = itemView.tvDueDateHistoryPocket
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.pocket_history_planning_item, parent, false)
        return ViewHoder(view)
    }

    override fun onBindViewHolder(holder: ViewHoder, position: Int) {
        val dataPocketHistoryPosition = dataPocketHistory.get(position)

        holder.title.text = dataPocketHistoryPosition.pocketTransactionType
        holder.dueDate.text = dataPocketHistoryPosition.date

        var type : String = "+"
        if (dataPocketHistoryPosition.pocketBalanceStatus == "PLUS") type = "+"
        if (dataPocketHistoryPosition.pocketBalanceStatus == "MINUS") type = "-"

        holder.typeAndAmount.text = type + " " + functionResource.rupiah(dataPocketHistoryPosition.amount?.toDouble()!!).toString()
    }

    override fun getItemCount(): Int = dataPocketHistory.size
}