package com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Fragment.GamificationDetailRewardMissionMyReward
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Fragment.GamificationDetailRewardMissionNextMission
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Fragment.GamificationDetailRewardMissionSuccessMission


class GamificationDetailRewardMissionPagerAdapter(fm : FragmentManager?) :
    FragmentPagerAdapter(fm!!)
{

    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                GamificationDetailRewardMissionSuccessMission()
            }
            1 -> {
                GamificationDetailRewardMissionNextMission()
            }
            2 -> {
                GamificationDetailRewardMissionMyReward()
            }
            else -> {
                GamificationDetailRewardMissionSuccessMission()
            }
        }
    }
}