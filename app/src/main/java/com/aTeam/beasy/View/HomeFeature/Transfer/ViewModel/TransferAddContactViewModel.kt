package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseAddContact
import com.aTeam.beasy.Data.Repository.Transfer.TransferContactRepository

class TransferAddContactViewModel : ViewModel(){

    val addContactRepository = TransferContactRepository()

    val responseData = MutableLiveData<ResponseAddContact>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun addContactViewModel(header:String, name:String, bankId:String, accountNumber:String){
        isLoading.value = true
        addContactRepository.addContact(tokenUser = header, name = name, bankid = bankId, accountNumber = accountNumber ,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}