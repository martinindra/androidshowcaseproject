package com.aTeam.beasy.View.Gamification.DetailRewardGamification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aTeam.beasy.R
import kotlinx.android.synthetic.main.gamification_detail_reward_activity.*

class GamificationDetailRewardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gamification_detail_reward_activity)

        
        gamification_detail_reward_back_icon.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}