package com.aTeam.beasy.View.Login.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthForgotPassword
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthLoginSuccess
import com.aTeam.beasy.Data.Repository.Auth.ForgotPasswordRepository

class ForgotPasswordViewModel : ViewModel() {
    val forgotPasswordRepository = ForgotPasswordRepository()

    val responseData = MutableLiveData<ResponseAuthForgotPassword>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun forgotPassword(email : String, accountNumber : String){
        isLoading.value = true
        forgotPasswordRepository.forgotPassword(email,accountNumber,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}