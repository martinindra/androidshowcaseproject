package com.aTeam.beasy.View.HomeFeature.Payment.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Payment.Merchant.ResponsePaymentMerchantList
import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Data.Repository.Payment.PaymentMainRepository


class PaymentMainViewModel : ViewModel() {

    val paymentMainRepository = PaymentMainRepository()

    val responsePaymentLastTransaction = MutableLiveData<ResponsePaymentLastTransaction>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getLastTransactionPayment(
        header : String
    ){
        isLoading.value = true

        paymentMainRepository.paymentLastTransaction(
            tokenUser = header,
            {
                isLoading.value = false
                responsePaymentLastTransaction.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

}