package com.aTeam.beasy.View.HomeFeature.Payment.CreditCard

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.CreditCard.DataCreditCardBill
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Payment.CreditCard.ViewModel.PaymentCreditCardViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payment_credit_card_main_activity.*

class PaymentCreditCardMainActivity : AppCompatActivity() {
    //token
    private lateinit var session: SessionManager

    //viewModel
    private lateinit var paymentCreditCardViewModel: PaymentCreditCardViewModel

    //data
    private lateinit var responseCreditcardBill : ArrayList<DataCreditCardBill>

    //function rupiah resource
    private var functionResource: FunctionResource = FunctionResource()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_credit_card_main_activity)
        paymentCreditCardViewModel = ViewModelProviders.of(this).get(PaymentCreditCardViewModel::class.java)
        session = SessionManager(this)
        responseCreditcardBill = arrayListOf()

        ivPaymentCreditCardBack.setOnClickListener {
            onBackPressed()
        }

        edtPaymentCreditCardNumberCard.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! >= 10){
                    btnPaymentCreditCardNext.isEnabled = true
                    checkButton()
                }else{
                    btnPaymentCreditCardNext.isEnabled = false
                    checkButton()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s?.length!! >= 10){
                    checkButton()

                }else{
                    btnPaymentCreditCardNext.isEnabled = false
                    checkButton()
                }
            }
        })


        btnPaymentCreditCardNext.setOnClickListener {
            checkViewModel()
            edtPaymentCreditCardNumberCard.isEnabled = false
            btnPaymentCreditCardNext.visibility = View.GONE
            btnPaymentCreditCardPay.visibility = View.VISIBLE
        }

        btnPaymentCreditCardPay.setOnClickListener {
            val paymentAmount = edtPaymentCreditCardPaymentAmount.text.toString().toInt()
            val minimumPayment = responseCreditcardBill.get(0).minimumPayment?.toInt()
            val totalPayment = responseCreditcardBill.get(0).billPayment?.toInt()
            var inputError = false

            if (paymentAmount == 0){
                inputError = true
                edtPaymentCreditCardPaymentAmount.setBackgroundResource(R.drawable.all_resource_bg_white_fillform_error)
                tvPaymentCreditCardPaymentAmountMessage.visibility = View.VISIBLE
                tvPaymentCreditCardPaymentAmountMessage.text = "Please fill payment amount"
            }
            if (!inputError) {
                if (paymentAmount >= minimumPayment!! && paymentAmount <= totalPayment!!) {
                    Log.d("LihatDataS", responseCreditcardBill.toString())
                    tvPaymentCreditCardPaymentAmountMessage.visibility = View.GONE
                    edtPaymentCreditCardPaymentAmount.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                    val intent = Intent(this, PaymentCreditCardPayConfirmationActivity::class.java)
                    intent.putExtra(
                        PaymentCreditCardPayConfirmationActivity.EXTRA_DATA,
                        responseCreditcardBill
                    )
                    intent.putExtra(
                        PaymentCreditCardPayConfirmationActivity.PAYMENT_AMOUNT,
                        paymentAmount.toString()
                    )
                    startActivity(intent)
                } else {
                    edtPaymentCreditCardPaymentAmount.setBackgroundResource(R.drawable.all_resource_bg_white_fillform_error)
                    tvPaymentCreditCardPaymentAmountMessage.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun checkViewModel() {
        session.login?.let {
            paymentCreditCardViewModel.getPaymentCreditCardBill(
                header = it,
                ccNumber = edtPaymentCreditCardNumberCard.text.toString()
            )
        }

        paymentCreditCardViewModel.responseData.observe(this, Observer {
            responseCreditcardBill.clear()
            responseCreditcardBill.add(it.data!!)
            rlPaymentCreditCardShowBill.visibility = View.VISIBLE
            bindTextFromResult(it.data)
        })

        paymentCreditCardViewModel.responseError.observe(this, Observer {
            Snackbar.make(clPaymentCreditCardBillContext, "CC Number is Unknown", Snackbar.LENGTH_LONG).show()
        })
    }

    private fun bindTextFromResult(data: DataCreditCardBill) {
        tvPaymentCreditCardShowBillCardName.text = data.name
        tvPaymentCreditCardShowBillCardBankInfo.text = "${data.bank} ${data.creditCardNumber}"
        tvPaymentCreditCardShowBillPaymentAmount.text = functionResource.rupiah(data.billPayment?.toDouble()!!).toString()
        tvPaymentCreditCardShowMinimumPaymentAmount.text = functionResource.rupiah(data.minimumPayment?.toDouble()!!).toString()
        tvPaymentCreditCardShowMinimumPaymentDueDate.text = "due date : ${data.on}"
    }

    private fun checkButton() {
        if (btnPaymentCreditCardNext.isEnabled == true){
            btnPaymentCreditCardNext.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
            btnPaymentCreditCardNext.setTextColor(Color.parseColor("#ffffff"))
        }else{
            btnPaymentCreditCardNext.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
            btnPaymentCreditCardNext.setTextColor(Color.parseColor("#000000"))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}