package com.aTeam.beasy.View.OnBoarding.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager.widget.PagerAdapter
import com.aTeam.beasy.Data.Model.SliderModel.SliderOnboardModel
import com.aTeam.beasy.R
import kotlinx.android.synthetic.main.onboard_slider_item.view.*

class OnBoardSliderAdapter (private val context: Context, private val dataListSlider : ArrayList<SliderOnboardModel>) : PagerAdapter() {
    override fun getCount(): Int = dataListSlider.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.onboard_slider_item, container, false)

        //get data
        val model = dataListSlider[position]
        val title = model.title
        val description = model.description
        val image = model.image

        //set data to UI view
        view.onboard_slider_item_image.setImageResource(image)
        view.onboard_slider_title_text.text = title

        //add view to container
        container.addView(view, position)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}