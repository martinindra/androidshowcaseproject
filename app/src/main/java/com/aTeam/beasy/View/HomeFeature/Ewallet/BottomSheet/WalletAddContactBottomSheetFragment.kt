package com.aTeam.beasy.View.HomeFeature.Ewallet.BottomSheet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Ewallet.DataItemEwallet
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletAddContactViewModel
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletAddGetContactByIdViewModel
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletListViewModel
import com.aTeam.beasy.View.HomeFeature.Ewallet.WalletMainActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.transfer_add_contact_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.wallet_add_account_bottom_sheet_fragment.*

class WalletAddContactBottomSheetFragment(var stateChangeWallet : () -> Unit) : BottomSheetDialogFragment() {
    //token
    private lateinit var sessionWallet: SessionManager

    //parent fragment
    private lateinit var walletMainActivity : WalletMainActivity

    //viewModel
    private lateinit var accountWalletListViewModel: WalletListViewModel
    private lateinit var walletAddContactViewModel: WalletAddContactViewModel
    private lateinit var walletAddGetContactByIdViewModel: WalletAddGetContactByIdViewModel

    private lateinit var walletData : ArrayList<DataItemEwallet>
    private lateinit var walletName : ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.wallet_add_account_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewmodel
        accountWalletListViewModel = ViewModelProviders.of(this).get(WalletListViewModel::class.java)
        walletAddContactViewModel = ViewModelProviders.of(this).get(WalletAddContactViewModel::class.java)
        walletAddGetContactByIdViewModel = ViewModelProviders.of(this).get(WalletAddGetContactByIdViewModel::class.java)


        //mainfragment
        walletMainActivity = WalletMainActivity()

        //e-wallet
        walletName = arrayListOf()

        //get session data
        sessionWallet = SessionManager(requireContext())

        //e-wallet list
        walletList()


        var walletNameSelected : String? = null
        var walletIdSelected : String? = null

        wallet_edt_select_wallet_contact.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                walletNameSelected = walletData[p2].ewalletName.toString()
                walletIdSelected = walletData[p2].ewalletId.toString()
            }

        }
        
        btnWalletAddRecipient.setOnClickListener {
            var errorAdd = false
            if (wallet_edt_name_contact.text.toString().trim().isEmpty()){
                wallet_edt_name_contact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }
            if (etWalletAccountNumberContact.text.toString().trim().isEmpty()){
                etWalletAccountNumberContact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }

            if (etWalletAccountNumberContact.text.toString().length < 10) {
                etWalletAccountNumberContact.error = "Please enter correct number"
                etWalletAccountNumberContact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }

            if (!errorAdd){
                sessionWallet.login?.let { it1 -> walletAddContactViewModel.addWalletViewModel(
                    header = it1,
                    name = wallet_edt_name_contact.text.toString(),
                    eWalletId = walletIdSelected.toString(),
                    accountNumberWallet = etWalletAccountNumberContact.text.toString()
                ) }

                walletAddContactViewModel.responseData.observe(viewLifecycleOwner, Observer {

//                    Snackbar.make(requireActivity().findViewById(R.id.rlMainHomeBaseContent),  wallet_edt_name_contact.text.toString() + " Success Added", Snackbar.LENGTH_LONG).show()
                    Toast.makeText(requireContext(), "Success add data", Toast.LENGTH_SHORT).show()
                    dismiss()
                    wallet_edt_name_contact.setText("")
                    etWalletAccountNumberContact.setText("")
                    stateChangeWallet()
                })
                walletAddContactViewModel.errorData.observe(viewLifecycleOwner, Observer {
                    Toast.makeText(requireContext(), wallet_edt_name_contact.text.toString() + " Error Add", Toast.LENGTH_LONG).show()
                })

                walletAddContactViewModel.isLoading.observe(viewLifecycleOwner, Observer {
                    if (it == true) skAddEditLoadingWallet.visibility = View.VISIBLE
                    else skAddEditLoadingWallet.visibility = View.GONE
                })
            }
        }
    }

    private fun walletList() {
        sessionWallet.login?.let { accountWalletListViewModel.getWalletData(header = it) }
        accountWalletListViewModel.responseData.observe(viewLifecycleOwner, Observer {
            walletData = it.data as ArrayList<DataItemEwallet>
            for (i in walletData.indices){
                walletName.add(walletData.get(i).ewalletName.toString())
            }
            wallet_edt_select_wallet_contact.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, walletName)
            Log.d("dataWallet", walletData.toString())
        })

        accountWalletListViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlWalletAddBottomContent),  "E-Wallet data is null", Snackbar.LENGTH_LONG).show()
        })
        accountWalletListViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            btnWalletAddRecipient.isEnabled = it != true
        })
    }

    override fun dismiss() {
        super.dismiss()
    }
}
