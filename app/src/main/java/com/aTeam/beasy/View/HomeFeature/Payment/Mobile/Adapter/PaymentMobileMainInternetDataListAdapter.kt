package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.DataItemInternetData
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import kotlinx.android.synthetic.main.payment_mobile_internet_data_list_item.view.*

class PaymentMobileMainInternetDataListAdapter (var callback : (id : String?) -> Unit) :
    RecyclerView.Adapter<PaymentMobileMainInternetDataListAdapter.ViewHolder>() {

    var dataInternetData: ArrayList<DataItemInternetData> = arrayListOf()

    private var lastChecked: ConstraintLayout? = null
    private var titleChecked : TextView? = null
    private var descriptionChecked: TextView? = null
    private var seeDetail: TextView? = null
    private var totalChecked: TextView? = null
    private var lastCheckedPos = -1

    private var functionResource: FunctionResource = FunctionResource()
    fun setData(dataInternet: ArrayList<DataItemInternetData>) {
        this.dataInternetData = dataInternet
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = itemView.tvPaymentMainMobileInternetDataName
        val description = itemView.tvPaymentMainInternetDataDescription
        val totalPayment = itemView.tvPaymentMainInternetDataTotal
        val seeDetail = itemView.tvPaymentMainInternetSeeDetail
        val layout = itemView.clPaymentMobileInternetData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.payment_mobile_internet_data_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataInternetData.get(position)
        holder.title.text = data.name
        holder.description.text = data.description
        holder.totalPayment.text = "Pay : " + functionResource.rupiah(data.price?.toInt()?.toDouble()!!).toString()

        holder.itemView.setOnClickListener {
            val id = data.id
            callback(id)
            if (lastCheckedPos == -1) {
                lastCheckedPos = position
                lastChecked = holder.layout
                titleChecked = holder.title
                descriptionChecked = holder.description
                totalChecked = holder.totalPayment
                seeDetail = holder.seeDetail
                holder.layout.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                holder.title.setTextColor(Color.parseColor("#ffffff"))
                holder.description.setTextColor(Color.parseColor("#ffffff"))
                holder.seeDetail.setTextColor(Color.parseColor("#ffffff"))
                holder.totalPayment.setTextColor(Color.parseColor("#ffffff"))
            } else {
                lastChecked?.setBackgroundResource(R.drawable.all_resource_payment_white_bg)
                titleChecked?.setTextColor(Color.parseColor("#000000"))
                descriptionChecked?.setTextColor(Color.parseColor("#000000"))
                seeDetail?.setTextColor(Color.parseColor("#000000"))
                totalChecked?.setTextColor(Color.parseColor("#000000"))
                holder.layout.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                holder.title.setTextColor(Color.parseColor("#ffffff"))
                holder.description.setTextColor(Color.parseColor("#ffffff"))
                holder.seeDetail.setTextColor(Color.parseColor("#ffffff"))
                holder.totalPayment.setTextColor(Color.parseColor("#ffffff"))
                lastCheckedPos = position
                lastChecked = holder.layout
                titleChecked = holder.title
                descriptionChecked = holder.description
                totalChecked = holder.totalPayment
                seeDetail = holder.seeDetail
            }
        }
    }


    override fun getItemCount(): Int = this.dataInternetData.size
}