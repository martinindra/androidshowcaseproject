package com.aTeam.beasy.View.Gamification.MainGamification.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aTeam.beasy.R
import kotlinx.android.synthetic.main.gamification_main_second_path_fragment.*

class GamificationMainSecondPathFragment(var planetSequence : Int) : Fragment() {

    lateinit var dataPlot : ArrayList<Int>
    var currentPosition : Int = planetSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gamification_main_second_path_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataPlot = arrayListOf()
        dataPlotImage()
    }

    private fun dataPlotImage() {
        dataPlot.clear()
        dataPlot.add(R.drawable.gamification_plot_oke)
        dataPlot.add(R.drawable.gamification_plot_locked)
        dataPlot.add(R.drawable.gamification_plot_locked)
        dataPlot.add(R.drawable.gamification_plot_locked)
        dataPlot.add(R.drawable.gamification_plot_locked)
        dataPlot.add(R.drawable.gamification_plot_locked)

        for (i in dataPlot.indices){
            if (i + 7 == currentPosition){
                dataPlot.set(i, R.drawable.gamification_beasy_rocket)
            } else if (i + 7 < currentPosition){
                dataPlot.set(i, R.drawable.gamification_plot_oke)
            } else if (i + 7 > currentPosition){
                dataPlot.set(i, R.drawable.gamification_plot_locked)
            }
        }

        plot_planet_7.setImageResource(dataPlot.get(0))
        plot_planet_8.setImageResource(dataPlot.get(1))
        plot_planet_9.setImageResource(dataPlot.get(2))
        plot_planet_10.setImageResource(dataPlot.get(3))
        plot_planet_11.setImageResource(dataPlot.get(4))
        plot_planet_12.setImageResource(dataPlot.get(5))
    }
}