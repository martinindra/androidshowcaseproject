package com.aTeam.beasy.View.Gamification.DetailMissionGamification

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Gamification.DataDetailRewardPlanet
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.ViewModel.GamificationDetailMissionViewModel
import kotlinx.android.synthetic.main.gamification_detail_mission_reward_activity.*
import kotlinx.android.synthetic.main.gamification_main_activity.*

class GamificationDetailMissionRewardActivity : AppCompatActivity() {
    companion object{
        const val IDREWARD = "planet_id"
        const val ISCLAIMABLE = "no"
    }

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var gamificationViewModel : GamificationDetailMissionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gamification_detail_mission_reward_activity)

        val idReward = intent.getStringExtra(IDREWARD)
        val isClaimable = intent.getStringExtra(ISCLAIMABLE)

        sessionManager = SessionManager(this)
        gamificationViewModel = ViewModelProviders.of(this).get(GamificationDetailMissionViewModel::class.java)

        if (isClaimable == "yes"){
            btnGamificationDetailMissionReward.visibility = View.VISIBLE
        }

        bindData(idReward)

        ivGamificationDetailMissionRewardClose.setOnClickListener {
            onBackPressed()
        }


    }

    private fun bindData(idReward: String?) {
        sessionManager.login?.let {
            gamificationViewModel.getGamificationDetailRewardPlanet(
                header = it,
                idReward = idReward!!
            )
        }

        responseBindApi()
    }

    private fun responseBindApi() {
        gamificationViewModel.responseGamificationDetailRewardPlanet.observe(this, Observer {
            when (it.data?.type) {
                "TRANSFER" -> {
                    ivGamificationDetailMissionRewardImage.setImageResource(R.drawable.gamification_reward_money_transaction)
                }

                "DISCOUNT" -> {
                    ivGamificationDetailMissionRewardImage.setImageResource(R.drawable.gamification_reward_voucher)
                }

                "CASHBACK" -> {
                    ivGamificationDetailMissionRewardImage.setImageResource(R.drawable.gamification_reward_money_bag)
                }
            }


            if (it.data?.tnc?.isNotEmpty()!! || it.data.tnc.isNotBlank() || it.data.tnc != ""){
                tvGamificationDetailMissionRewardTermAndCondition.text = it.data.tnc
            } else {
                tvGamificationDetailMissionRewardTermAndCondition.visibility = View.GONE
            }

            if(it.data.isClaimed == false){
                tvGamificationDetailMissionRewardText.text = it.data.wording
            }else {
                if (it.data.type == "DISCOUNT") tvGamificationDetailMissionRewardText.text = it.data.wording
                else tvGamificationDetailMissionRewardText.text = "Your reward has been claimed"

                tvGamificationDetailMissionRewardTermAndCondition.visibility = View.GONE
                btnGamificationDetailMissionReward.visibility = View.GONE
            }
            btnClaim(it.data)

        })

        gamificationViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to get Reward Information", Toast.LENGTH_SHORT).show()
            gamificationDetailMissionRewardErrorFound.visibility = View.VISIBLE
        })

        gamificationViewModel.isLoading.observe(this, Observer {
            if (it == true){
                gamificationDetailMissionRewardLoading.visibility = View.VISIBLE
            }else {
                gamificationDetailMissionRewardLoading.visibility = View.GONE
            }
        })

    }

    private fun btnClaim(data: DataDetailRewardPlanet) {
        btnGamificationDetailMissionReward.setOnClickListener {
            sessionManager.login?.let { it1 ->
                gamificationViewModel.postGamificationClaimReward(
                    header = it1,
                    idReward = data.id!!
                )
            }

            rewardPostProcessResponse()
        }

        gamificationViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to Claim Reward", Toast.LENGTH_SHORT).show()
        })

        gamificationViewModel.isLoading.observe(this, Observer {
            if (it == true)
                gamificationDetailMissionRewardLoading.visibility = View.VISIBLE
            else
                gamificationDetailMissionRewardLoading.visibility = View.GONE
        })
    }

    private fun rewardPostProcessResponse() {
        gamificationViewModel.responseDataGamificationClaimReward.observe(this, Observer {
            tvGamificationDetailMissionRewardTermAndCondition.visibility = View.GONE
            tvGamificationDetailMissionRewardText.text = "Your reward has been claimed"
            tvGamificationDetailMissionRewardText.setTextColor(Color.parseColor("#46AE3D"))
            lvGamificationDetailMissionRewardSuccessButtonAnimation.visibility = View.VISIBLE
            btnGamificationDetailMissionReward.visibility = View.GONE

            if (it.data?.additionalInformation?.toString() != null ){
                tvGamificationDetailMissionRewardRefCode.visibility = View.VISIBLE
                tvGamificationDetailMissionRewardRefCode.text = it.data?.additionalInformation
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}