package com.aTeam.beasy.View.HomeFeature.Transfer.Adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet.TransferMenuBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Transfer.TransferPageActivity
import kotlinx.android.synthetic.main.transfer_contact_item.view.*

class TransferContactAdapter(private var fragmentManager: FragmentManager, var callBackAdapter : () -> Unit) : RecyclerView.Adapter<TransferContactAdapter.ViewHolder>(), Filterable {
    var dataContact : ArrayList<DataItemContact> = arrayListOf()
    var dataContactFilter : ArrayList<DataItemContact> = arrayListOf()

    fun setData (dataContact : ArrayList<DataItemContact>){
        this.dataContact = dataContact
        this.dataContactFilter = dataContact
        notifyDataSetChanged()
    }
    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var imageAvatar = itemView.avTransferItemAvatar
        var nameAvatar = itemView.tvTransferItemName
        var bankNameAvatar = itemView.tvBankName
        var moreButton = itemView.ibTransferItemMoreButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.transfer_contact_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var dataContact = this.dataContact.get(position)
        holder.imageAvatar.setText(dataContact.name.toString())
        holder.nameAvatar.text = dataContact.name
        holder.bankNameAvatar.text = dataContact.bankName + " " + dataContact.accountNumber

        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, TransferPageActivity::class.java)
            intent.putExtra(TransferPageActivity.EXTRA_DATA, dataContact)
            it.context.startActivities(
                arrayOf(intent)
            )
        }

        holder.moreButton.setOnClickListener {
            val transferBottomSheetFragment = TransferMenuBottomSheetFragment(dataContact
            ) {
                callBackAdapter()
            }
            transferBottomSheetFragment.show(fragmentManager, "BottomSheetDialog")
        }
    }

    override fun getItemCount(): Int = dataContact.size
    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
                var filterResults = Filter.FilterResults()
                Log.d("HasilFilterQuery", constraint.toString())

                if (constraint == null || constraint.length == 0) {
                    Log.d("HasilFilterQuery", "disini")
                    Log.d("HasilFilterQuery", dataContactFilter.toString())
                    filterResults.count = dataContactFilter.size
                    filterResults.values = dataContactFilter
                } else {
                    var dataBaru = ArrayList<DataItemContact>()
                    var queryString = constraint.toString().toLowerCase()
                    for (row in dataContactFilter) {
                        if (row.name?.toLowerCase()?.contains(queryString)!!
                        ) {
                            dataBaru.add(row)
                        }
                    }
                    filterResults.count = dataBaru.size
                    filterResults.values = dataBaru
                }

                Log.d("HasilFilterData", filterResults.values.toString())
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataContact = results!!.values as ArrayList<DataItemContact>
                notifyDataSetChanged()
            }
        }
    }
    
}