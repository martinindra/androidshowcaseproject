package com.aTeam.beasy.View.HomeFeature.Transfer

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.TransferConfirmationViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.login_main_activity.*
import kotlinx.android.synthetic.main.payment_mobile_main_activity.*
import kotlinx.android.synthetic.main.transfer_confirmation_cancel_dialog.view.*
import kotlinx.android.synthetic.main.transfer_confirmation_otp_dialog.*
import kotlinx.android.synthetic.main.transfer_confirmation_otp_dialog.view.*
import kotlinx.android.synthetic.main.transfer_confirmation_page_activity.*
import kotlinx.android.synthetic.main.transfer_confirmation_page_activity.view.*
import kotlinx.android.synthetic.main.transfer_page_activity.*
import java.util.*
import javax.inject.Inject

//TODO change AppCompactActivity to DaggerAppCompactActivity
class TransferConfirmationPageActivity : DaggerAppCompatActivity() {

    //Inject Function Resource
    @Inject
    lateinit var functionResource : FunctionResource

    companion object{
        const val CONTACT_DATA = "extra_data"
        const val TRANSFER_AMOUNT = "transfer_amount"
        const val TRANSFER_NOTE = "transfer_note"
    }

    //viewmodel
    private lateinit var transferConfirmationViewModel: TransferConfirmationViewModel

    //session
    private lateinit var session: SessionManager

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transfer_confirmation_page_activity)
        supportActionBar?.hide()

        val window : Window = this@TransferConfirmationPageActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@TransferConfirmationPageActivity, R.color.status_bar_blue)

        //viewmodel
        transferConfirmationViewModel = ViewModelProviders.of(this).get(TransferConfirmationViewModel::class.java)

        //session
        session = SessionManager(this)

        val contactData = intent.getParcelableExtra<DataItemContact>(CONTACT_DATA) as DataItemContact
        val transferAmount = intent.getStringExtra(TRANSFER_AMOUNT)
        val transferNote = intent.getStringExtra(TRANSFER_NOTE)
        val transferCost = contactData.cost

        val transferAmountDebited = transferAmount?.toInt()!! + transferCost?.toInt()!!

        avTransferConfrimationAvatar.setText(contactData.name.toString())
        tvTransferConfirmationName.text = contactData.name.toString()
        tvTransferConfirmationRekening.text = contactData.bankName.toString()
        tvTransferConfirmationAmount.text = functionResource.rupiah(transferAmount.toDouble())
        tvTransferConfirmationCost.text = contactData.cost?.let { functionResource.rupiah(it.toDouble()) }
        tvTransferConfirmationAmountDebited.text = functionResource.rupiah(transferAmountDebited.toDouble())
        tvTransferConfirmationNote.text = transferNote

        btnTransferConfirmationNextButton.setOnClickListener {
            showCreateCategoryDialog(transferAmount, transferNote, contactData)
        }

        btnTransferConfirmationCancelButton.setOnClickListener {
            showCancelDialog()
        }

        backTransferConfirmation.setOnClickListener {
            onBackPressed()
        }

    }

    private fun showCancelDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)

        val view = layoutInflater.inflate(R.layout.transfer_confirmation_cancel_dialog, null)

        var dialog = builder.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferConfirmationCancelDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnTransferConfirmationCancelDialogSubmit.setOnClickListener {
            val intent = Intent(context, MainBaseActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }

        dialog.show()
    }

    fun showCreateCategoryDialog(
        transferAmount: String,
        transferNote: String?,
        contactData: DataItemContact
    ) {
        val context = this
        val builder = AlertDialog.Builder(context)

        val view = layoutInflater.inflate(R.layout.transfer_confirmation_otp_dialog, null)
        var pinError = false

        var dialog = builder.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferConfirmationDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnTransferConfirmationDialogSubmit.setOnClickListener {

            val pin = view.pvTransferConfirmationOtp.text.toString()

            if (pin.length != 6){
                pinError = true
                Toast.makeText(this, "Your pin is less than 6!", Toast.LENGTH_SHORT).show()

            }else if (pin.isEmpty()){
                pinError = true
                Toast.makeText(this, "Your pin is empty!", Toast.LENGTH_SHORT).show()

            }else{
                view.btnTransferConfirmationDialogSubmit.isEnabled = false
                session.login?.let { it1 -> transferConfirmationViewModel.addContactViewModel(it1,
                    contactId = contactData.id.toString(),
                    amount = transferAmount.toInt(),
                    note = transferNote.toString(),
                    pin = pin.toInt()
                ) }

                checkTransfer(view, transferAmount, transferNote, contactData)
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    private fun checkTransfer(
        view: View,
        transferAmount: String,
        transferNote: String?,
        contactData: DataItemContact
    ) {
        transferConfirmationViewModel.responseData.observe(this, androidx.lifecycle.Observer {
            val moveConfirmationIntent = Intent(this@TransferConfirmationPageActivity, TransferResultActivity::class.java)
            finish()
            moveConfirmationIntent.putExtra(TransferResultActivity.TRANSFER_RESULT, it.data)
            startActivity(moveConfirmationIntent)
            Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
        })

        transferConfirmationViewModel.errorData.observe(this, androidx.lifecycle.Observer {
            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
        })

        transferConfirmationViewModel.isLoading.observe(this, androidx.lifecycle.Observer {
            if (it == true) loadingWaitTransferConfirmation.visibility = View.VISIBLE
            else {
                loadingWaitTransferConfirmation.visibility = View.GONE
                showCreateCategoryDialog(transferAmount, transferNote, contactData)
            }
        })
    }

}