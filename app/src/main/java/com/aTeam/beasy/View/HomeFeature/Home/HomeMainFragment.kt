package com.aTeam.beasy.View.HomeFeature.Home

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Gamification.DataGamificationItemUserStatus
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.Gamification.MainGamification.GamificationMainActivity
import com.aTeam.beasy.View.Gamification.MainGamification.ViewModel.GamificationMainViewModel
import com.aTeam.beasy.View.HomeFeature.Base.ViewModel.BaseMainUserViewModel
import com.aTeam.beasy.View.HomeFeature.Ewallet.WalletMainActivity
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.home_main_fragment.*

class HomeMainFragment : Fragment(), View.OnClickListener {

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    //gamification
    private lateinit var gamificationViewModel : GamificationMainViewModel

    //main user
    private lateinit var userMainBalance : BaseMainUserViewModel

    //function resource
    private var functionResource : FunctionResource = FunctionResource()

    //card promotion
    private var modelList = arrayListOf<Int>(R.drawable.home_promo_1, R.drawable.home_promo_2, R.drawable.home_promo_3)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(requireContext())
        gamificationViewModel = ViewModelProviders.of(this).get(GamificationMainViewModel::class.java)
        userMainBalance = ViewModelProviders.of(this).get(BaseMainUserViewModel::class.java)

        ivHomeToggleEyeMainBalance.setOnClickListener(this)
        gamificationView()
        userMainBalanceView()

        carouselPromo()
        menuHome()
    }

    private fun carouselPromo() {
        crImagePromo.apply {
            size = modelList.size
            resource = R.layout.home_promotion_item
            setCarouselViewListener { view, position ->
                val imageView = view.findViewById<ImageView>(R.id.background_card_promotion)
                imageView.setImageDrawable(resources.getDrawable(modelList.get(position)))
            }
            show()
        }
    }

    private fun menuHome() {
        llHomeEwalletMenu.setOnClickListener{
            val intent = Intent(requireContext(), WalletMainActivity::class.java)
            startActivity(intent)
        }

        llHomeTopUpMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }
        llHomeHistoryMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        llHomeRequestMenu.setOnClickListener{
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ivHomeToggleEyeMainBalance -> {
                if(tvHomeUserBalance.transformationMethod is PasswordTransformationMethod){
                    tvHomeUserBalance.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    ivHomeToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_24)
                } else{
                    tvHomeUserBalance.transformationMethod = PasswordTransformationMethod.getInstance()
                    ivHomeToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_off_24)
                }
            }
        }
    }

    private fun userMainBalanceView() {
        sessionManager.login?.let { userMainBalance.getUserMainBalance(header = it) }
        userMainBalance.responseUserViewModel.observe(viewLifecycleOwner, Observer {
            Log.d("coba", it.data.toString())
            val userBalance = it.data?.balance
            tvHomeUserBalance.text = functionResource.rupiah(userBalance?.toDouble()!!).toString()
        })

        userMainBalance.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get user balance", Toast.LENGTH_SHORT).show()
            tvHomeUserBalance.text = "IDR -"
        })

        userMainBalance.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) mainHomeLoading.visibility = View.VISIBLE
            else mainHomeLoading.visibility = View.GONE
        })
    }

    private fun gamificationView() {
        sessionManager.login?.let {
            gamificationViewModel.getGamificationStatusUser(
                header = it
            )
        }

        responseGamification()
    }

    private fun responseGamification() {
        gamificationViewModel.responseGamificationStatusUser.observe(viewLifecycleOwner, Observer {
            tvHomeGamificationText.text = it.data?.planetWording.toString()
            Glide.with(requireContext()).load(it.data?.gamificationAnimation).into(ivHomeGamificationAnimation)
            buttonGamification(it.data)
        })

        gamificationViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Error get Gamification Data", Toast.LENGTH_SHORT).show()
        })
    }

    private fun buttonGamification(data: DataGamificationItemUserStatus?) {
        if (data?.isStartedGamification == false){
            btnHomeGamificationStart.setText("Start Mission")
            btnHomeGamificationStart.setOnClickListener {
                sessionManager.login?.let { it1 -> gamificationViewModel.postGamificationStart(it1) }
                responseGamificationStart()
            }
        }

        if (data?.isOnLastPlanet == true && data?.isStartedGamification == true){
            btnHomeGamificationStart.setText("Next Mission")
            btnHomeGamificationStart.setOnClickListener {
                val intent = Intent(context, GamificationMainActivity::class.java)
                startActivity(intent)
            }
        }

        if (data?.isOnLastPlanet == false && data?.isStartedGamification == true){
            btnHomeGamificationStart.setText("Go To ${data.planetName}")
            btnHomeGamificationStart.setOnClickListener {
                val intent = Intent(context, GamificationMainActivity::class.java)
                startActivity(intent)
            }
        }


    }

    private fun responseGamificationStart() {
        gamificationViewModel.responseGamificationStart.observe(viewLifecycleOwner, Observer {
            gamificationView()
        })

        gamificationViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Cannot start user gamification", Toast.LENGTH_SHORT).show()
        })
        gamificationViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) mainHomeLoading.visibility = View.VISIBLE
            else mainHomeLoading.visibility = View.GONE
        })
    }
}
