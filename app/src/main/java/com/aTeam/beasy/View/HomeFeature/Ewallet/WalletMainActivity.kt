package com.aTeam.beasy.View.HomeFeature.Ewallet

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.R.id.ivBackWalletPageMain
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.HomeFeature.Ewallet.Adapter.WalletContactAdapter
import com.aTeam.beasy.View.HomeFeature.Ewallet.BottomSheet.WalletAddContactBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletMainContactViewModel
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import kotlinx.android.synthetic.main.wallet_main_activity.*


class WalletMainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener,
    View.OnClickListener {
    val changeState = MutableLiveData<Boolean>()
    //token
    private lateinit var sessionWallet: SessionManager

    //viewModel
    private lateinit var walletMainContactViewModel: WalletMainContactViewModel

    private lateinit var dataWalletContactAll : ArrayList<DataItemAccount>
    private lateinit var dataWalletContactRecent : ArrayList<DataItemAccount>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallet_main_activity)

        //back button
        ivBackWalletPageMain.setOnClickListener(this)

        //get token
        sessionWallet = SessionManager(context = this)

        //viewmodel
        walletMainContactViewModel = ViewModelProviders.of(this).get(WalletMainContactViewModel::class.java)
        showAllWallet()
        showRecentWallet()

        dataWalletContactAll = arrayListOf()
        dataWalletContactRecent = arrayListOf()

        val walletBottomSheetFragment = WalletAddContactBottomSheetFragment {
            showAllWallet()
            showRecentWallet()
        }

        btnWalletAddAccount.setOnClickListener {
            walletBottomSheetFragment.show(supportFragmentManager, "BottomSheetDialog")
        }

        //swipe refresh data
        swipeWalletAccount.setOnRefreshListener(this)

        btnSearchAccount.setOnClickListener {
            val intent = Intent(this, WalletSearchContact::class.java)
            intent.putExtra(WalletSearchContact.EXTRA_DATA_WALLET, dataWalletContactAll)
            startActivity(intent)
        }

        changeStateReloadWallet()

    }

    fun changeStateReloadWallet() {
        changeState.observe(this, Observer {
            Log.d("here", "here ${it.toString()}")
            if (it == true){
                showAllWallet()
                showRecentWallet()
                changeState.value = false
            }
        })
    }

    fun showAllWallet() {
        sessionWallet.login?.let { walletMainContactViewModel.getContactData(header = it) }
        walletMainContactViewModel.responseData.observe(this, Observer {
            dataWalletContactAll = it.data as ArrayList<DataItemAccount>
            val newAdapter = WalletContactAdapter(supportFragmentManager
            ) {
                showAllWallet()
            }
            newAdapter.setData(dataEwallet = dataWalletContactAll)
            rvAllAccount.apply {
                adapter = newAdapter
                setHasFixedSize(true)
            }
        })

        walletMainContactViewModel.errorData.observe(this, Observer {
            Log.d("WalletContactError", it.localizedMessage)
        })

        walletMainContactViewModel.isLoading.observe(this, Observer {
            if(it == true) refreshLayoutWallet.visibility = View.VISIBLE
            else refreshLayoutWallet.visibility = View.GONE
        })

    }

    fun showRecentWallet() {
        ivWalletTopBarNotification.setOnClickListener {
            val intent = Intent (this, UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        sessionWallet.login?.let { walletMainContactViewModel.getContactRecent(header = it) }
        walletMainContactViewModel.responseDataRecent.observe(this, Observer {
            Log.d("DataRecentContact", it.data.toString())
            dataWalletContactRecent = it.data as ArrayList<DataItemAccount>
            val adapterRecent = WalletContactAdapter(supportFragmentManager) {
                showRecentWallet()
            }
            adapterRecent.setData(dataEwallet = dataWalletContactRecent)
            rvRecentAccount.apply {
                adapter = adapterRecent
                setHasFixedSize(true)
            }
        })

        walletMainContactViewModel.errorDataRecent.observe(this, Observer {
            Log.d("WalletContactError", it.localizedMessage)
        })

        walletMainContactViewModel.isLoadingRecent.observe(this, Observer {
            if(it == true) refreshLayoutWallet.visibility = View.VISIBLE
            else refreshLayoutWallet.visibility = View.GONE
        })
    }

    override fun onRefresh() {
        showAllWallet()
        showRecentWallet()
        swipeWalletAccount.isRefreshing = false
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.ivBackWalletPageMain -> {
                onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainBaseActivity::class.java)
        startActivity(intent)
    }
}