package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseEditContact
import com.aTeam.beasy.Data.Repository.Transfer.TransferContactRepository

class TransferEditContactViewModel : ViewModel() {
    val transferContactRepository = TransferContactRepository()

    val responseData = MutableLiveData<ResponseEditContact>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun editContact(header:String, name:String, bankId:String, accountNumber:String, id:String){
        isLoading.value = true

        transferContactRepository.editContact(
            tokenUser = header,
            idUser = id,
            name = name,
            bankid = bankId,
            accountNumber = accountNumber,
            {
                isLoading.value = false
                responseData.value = it
            },
            {
                isLoading.value = false
                errorData.value = it
            }
        )
    }
}