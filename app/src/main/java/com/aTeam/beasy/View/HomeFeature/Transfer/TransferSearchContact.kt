package com.aTeam.beasy.View.HomeFeature.Transfer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.Adapter.TransferContactAdapter
import kotlinx.android.synthetic.main.transfer_search_contact_activity.*

class TransferSearchContact : AppCompatActivity() {
    companion object{
        const val EXTRA_DATA = "extra_data"
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transfer_search_contact_activity)

        ivBackTransferSearch.setOnClickListener {
            onBackPressed()
        }

        val dataContact = intent.getParcelableArrayListExtra<DataItemContact>(EXTRA_DATA)

        val adapter = TransferContactAdapter(supportFragmentManager) {
        }
        adapter.setData(dataContact = dataContact as ArrayList<DataItemContact>)
        rvTransferSearchContact.adapter = adapter
        rvTransferSearchContact.setHasFixedSize(true)

        edtTransferSearchSearch.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter.filter(s.toString())
            }
        })

    }
}