package com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pocket_edit_pocket_bottom_sheet_fragment.*
import java.text.SimpleDateFormat
import java.util.*


class PocketEditBottomSheetFragment (
    var pocketId : String,
    var imageProfile : String,
    var nameOfPocket : String,
    var dueDateDDMMYY : String,
    var target : Int,
    var onResultEdit:() -> Unit
): BottomSheetDialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var uploadImageUriEdit: Uri

    private lateinit var sessionManager: SessionManager
    private lateinit var pocketMainViewModel : PocketMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.pocket_edit_pocket_bottom_sheet_fragment, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)

        Glide.with(requireContext()).load(imageProfile).into(ivImageProfileEdit)
        etEnterNameOnEdit.setText(nameOfPocket)
        etDueDateEdit.setText(dueDateDDMMYY)
        Log.d("dueDateDDMMYY", dueDateDDMMYY)
        etAmountTargetOnEditPocket.setText(target.toString())

        buttonDoneOnEditBottomSheet.setOnClickListener{
            var errorEdit = false
            if (etEnterNameOnEdit.text.toString().trim().isEmpty() || etEnterNameOnEdit.text.toString().trim() == ""){
                errorEdit = true
                etEnterNameOnEdit.error = "Please fill pocket name"
            }
            if (etDueDateEdit.text.toString().trim().isEmpty() || etDueDateEdit.text.toString().trim() == ""){
                errorEdit = true
                etDueDateEdit.error = "Please fill pocket duedate"
            }

            if (etAmountTargetOnEditPocket.text.toString().trim().isEmpty() || etAmountTargetOnEditPocket.text.toString().trim() == ""){
                errorEdit = true
                etAmountTargetOnEditPocket.error = "Please fill pocket name"
            }

            if (!errorEdit) processEdit()
        }

        //Date Picker//
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        val dpd = DatePickerDialog(requireContext(), this, year, month, day)

        dpd.datePicker.minDate = Calendar.getInstance().timeInMillis

        etDueDateEdit.setOnClickListener{
            dpd.show()
        }

    }

    private fun processEdit() {
        sessionManager.login?.let {token -> pocketMainViewModel.pocketEdit(
            header = token,
            dueDate = etDueDateEdit.text.toString(),
            name = etEnterNameOnEdit.text.toString(),
            target = etAmountTargetOnEditPocket.text.toString().toInt(),
            pocketId = pocketId
        ) }

        responseEdit()
    }

    private fun responseEdit() {
        pocketMainViewModel.responsePocketEdit.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Toast.makeText(requireContext(), "${it.data?.pocketName} successfully updated", Toast.LENGTH_SHORT).show()
            onResultEdit()
            dismiss()
        })
        pocketMainViewModel.responseErrorData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Toast.makeText(requireContext(), "Failed to update data", Toast.LENGTH_SHORT).show()
        })
        pocketMainViewModel.isLoading.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it == true) skEditLoading.visibility = View.VISIBLE
            else skEditLoading.visibility = View.GONE
        })
    }

    fun onClick(v: View?) {

        when(v?.id){

            //Upload Image With Image View//
            R.id.ivImageProfileEdit -> {
                pickImageGalleryEdit()
            }

            //Upload Image With Text View//
            R.id.tvTextForImagePickerOnEditPocket -> {
                pickImageGalleryEdit()
            }
        }
    }

    //Image Picker
    private fun pickImageGalleryEdit (){

        ivImageProfileEdit.setOnClickListener {
            ImagePicker.with(this)
                .galleryMimeTypes(mimeTypes = arrayOf("image/png", "image/jpg", "image/jpeg"))
                .crop()
                .cropSquare()
                .compress(1024)
                .maxResultSize(1080, 1080)
                .start()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uploadImageUriEdit = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            ivImageProfileEdit?.setImageURI(uploadImageUriEdit)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    //Date Picker
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar[year, month] = dayOfMonth
        val sdf = SimpleDateFormat("dd MMMM yyyy", Locale("en"))
        val formattedDateEdit = sdf.format(calendar.time)
        etDueDateEdit.setText(formattedDateEdit)
    }

}