package com.aTeam.beasy.View.HomeFeature.Ewallet.BottomSheet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.Data.Model.Ewallet.DataItemEwallet
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletEditContactViewModel
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletListViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.wallet_add_account_bottom_sheet_fragment.*

class WalletEditContactBottomSheetFragment(
    var arrayContactData: DataItemAccount,
    var parentMenuFragment: BottomSheetDialogFragment,
    var callbackEdit: () -> Unit
    ) : BottomSheetDialogFragment() {

    //token
    private lateinit var session: SessionManager

    //viewModel
    private lateinit var walletListViewModel: WalletListViewModel
    private lateinit var walletEditContactViewModel: WalletEditContactViewModel

    private lateinit var walletData : ArrayList<DataItemEwallet>
    private lateinit var walletName : ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.wallet_add_account_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvAccountDialogTitle.text = "Edit Account"
        btnWalletAddRecipient.setText("Done")
        view.setBackgroundResource(R.drawable.all_resource_white_rectangle_radius)

        //viewmodel
        walletListViewModel = ViewModelProviders.of(this).get(WalletListViewModel::class.java)
        walletEditContactViewModel = ViewModelProviders.of(this).get(WalletEditContactViewModel::class.java)

        walletName = arrayListOf()

        //get session data
        session = SessionManager(requireContext())

        //wallet data
        walletList()

        wallet_edt_name_contact.setText("${arrayContactData.name}")
        etWalletAccountNumberContact.setText("${arrayContactData.accountAccNumber}")
        var walletNameSelected : String? = null
        var walletIdSelected : String? = null
        val selection = findIndex(walletName, arrayContactData.accountName.toString())
        Log.d("WalletSelection", selection.toString())
        wallet_edt_select_wallet_contact.setSelection(selection)
        wallet_edt_select_wallet_contact.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                walletNameSelected = walletData[selection].ewalletName
                walletIdSelected = walletData[selection].ewalletId
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                walletNameSelected = walletData[p2].ewalletName
                walletIdSelected = walletData[p2].ewalletId
            }

        }

        btnWalletAddRecipient.setOnClickListener {
            editContact(arrayContactData.id!!, wallet_edt_name_contact.text.toString(),
                walletIdSelected!!, etWalletAccountNumberContact.text.toString())
        }
    }

    private fun walletList() {
        session.login?.let { walletListViewModel.getWalletData(header = it) }
        walletListViewModel.responseData.observe(viewLifecycleOwner, Observer {
            walletData = it.data as ArrayList<DataItemEwallet>
            for (i in walletData.indices){
                walletName.add(walletData.get(i).ewalletName.toString())
            }
            wallet_edt_select_wallet_contact.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, walletName)
        })

        walletListViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlWalletAddBottomContent),  "Wallet data is null", Snackbar.LENGTH_LONG).show()
        })

        walletListViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            btnWalletAddRecipient.isEnabled = it != true
        })
    }

    private fun editContact(
        id: String,
        nameContact: String,
        walletIdSelected: String,
        accountNumberWallet: String
    ) {
        session.login?.let { walletEditContactViewModel.editContact(
            header = it,
            name = nameContact,
            eWalletId = walletIdSelected,
            accountNumberWallet = accountNumberWallet,
            id = id
        ) }

        walletEditContactViewModel.responseData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlWalletMainPageContent),  nameContact.toString() + " Success updated", Snackbar.LENGTH_LONG).show()
            dismiss()
            parentMenuFragment.dismiss()
            callbackEdit()
        })
        walletEditContactViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), nameContact.toString() + " Update Error! ", Toast.LENGTH_SHORT).show()
        })

        walletEditContactViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) skAddEditLoadingWallet.visibility = View.VISIBLE
            else skAddEditLoadingWallet.visibility = View.GONE
        })
    }

    fun findIndex(arr: ArrayList<String>, item: String) : Int {
        for (i in arr.indices)
        {
            if (arr[i] == item) {
                return i
            }
        }
        return -1
    }

    override fun dismiss() {
        super.dismiss()
    }
}