package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.DataItemInternetData
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter.PaymentMobileMainInternetDataListAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.ViewModel.PaymentMobileViewModel
import kotlinx.android.synthetic.main.payment_mobile_internet_data_fragment.*

class PaymentMobileMainInternetDataFragment(var phoneNumber : String, var callback : (id : String?) -> Unit) : Fragment() {
    private lateinit var dataInternetData : ArrayList<DataItemInternetData>

    private lateinit var paymentMobileViewModel: PaymentMobileViewModel
    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.payment_mobile_internet_data_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        paymentMobileViewModel = ViewModelProviders.of(this).get(PaymentMobileViewModel::class.java)
        rvBind()
    }

    private fun rvBind() {
        dataInternetData = arrayListOf()

        sessionManager.login?.let {
            paymentMobileViewModel.getPaymentInternetData(
                header = it,
                phoneNumber = phoneNumber
            )
        }

        paymentMobileViewModel.responseInternetData.observe(viewLifecycleOwner, Observer {
            dataInternetData = it.data as ArrayList<DataItemInternetData>
            val adapterInternetData = PaymentMobileMainInternetDataListAdapter { id ->
                callback(id)
            }
            adapterInternetData.setData(dataInternet = it.data)

            rvPaymentMobileMainInternetData.apply {
                adapter = adapterInternetData
                setHasFixedSize(true)
            }
        })

        paymentMobileViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Fail to get Internet Data", Toast.LENGTH_SHORT).show()
        })

    }
}