package com.aTeam.beasy.View.Gamification.DetailMissionGamification

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Gamification.DataGamificationDetailPlanet
import com.aTeam.beasy.Data.Model.Gamification.MissionItem
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.Adapter.GamificationDetailMissionItemAdapter
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.ViewModel.GamificationDetailMissionViewModel
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.GamificationDetailRewardAndMissionActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gamification_detail_mission_activity.*

class GamificationDetailMissionActivity : AppCompatActivity() {
    companion object{
        const val IDPLANET = "planet_id"
        const val STATUSDELAY = "false"
    }

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var gamificationViewModel : GamificationDetailMissionViewModel

    private lateinit var rewardId : String

    private var missionSuccessCount : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gamification_detail_mission_activity)

        sessionManager = SessionManager(this)
        gamificationViewModel = ViewModelProviders.of(this).get(GamificationDetailMissionViewModel::class.java)

        val idPlanet = intent.getStringExtra(IDPLANET)
        val statusDelay = intent.getStringExtra(STATUSDELAY)
        Log.d("StatusDelay", statusDelay.toString())
        if (statusDelay == "true"){
            tvGamificationDetailMessage.visibility = View.VISIBLE
        }

        gamificationVM(idPlanet)
        ivGamificationDetailMissionBack.setOnClickListener {
            onBackPressed()
        }

        btnGamificationDetailReward.setOnClickListener {
            var isClamiable = "no"
            if (statusDelay == "true") isClamiable = "yes"
            val intent =  Intent(this, GamificationDetailMissionRewardActivity::class.java)
            intent.putExtra(GamificationDetailMissionRewardActivity.IDREWARD, rewardId)
            intent.putExtra(GamificationDetailMissionRewardActivity.ISCLAIMABLE, isClamiable)
            startActivity(intent)
        }
        ivGamificationDetailReward.setOnClickListener {
            startActivity(Intent(this, GamificationDetailRewardAndMissionActivity::class.java))
        }
    }

    private fun gamificationVM(idPlanet: String?) {
        sessionManager.login?.let {
            gamificationViewModel.getGamificationDetailPlanet(
                header = it,
                idPlanet = idPlanet!!
            )
        }

        responseGamificationDetailPlanet()
    }

    private fun responseGamificationDetailPlanet() {
        gamificationViewModel.responseGamificationDetailPlanet.observe(this, Observer {
            bindView(it.data)
            rewardId = it.data?.rewardId.toString()
        })

        gamificationViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to get Planet Information", Toast.LENGTH_SHORT).show()
            gamificationDetailRewardErrorFound.visibility = View.VISIBLE
        })

        gamificationViewModel.isLoading.observe(this, Observer {
            if (it == true){
                gamificationDetailRewardLoading.visibility = View.VISIBLE
            }else {
                gamificationDetailRewardLoading.visibility = View.GONE
            }
        })
    }

    private fun bindView(data: DataGamificationDetailPlanet?) {
        Glide.with(this).load(data?.image).into(ivGamificationDetailPlanet)
        tvGamificationDetailPlanetTitle.text = "Planet " + data?.name
        tvGamificationDetailMissionDescription.text = data?.storytelling
        if (data?.status == "CURRENT" || data?.status == "DONE"){
            btnGamificationDetailReward.visibility = View.VISIBLE
        }
        missionBind(data?.mission)
    }

    private fun missionBind(mission: List<MissionItem?>?) {
        val dataMission : ArrayList<MissionItem> = mission as ArrayList<MissionItem>
        val adapterMission = GamificationDetailMissionItemAdapter()
        adapterMission.setData(dataMission)

        rvGamificationDetailMissionListMission.apply {
            adapter = adapterMission
            setHasFixedSize(true)
        }

        for (i in dataMission.indices){
            if (dataMission.get(i).passed == true){
                missionSuccessCount += 1
            }
        }

        if (missionSuccessCount >= 0){
            btnGamificationDetailReward.isEnabled = true
            btnGamificationDetailReward.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
            btnGamificationDetailReward.setTextColor(Color.parseColor("#ffffff"))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}