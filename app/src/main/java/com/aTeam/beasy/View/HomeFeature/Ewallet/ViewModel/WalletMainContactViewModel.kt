package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletContact
import com.aTeam.beasy.Data.Repository.Ewallet.WalletContactRepository

class WalletMainContactViewModel : ViewModel() {
    val walletContactRepository = WalletContactRepository()

    val responseData = MutableLiveData<ResponseEwalletContact>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    val responseDataRecent = MutableLiveData<ResponseEwalletContact>()
    val errorDataRecent = MutableLiveData<Throwable>()
    val isLoadingRecent = MutableLiveData<Boolean>()

    fun getContactData(header:String){
        isLoading.value = true
        walletContactRepository.getAccount(header,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }

    fun getContactRecent(header: String){
        isLoadingRecent.value = true
        walletContactRepository.getAccountRecent(header,
            {
                responseDataRecent.value = it
                Log.d("dataRecent", it.toString())
                isLoadingRecent.value = false
            },
            {
                errorDataRecent.value = it
                isLoadingRecent.value = false
            }
        )
    }
}