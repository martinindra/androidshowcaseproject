package com.aTeam.beasy.View.HomeFeature.Payment.CreditCard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.CreditCard.DataCreditCardBill
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Payment.CreditCard.ViewModel.PaymentCreditCardViewModel
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.PaymentSuccessTransactionActivity
import com.aTeam.beasy.View.HomeFeature.Payment.PaymentSuccessTransfer
import com.aTeam.beasy.View.HomeFeature.Transfer.TransferConfirmationPageActivity
import kotlinx.android.synthetic.main.payment_credit_card_pay_confirmation_activity.*
import kotlinx.android.synthetic.main.payment_mobile_main_activity.*
import kotlinx.android.synthetic.main.transfer_confirmation_otp_dialog.view.*

class PaymentCreditCardPayConfirmationActivity : AppCompatActivity() {
    companion object{
        const val EXTRA_DATA = "extra_data"
        const val PAYMENT_AMOUNT = "payment_amount"
    }

    private var functionResource = FunctionResource()
    private lateinit var paymentCreditCardViewModel : PaymentCreditCardViewModel
    private lateinit var session : SessionManager
    private lateinit var dataPaymentCreditCard : ArrayList<DataCreditCardBill>
    private var paymentAmount : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_credit_card_pay_confirmation_activity)

        paymentCreditCardViewModel = ViewModelProviders.of(this).get(PaymentCreditCardViewModel::class.java)
        session = SessionManager(this)

        dataPaymentCreditCard = arrayListOf()

        dataPaymentCreditCard = intent.getParcelableArrayListExtra<DataCreditCardBill>(
            EXTRA_DATA
        ) as ArrayList<DataCreditCardBill>
        paymentAmount = intent.getStringExtra(PAYMENT_AMOUNT)

        tvPaymentCreditCardConifrmationAccountNameData.text = dataPaymentCreditCard.get(0).name
        tvPaymentCreditCardConifrmationCardTypeData.text = dataPaymentCreditCard.get(0).bank
        tvPaymentCreditCardConifrmationCardNumberData.text = dataPaymentCreditCard.get(0).creditCardNumber
        tvPaymentCreditCardConifrmationPaymentAmountData.text = functionResource.rupiah(paymentAmount?.toInt()?.toDouble()!!).toString()

        backPaymentCreditCardConfirmation.setOnClickListener {
            onBackPressed()
        }

        btnPaymentCreditCardConifrmationNext.setOnClickListener {
            showPinDialog()
        }
    }

    private fun showPinDialog() {

        val context = this
        val builder = AlertDialog.Builder(context)

        val view = layoutInflater.inflate(R.layout.transfer_confirmation_otp_dialog, null)
        var pinError = false

        var dialog = builder.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferConfirmationDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnTransferConfirmationDialogSubmit.setOnClickListener {

            val pin = view.pvTransferConfirmationOtp.text.toString()
            if (pin.length != 6){
                pinError = true
                Toast.makeText(this, "your pin is less than 6", Toast.LENGTH_SHORT).show()
            }else if (pin.isEmpty()){
                pinError = true
                Toast.makeText(this, "your pin is empty", Toast.LENGTH_SHORT).show()
            }else{
                session.login?.let { it1 ->
                    paymentCreditCardViewModel.payPaymentCreditCardBill(
                        header = it1,
                        ccNumber = dataPaymentCreditCard.get(0).creditCardNumber!!,
                        amount = paymentAmount?.toInt()!!,
                        pin = pin.toInt()
                    )
                }

                payProcess()
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun payProcess() {
        paymentCreditCardViewModel.responsePayData.observe(this, Observer {
            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, PaymentSuccessTransfer::class.java))
            finish()
        })

        paymentCreditCardViewModel.responseError.observe(this, Observer {
            Toast.makeText(this, "Error Pay bill", Toast.LENGTH_SHORT).show()
        })

        paymentCreditCardViewModel.isLoading.observe(this, Observer {
            if (it == true){
                loadingWait.visibility = View.VISIBLE
            }else{
                loadingWait.visibility = View.GONE
                showPinDialog()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}