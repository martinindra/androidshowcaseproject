package com.aTeam.beasy.View.HomeFeature.Payment.Merchants

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Payment.Merchant.DataItemMerchantList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.Adapter.PaymentMerchantMainHorizontalListAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.BottomSheet.PaymentMerchantPayBottomSheetDialogFragment
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.ViewModel.PaymentMerchantViewModel
import kotlinx.android.synthetic.main.payment_merchant_main_activity.*

class PaymentMerchantMainActivity : AppCompatActivity() {

    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var paymentMerchantViewModel: PaymentMerchantViewModel

    private lateinit var dataMerchantList : ArrayList<DataItemMerchantList>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_merchant_main_activity)

        sessionManager = SessionManager(this)
        paymentMerchantViewModel = ViewModelProviders.of(this).get(PaymentMerchantViewModel::class.java)

        rvBind()
        btnPaymentMerchantsScanQR.setOnClickListener {
            startActivity(Intent(this, PaymentMerchantMainQRCodeActivity::class.java))
        }

        btnPaymentMerchantsInputID.setOnClickListener {
            val paymentMerchantPayBottomSheetDialogFragment = PaymentMerchantPayBottomSheetDialogFragment("")
            paymentMerchantPayBottomSheetDialogFragment.show(supportFragmentManager, "BottomSheet")
        }

        tvPaymentMerchantMainSeeMore.setOnClickListener {
            startActivity(Intent(this, PaymentMerchantSeeAllActivity::class.java))
        }

        ivPaymentMerchantsBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun rvBind() {
        dataMerchantList = arrayListOf()

        sessionManager.login?.let {
            paymentMerchantViewModel.getMerchantListData(
                header = it
            )
        }

        paymentMerchantViewModel.responseMerchantList.observe(this, Observer {
            dataMerchantList = it.data as ArrayList<DataItemMerchantList>
            val adapterMerchant = PaymentMerchantMainHorizontalListAdapter(this)
            adapterMerchant.setData(
                dataMerchant = dataMerchantList
            )

            rvPaymentMerchantsMainIcon.apply {
                adapter = adapterMerchant
                setHasFixedSize(true)
            }
        })

        paymentMerchantViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Cannot Get Merchant Data", Toast.LENGTH_SHORT).show()
        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}