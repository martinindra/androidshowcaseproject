package com.aTeam.beasy.View.HomeFeature.Profile.Adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.aTeam.beasy.R

class ProfileFaqExpandableListAdapter internal constructor(
    private val context: Context,
    private var titleList : List<String>,
    private var dataList : HashMap<String, List<String>>
) : BaseExpandableListAdapter()
{
    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return this.dataList[this.titleList[groupPosition]]!![childPosition]
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertViews = convertView
        val expandableListText = getChild(groupPosition, childPosition) as String
        if (convertViews == null){
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViews = layoutInflater.inflate(R.layout.profile_faq_list_item, parent, false)
        }
        val expandedListTextView = convertViews!!.findViewById<TextView>(R.id.tvProfileFaqList)
        expandedListTextView.text = expandableListText
        return convertViews
    }
    override fun getGroup(groupPosition: Int): Any {
        return this.titleList[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.titleList.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.dataList[this.titleList[groupPosition]]!!.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertViews = convertView
        val listTitle = getGroup(groupPosition) as String
        if (convertViews == null){
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViews = layoutInflater.inflate(R.layout.profile_faq_list_item, parent, false)
        }
        val listTitleTextView = convertViews!!.findViewById<TextView>(R.id.tvProfileFaqList)
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = listTitle
        return convertViews
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}