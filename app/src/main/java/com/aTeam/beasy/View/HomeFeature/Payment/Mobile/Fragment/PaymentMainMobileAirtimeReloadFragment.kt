package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.DataItemAirtimeReload
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter.PaymentMobileMainAirtimeReloadListAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.ViewModel.PaymentMobileViewModel
import kotlinx.android.synthetic.main.payment_main_mobile_airtime_reload_fragment.*

class PaymentMainMobileAirtimeReloadFragment(var phoneNumber: String, var callback: (id : String?) -> Unit, ) : Fragment() {
    private lateinit var dataAirtimeReload : ArrayList<DataItemAirtimeReload>

    private lateinit var paymentMobileViewModel: PaymentMobileViewModel
    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.payment_main_mobile_airtime_reload_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        paymentMobileViewModel = ViewModelProviders.of(this).get(PaymentMobileViewModel::class.java)
        rvBind()
    }

    private fun rvBind() {
        dataAirtimeReload = arrayListOf()
        sessionManager.login?.let {
            paymentMobileViewModel.getPaymentAirtimeReloadData(
                header = it,
                phoneNumber = phoneNumber
            )
        }

        paymentMobileViewModel.responseAirtimeReloadData.observe(viewLifecycleOwner, Observer {
            dataAirtimeReload = it.data as ArrayList<DataItemAirtimeReload>
            val adapterAirtime = PaymentMobileMainAirtimeReloadListAdapter {id ->
                callback(id)
            }
            adapterAirtime.setData(dataAirtime = it.data)

            rvPaymentMobileMainAirtimeReload.apply {
                adapter = adapterAirtime
                layoutManager = GridLayoutManager(context, 2)
                setHasFixedSize(true)
            }
        })

        paymentMobileViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Fail to get Mobile Data", Toast.LENGTH_SHORT).show()
            Log.d("phoneNumberError", it.toString())
        })

    }
}