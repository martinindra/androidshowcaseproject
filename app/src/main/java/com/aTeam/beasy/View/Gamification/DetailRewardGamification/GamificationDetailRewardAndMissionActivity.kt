package com.aTeam.beasy.View.Gamification.DetailRewardGamification

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter.GamificationDetailRewardMissionPagerAdapter
import kotlinx.android.synthetic.main.base_main_activity.*
import kotlinx.android.synthetic.main.gamification_detail_reward_and_mission_activity.*

class GamificationDetailRewardAndMissionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gamification_detail_reward_and_mission_activity)

        val adapter = GamificationDetailRewardMissionPagerAdapter(supportFragmentManager)
        vpGamificationDetailMissionReward.adapter = adapter
        vpGamificationDetailMissionReward.currentItem = 0
        vpGamificationDetailMissionReward.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                changingMissionRewards(position)
            }

            override fun onPageSelected(position: Int) {
                changingMissionRewards(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
        buttonOnClick()

        ivGamificationDetailRewardMissionBack.setOnClickListener {
            onBackPressed()
        }
    }
    private fun buttonOnClick() {
        btnGamificationDetailMissionRewardMissionComplete.setOnClickListener {
            vpGamificationDetailMissionReward.currentItem = 0
        }
        btnGamificationDetailMissionRewardNextMission.setOnClickListener {
            vpGamificationDetailMissionReward.currentItem = 1
        }
        btnGamificationDetailMissionRewardMyRewards.setOnClickListener {
            vpGamificationDetailMissionReward.currentItem = 2
        }
    }
    private fun changingMissionRewards(position: Int) {
        when(position){
            0 -> {
                btnGamificationDetailMissionRewardMissionComplete.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                btnGamificationDetailMissionRewardMissionComplete.setTextColor(Color.parseColor("#ffffff"))
                btnGamificationDetailMissionRewardNextMission.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardNextMission.setTextColor(Color.parseColor("#000000"))
                btnGamificationDetailMissionRewardMyRewards.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardMyRewards.setTextColor(Color.parseColor("#000000"))
            }
            1 -> {
                btnGamificationDetailMissionRewardMissionComplete.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardMissionComplete.setTextColor(Color.parseColor("#000000"))
                btnGamificationDetailMissionRewardNextMission.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                btnGamificationDetailMissionRewardNextMission.setTextColor(Color.parseColor("#ffffff"))
                btnGamificationDetailMissionRewardMyRewards.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardMyRewards.setTextColor(Color.parseColor("#000000"))
            }
            2 -> {
                btnGamificationDetailMissionRewardMissionComplete.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardMissionComplete.setTextColor(Color.parseColor("#000000"))
                btnGamificationDetailMissionRewardNextMission.setBackgroundResource(R.drawable.all_resource_bg_white_fillform)
                btnGamificationDetailMissionRewardNextMission.setTextColor(Color.parseColor("#000000"))
                btnGamificationDetailMissionRewardMyRewards.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                btnGamificationDetailMissionRewardMyRewards.setTextColor(Color.parseColor("#ffffff"))
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}