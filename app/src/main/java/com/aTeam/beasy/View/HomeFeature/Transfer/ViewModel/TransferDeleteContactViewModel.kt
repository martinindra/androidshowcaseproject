package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseDeleteContact
import com.aTeam.beasy.Data.Repository.Transfer.TransferContactRepository

class TransferDeleteContactViewModel : ViewModel() {
    val transferContactRepository = TransferContactRepository()

    val responseData = MutableLiveData<ResponseDeleteContact>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun deleteData(header:String, idUser : String){
        isLoading.value = true
        transferContactRepository.deleteContactById(tokenUser = header, idUser = idUser,
            {
                isLoading.value = false
                responseData.value = it
            },
            {
                isLoading.value = false
                errorData.value = it
            }
        )
    }
}