package com.aTeam.beasy.View.HomeFeature.Payment.Mobile

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.ContactsContract.Contacts
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.Adapter.PaymentMobileMainPagerAdapter
import com.aTeam.beasy.View.HomeFeature.Payment.Mobile.ViewModel.PaymentMobileViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.base_main_activity.*
import kotlinx.android.synthetic.main.login_main_activity.*
import kotlinx.android.synthetic.main.payment_mobile_main_activity.*
import kotlinx.android.synthetic.main.transfer_confirmation_otp_dialog.view.*
import java.security.Permission
import java.util.jar.Manifest


class PaymentMobileMainActivity : AppCompatActivity() {

    val CONTACT_PICKER_RESULT : Int = 1001
    private var idMobilePicker : String? = null
    private var typeMobilePicker : Int = 0

    //token
    private lateinit var session: SessionManager

    //viewModel
    private lateinit var paymentMobileViewModel: PaymentMobileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_mobile_main_activity)

        session = SessionManager(this)
        paymentMobileViewModel = ViewModelProviders.of(this).get(PaymentMobileViewModel::class.java)
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_CONTACTS) !== PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 1)
            } else {
                ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 1)
            }
        }else{
            editTextPaymentMobile()
        }
        ivPaymentMobileBack.setOnClickListener {
            onBackPressed()
        }

        btnPaymentMobileMainBuy.setOnClickListener {
            var tipe : String? = ""
            if (typeMobilePicker == 1) tipe = "Airtime Reload"
            if (typeMobilePicker == 2) tipe = "Internet Data"
            Snackbar.make(paymentMobileMain, "You buy $tipe with id $idMobilePicker", Snackbar.LENGTH_SHORT).show()
            showPinDialog(edtPaymentMobileMainPhoneNumber.text.toString())
        }

        btnPaymentMobileMainCheck.setOnClickListener {
            viewPagerPaymentMobile()
            btnPaymentMobileMainCheck.visibility = View.GONE
            btnPaymentMobileMainBuy.visibility = View.VISIBLE
            edtPaymentMobileMainPhoneNumber.isEnabled = false
        }

    }

    private fun viewPagerPaymentMobile() {
        val paymentMobileAdapterPager : PaymentMobileMainPagerAdapter = PaymentMobileMainPagerAdapter(supportFragmentManager,
            phoneNumber = edtPaymentMobileMainPhoneNumber.text.toString(),
         {
             idMobilePicker = it
             btnPaymentMobileMainBuy.isEnabled = true
             btnPaymentMobileMainBuy.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
             btnPaymentMobileMainBuy.setTextColor(Color.parseColor("#ffffff"))

         },{
             typeMobilePicker = it
         }
        )
        vpPaymentMobileMain.adapter = paymentMobileAdapterPager
        changingTabs(0)

        vpPaymentMobileMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                changingTabs(position)
            }

            override fun onPageSelected(position: Int) {
                changingTabs(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
        vpPaymentMobileMain.visibility = View.VISIBLE
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun editTextPaymentMobile() {
        edtPaymentMobileMainPhoneNumber.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP){
                if(event.getRawX() >= (edtPaymentMobileMainPhoneNumber.getRight() - edtPaymentMobileMainPhoneNumber.getCompoundDrawables()[2].getBounds().width())) {
                    val contactPickerIntent = Intent(
                        Intent.ACTION_PICK,
                        Contacts.CONTENT_URI
                    )
                    startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT)
                    return@setOnTouchListener true
                }
            }

            return@setOnTouchListener false
        }

        edtPaymentMobileMainPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                tvPaymentMobileMainAirtimeReload.visibility = View.VISIBLE
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edtPaymentMobileMainPhoneNumber.text.toString().length > 10) {
                    tvPaymentMobileMainEnterPhone.visibility = View.GONE
                    btnPaymentMobileMainCheck.isEnabled = true
                }else{
                    tvPaymentMobileMainAirtimeReload.visibility = View.VISIBLE
                    btnPaymentMobileMainCheck.isEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (edtPaymentMobileMainPhoneNumber.text.toString().length > 10) {
                    tvPaymentMobileMainEnterPhone.visibility = View.GONE
                    btnPaymentMobileMainCheck.isEnabled = true
                    btnPaymentMobileMainCheck.setTextColor(Color.parseColor("#ffffff"))
                    btnPaymentMobileMainCheck.setBackgroundResource(R.drawable.all_resource_blue_primary_button_rounded)
                }else{
                    tvPaymentMobileMainAirtimeReload.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun changingTabs(position: Int) {
        if (position == 0){
            tvPaymentMobileMainAirtimeReload.setTextColor(Color.parseColor("#0D3BA7"))
            vPaymentMobileMainAirtimeReload.setBackgroundResource(R.color.blue_primary)
            tvPaymentMobileMainInternetData.setTextColor(Color.parseColor("#000000"))
            vPaymentMobileMainInternetData.setBackgroundResource(R.color.black)
        } else if (position == 1){
            tvPaymentMobileMainAirtimeReload.setTextColor(Color.parseColor("#000000"))
            vPaymentMobileMainAirtimeReload.setBackgroundResource(R.color.black)
            tvPaymentMobileMainInternetData.setTextColor(Color.parseColor("#0D3BA7"))
            vPaymentMobileMainInternetData.setBackgroundResource(R.color.blue_primary)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val cr: ContentResolver = getContentResolver()
        val cur: Cursor? = cr.query(data?.getData()!!, null, null, null, null)
        var phone = ""
        if (cur?.getCount()!! > 0) {
            while (cur.moveToNext()) {
                val id: String = cur.getString(
                    cur.getColumnIndex(Contacts._ID)
                )
                val name: String = cur.getString(
                    cur.getColumnIndex(Contacts.DISPLAY_NAME)
                )

                if (cur.getString(cur.getColumnIndex(Contacts.HAS_PHONE_NUMBER)).toInt() > 0) {
                    //Query phone here.  Covered next
                    val pCur: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    while (pCur?.moveToNext()!!) {
                        // Do something with phones
                        phone = pCur.getString(
                            pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        ) ?: ""
                        Log.e("ContactPickerFragment", "onActivityResult phone $phone" )
                    }
                    pCur.close()
                }
            }
        }
        phone = phone.trim { it <= ' ' } ?: ""
        edtPaymentMobileMainPhoneNumber.setText(phone)
    }

    fun showPinDialog(phoneNumber: String) {
        val context = this
        val builder = AlertDialog.Builder(context)

        val view = layoutInflater.inflate(R.layout.transfer_confirmation_otp_dialog, null)
        var pinError = false

        var dialog = builder.setView(view).create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferConfirmationDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        view.btnTransferConfirmationDialogSubmit.setOnClickListener {

            val pin = view.pvTransferConfirmationOtp.text.toString()
            if (pin.length != 6){
                pinError = true
                Toast.makeText(this, "your pin is less than 6", Toast.LENGTH_SHORT).show()
            }else if (pin.isEmpty()){
                pinError = true
                Toast.makeText(this, "your pin is empty", Toast.LENGTH_SHORT).show()
            }else{
                session.login?.let { it1 ->
                    paymentMobileViewModel.postMobilePay(
                        header = it1,
                        id = idMobilePicker.toString(),
                        pin = pin.toInt(),
                        phoneNumber = phoneNumber
                    )
                }

                paymentMobileViewModel.responseMobilePay.observe(this, Observer {
                    val intent = Intent(this, PaymentSuccessTransactionActivity::class.java)
                    intent.putExtra(PaymentSuccessTransactionActivity.ID, idMobilePicker.toString())
                    intent.putExtra(PaymentSuccessTransactionActivity.TYPE, typeMobilePicker.toString())
                    intent.putExtra(PaymentSuccessTransactionActivity.NUMBERPHONE, edtPaymentMobileMainPhoneNumber.text.toString())
                    startActivity(intent)
                    finish()
                })

                paymentMobileViewModel.responseErrorData.observe(this, Observer {
                    Toast.makeText(this, "Fail to Pay", Toast.LENGTH_SHORT).show()
                })

            }
        }

        dialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}