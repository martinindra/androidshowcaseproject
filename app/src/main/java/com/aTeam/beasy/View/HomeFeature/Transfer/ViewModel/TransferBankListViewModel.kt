package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Bank.ResponseBankList
import com.aTeam.beasy.Data.Model.Transfer.ResponseTransferContact
import com.aTeam.beasy.Data.Repository.Bank.BankListRepository

class TransferBankListViewModel : ViewModel() {
    val bankListRepository = BankListRepository()

    val responseData = MutableLiveData<ResponseBankList>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun getBankData(header:String){
        isLoading.value = true
        bankListRepository.getBankList(header,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}