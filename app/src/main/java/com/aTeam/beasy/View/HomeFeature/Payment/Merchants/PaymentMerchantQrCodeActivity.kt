package com.aTeam.beasy.View.HomeFeature.Payment.Merchants

import android.content.pm.PackageManager
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.BottomSheet.PaymentMerchantPayBottomSheetDialogFragment
import com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet.TransferMenuBottomSheetFragment
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import kotlinx.android.synthetic.main.payment_merchant_qr_code_activity.*

class PaymentMerchantQrCodeActivity : AppCompatActivity() {

    private lateinit var codeScanner: CodeScanner
    private lateinit var scanResult : String
    private var isScanSuccess : Boolean = false

    companion object{
        const val CAMERA_REQ = 101
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_merchant_qr_code_activity)

        setupPermission()
        codeScanners()

        btnPaymentMerchantsQRNext.setOnClickListener {
            val paymentMerchantPayBottomSheetDialogFragment = PaymentMerchantPayBottomSheetDialogFragment(scanResult)
            paymentMerchantPayBottomSheetDialogFragment.show(supportFragmentManager, "BottomSheet")
        }

        btnPaymentMerchantsMainQRResultRescan.setOnClickListener {
            scnPaymentMerchantQrScanner.isEnabled = false
            scnPaymentMerchantQrScanner.visibility = View.VISIBLE
            llPaymentMerchantQRResult.visibility = View.GONE
            tvPaymentMerchantQrResult.visibility = View.VISIBLE
            btnPaymentMerchantsQRNext.visibility = View.GONE
        }
    }

    private fun checkResult() {
        if (isScanSuccess == true){
            btnPaymentMerchantsQRNext.visibility = View.VISIBLE
            scnPaymentMerchantQrScanner.visibility = View.GONE
            llPaymentMerchantQRResult.visibility = View.VISIBLE
            tvPaymentMerchantQrResult.visibility = View.GONE
        }else{
            btnPaymentMerchantsQRNext.visibility = View.GONE
        }
    }


    private fun setupPermission() {
        val permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED){
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this, arrayOf(android.Manifest.permission.CAMERA),
            CAMERA_REQ
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            CAMERA_REQ -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "You need the camera permission to use this app", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun codeScanners() {
        isScanSuccess = false
        codeScanner = CodeScanner(this, scnPaymentMerchantQrScanner)

        codeScanner.apply {
            camera = CodeScanner.CAMERA_BACK
            formats = CodeScanner.ALL_FORMATS

            autoFocusMode = AutoFocusMode.SAFE
            scanMode = ScanMode.CONTINUOUS
            isAutoFocusEnabled = true
            isFlashEnabled = false

            decodeCallback = DecodeCallback {
                runOnUiThread{
                    scanResult = it.text
                    isScanSuccess = true
                    checkResult()
                }
            }

            errorCallback = ErrorCallback {
                runOnUiThread {
                    Log.e("ErrorQRScanner", "${it.message}")
                    isScanSuccess = false
                }
            }

            scnPaymentMerchantQrScanner.setOnClickListener {
                codeScanner.startPreview()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPanelClosed(featureId: Int, menu: Menu) {
        codeScanner.releaseResources()
        super.onPanelClosed(featureId, menu)
    }

}

