package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletConfirmation
import com.aTeam.beasy.Data.Repository.Ewallet.WalletRepository

class WalletConfirmationViewModel : ViewModel() {
    val walletRepository = WalletRepository()

    val responseData = MutableLiveData<ResponseEwalletConfirmation>()
    val errorData = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()

    fun addAccountViewModel(header:String, accountId : String, amount : Int, message : String, pin : Int){
        isLoading.value = true
        walletRepository.walletToAccount(
            tokenUser = header,
            accountId = accountId,
            amount = amount,
            message = message,
            pin = pin,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                Log.d("errorData", it)
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}