package com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Gamification.PlanetStatusDataItem
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.GamificationDetailMissionActivity
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.GamificationDetailMissionRewardActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gamification_detail_reward_mission_item_list.view.*

class GamificationDetailRewardMissionItemAdapter(var type : Int?) : RecyclerView.Adapter<GamificationDetailRewardMissionItemAdapter.ViewHolder>(){
    var dataGamification : ArrayList<PlanetStatusDataItem> = arrayListOf()

    fun setData (dataGamification : ArrayList<PlanetStatusDataItem>){
        this.dataGamification = dataGamification
        notifyDataSetChanged()
    }
    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var image = itemView.ivGamificationDetailMissionIcon
        var name = itemView.tvGamificationDetailMissionTitle
        var button = itemView.btnGamificationDetailMissionButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.gamification_detail_reward_mission_item_list, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataGamifikasi = this.dataGamification.get(position)

        Glide.with(holder.itemView.context).load(dataGamifikasi.image).into(holder.image)
        holder.name.text = "Planet " + dataGamifikasi.name

        when(type){
            1 -> {
                holder.button.text = "Mission Detail"
                holder.button.setOnClickListener {
                    val intent = Intent(it.context, GamificationDetailMissionActivity::class.java)
                    intent.putExtra(GamificationDetailMissionActivity.IDPLANET, dataGamifikasi.id)
                    intent.putExtra(GamificationDetailMissionActivity.STATUSDELAY, "false")
                    it.context.startActivities(arrayOf(intent))
                }
            }
            2 -> {
                holder.button.text = "Details"
                holder.button.setOnClickListener {
                    val intent = Intent(it.context, GamificationDetailMissionRewardActivity::class.java)
                    intent.putExtra(GamificationDetailMissionRewardActivity.IDREWARD, dataGamifikasi.rewardId)
                    it.context.startActivities(arrayOf(intent))
                }
            }
        }

    }

    override fun getItemCount(): Int = dataGamification.size

}