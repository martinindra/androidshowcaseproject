package com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.HomeFeature.Pocket.PocketMainFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.pocket_delete_edit_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.pocket_edit_pocket_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.transfer_delete_account_dialog.view.*

class PocketDeleteEditBottomSheetFragment (
    var idPocket : String,
    var imageProfile : String,
    var nameOfPocket : String,
    var dueDateDDMMYY : String,
    var target : Int,
    var onResult:() -> Unit
): BottomSheetDialogFragment() {

    private lateinit var sessionManager: SessionManager
    private lateinit var pocketMainViewModel: PocketMainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pocket_delete_edit_bottom_sheet_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sessionManager = SessionManager(requireContext())
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)
        view.hideKeyboard()
        tvPocketMenuEditPocket.setOnClickListener{

            val pocketEditBottomSheetFragment = PocketEditBottomSheetFragment(
                idPocket,
                imageProfile,
                nameOfPocket,
                dueDateDDMMYY,
                target,
            ){
                onResult()
            }
            pocketEditBottomSheetFragment.show(parentFragmentManager, "TAG")

            dismiss()
        }

        tvPocketMenuDeletePocket.setOnClickListener {
            showDeleteDialog()
        }

    }

    fun View.hideKeyboard() {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    fun showDeleteDialog() {
        val context = getContext()
        val builder = context?.let { AlertDialog.Builder(it) }

        val view = layoutInflater.inflate(R.layout.transfer_delete_account_dialog, null)

        val dialog = builder?.setView(view)?.create()
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.tvTitleDelete.text = "Delete Pocket?"
        view.tvWordingDelete.text = "Are you sure you want to delete this pocket?"

        view.btnTransferDeleteDialogCancel.setOnClickListener {
            dialog?.dismiss()
        }
        view.btnTransferDeleteDialogSubmit.setOnClickListener {
            sessionManager.login?.let { it1 -> pocketMainViewModel.pocketDelete(
                header = it1,
                pocketId = idPocket
            ) }
            deleteAction(view, dialog)
        }

        dialog?.show()
    }

    private fun deleteAction(view: View, dialog: AlertDialog?) {
        pocketMainViewModel.responsePocketDelete.observe(
            viewLifecycleOwner, Observer {
                if (true == it.success){
                    Toast.makeText(requireContext(), "Pocket Successfully Deleted", Toast.LENGTH_LONG).show()
                    dialog?.dismiss()
                    dismiss()
                    startActivity(Intent(requireContext(), MainBaseActivity::class.java))
                    requireActivity().finish()
                }else{
                    Toast.makeText(requireContext(), "Please Move Balance Before Delete Pocket", Toast.LENGTH_LONG).show()
                    dialog?.dismiss()
                    dismiss()
                }
            }
        )
        pocketMainViewModel.responseErrorData.observe(
            viewLifecycleOwner, Observer {
                Toast.makeText(context, "Cannot Delete Pocket", Toast.LENGTH_LONG).show()
            }
        )
        pocketMainViewModel.isLoading.observe(
            viewLifecycleOwner, Observer {
                if (it == true) view.skDeleteLoading.visibility = View.VISIBLE
                else view.skDeleteLoading.visibility = View.GONE
            }
        )
    }

}