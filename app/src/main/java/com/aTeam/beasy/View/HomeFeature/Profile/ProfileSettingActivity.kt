package com.aTeam.beasy.View.HomeFeature.Profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Profile.ViewModel.ProfileMainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.profile_setting_activity.*

class ProfileSettingActivity : AppCompatActivity() {

    //token
    private lateinit var sessionManager: SessionManager
    //viewModel
    private lateinit var profileMainViewModel: ProfileMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_setting_activity)

        //initiate
        sessionManager = SessionManager(this)
        profileMainViewModel = ViewModelProviders.of(this).get(ProfileMainViewModel::class.java)

        ivProfileSettingBack.setOnClickListener {
            onBackPressed()
        }

        processGetData()
        updateProfileSetting()
    }

    private fun updateProfileSetting() {
        btnProfileSettingSave.setOnClickListener {
            var errorUpdate = false
            val pinInput = edtProfileSettingPin.text
            val emailInput = edtProfileSettingEmail.text
            if (pinInput?.isEmpty() == true){
                errorUpdate = true
                edtProfileSettingPin.error = "Please fill pin field"
            } else if (pinInput?.length!! < 6){
                errorUpdate = true
                edtProfileSettingPin.error = "Pin length must be 6"
            }

            if (emailInput.isEmpty()){
                errorUpdate = true
                edtProfileSettingEmail.error = "Please fill email field"
            }

            if (!errorUpdate){
                sessionManager.login?.let { token ->
                    profileMainViewModel.putUserProfileSetting(
                        header = token,
                        email = emailInput.toString(),
                        pin = pinInput.toString().toInt()
                    )
                }
                responseSettingEdit()
            }
        }
    }

    private fun responseSettingEdit() {
        profileMainViewModel.responseProfileSettingEdit.observe(this, Observer {
            Snackbar.make(profileSetting, "Success Update Profile Setting", Snackbar.LENGTH_SHORT).show()
            processGetData()
        })
        profileMainViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Cannot Update Setting", Toast.LENGTH_SHORT).show()
        })
        profileMainViewModel.isLoading.observe(this, Observer {
            if (it == true) profileSettingLoading.visibility = View.VISIBLE
            else profileSettingLoading.visibility = View.GONE
        })
    }

    private fun processGetData() {
        sessionManager.login?.let { profileMainViewModel.getUserProfileSetting(it) }
        responseProcess()
    }

    private fun responseProcess() {
        profileMainViewModel.responseProfileSetting.observe(this, Observer {
            edtProfileSettingPin.setText(it.data?.pin.toString())
            edtProfileSettingEmail.setText(it.data?.email)
        })

        profileMainViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to get user setting data", Toast.LENGTH_SHORT).show()
            profileSettingErrorFound.visibility = View.VISIBLE
        })

        profileMainViewModel.isLoading.observe(this, Observer {
            if (it == true) profileSettingLoading.visibility = View.VISIBLE
            else profileSettingLoading.visibility = View.GONE
        })
    }
}