package com.aTeam.beasy.View.HomeFeature.Base.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aTeam.beasy.View.HomeFeature.Home.HomeMainFragment
import com.aTeam.beasy.View.HomeFeature.Payment.PaymentMainFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.PocketMainFragment
import com.aTeam.beasy.View.HomeFeature.Profile.ProfileMainFragment
import com.aTeam.beasy.View.HomeFeature.Transfer.TransferMainFragment

class MainBasePagerAdapter(fm: FragmentManager?) :
    FragmentPagerAdapter(fm!!)
{
    override fun getCount(): Int {
        return 5
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                HomeMainFragment()
            }
            1 -> PocketMainFragment()
            2 -> PaymentMainFragment()
            3 -> TransferMainFragment()
            4 -> ProfileMainFragment()
            else -> HomeMainFragment()
        }
    }
}