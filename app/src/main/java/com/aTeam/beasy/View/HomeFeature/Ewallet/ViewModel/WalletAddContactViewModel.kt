package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseAddAccount
import com.aTeam.beasy.Data.Repository.Ewallet.WalletContactRepository

class WalletAddContactViewModel : ViewModel(){

    val addContactRepositoryWallet = WalletContactRepository()

    val responseData = MutableLiveData<ResponseAddAccount>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun addWalletViewModel(header:String, name:String, eWalletId:String, accountNumberWallet:String){
        isLoading.value = true
        addContactRepositoryWallet.addAccount(tokenUser = header, name = name, eWalletId = eWalletId, accountNumberWallet = accountNumberWallet ,
            {
                Log.d("responseAdd", "disini")
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}