package com.aTeam.beasy.View.HomeFeature.Ewallet.BottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel.WalletDeleteAccountViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.wallet_delete_account_dialog.view.*
import kotlinx.android.synthetic.main.wallet_menu_bottom_sheet_fragment.*

class WalletMenuBottomSheetFragment(
    var arrayContactDataWallet: DataItemAccount,
    var callbackMenuWallet: () -> Unit
) : BottomSheetDialogFragment() {

    //session
    private lateinit var sessionManagerWallet: SessionManager

    //viewmodeldelete
    private lateinit var walletDeleteAccountViewModel: WalletDeleteAccountViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.wallet_menu_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sessionManagerWallet = SessionManager(requireContext())
        walletDeleteAccountViewModel = ViewModelProviders.of(this).get(
            WalletDeleteAccountViewModel::class.java
        )

        tvWalletMenuDeleteAccount.setOnClickListener {
            showDeleteDialogWallet()
        }

        tvWalletMenuEditAccount.setOnClickListener {
            val walletEditContactBottomSheetFragment =
                WalletEditContactBottomSheetFragment(
                    arrayContactDataWallet,
                    this@WalletMenuBottomSheetFragment
                ) {
                    callbackMenuWallet()
                }
            walletEditContactBottomSheetFragment.show(
                parentFragmentManager,
                "EditBottomSheetDialog"
            )
        }
    }

    fun showDeleteDialogWallet() {
        val contextWallet = context
        val builderWallet = context?.let { AlertDialog.Builder(it) }

        val view = layoutInflater.inflate(R.layout.wallet_delete_account_dialog, null)

        val dialog = builderWallet?.setView(view)?.create()

        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnWalletDeleteDialogCancel.setOnClickListener {
            dialog?.dismiss()
        }
        view.btnWalletDeleteDialogSubmit.setOnClickListener {
            sessionManagerWallet.login?.let { it1 ->
                walletDeleteAccountViewModel.deleteDataWallet(
                    header = it1,
                    idUser = arrayContactDataWallet.id.toString()
                )
            }
            deleteActionWallet(view, dialog)
        }

        dialog?.show()
    }

    private fun deleteActionWallet(view: View, dialog: AlertDialog?) {
        walletDeleteAccountViewModel.responseDataWallet.observe(
            viewLifecycleOwner, Observer {
                Toast.makeText(
                    context,
                    "${arrayContactDataWallet.name} Already Deleted!",
                    Toast.LENGTH_SHORT
                ).show()
                dialog?.dismiss()
                dismiss()
                callbackMenuWallet()
            }
        )
        walletDeleteAccountViewModel.errorDataWallet.observe(
            viewLifecycleOwner, Observer {
                Toast.makeText(context, "${it.localizedMessage}", Toast.LENGTH_LONG).show()
            }
        )
//        walletDeleteAccountViewModel.isLoadingWallet.observe(
//            viewLifecycleOwner, Observer {
//                if (it == true) view.skDeleteLoadingWallet.visibility = View.VISIBLE
//                else view.skDeleteLoadingWallet.visibility = View.GONE
//            }
//        )
    }
}