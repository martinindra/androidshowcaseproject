package com.aTeam.beasy.View.HomeFeature.Profile.Adapter

import kotlin.math.exp

internal object ProfileFaqExpandableListData {
    val data : HashMap<String, List<String>>
    get() {
        val expandableListDetail = HashMap<String, List<String>>()

        val beasyDetail : MutableList<String> = ArrayList()
        beasyDetail.add("Bank Beasy is a life finance solutions provider supervised by Bank Indonesia and OJK. We help you save & send money, top up e-Wallet, pay bills, request money from your friends, anytime and anywhere you like.")

        val beasyOjk : MutableList<String> = ArrayList()
        beasyOjk.add("PT Bank Beasy Tbk is registered and supervised by Financial Services Authority (OJK) and guaranteed by Indonesia Deposit Insurance Corporation (LPS).")

        val beasyBenefit : MutableList<String> = ArrayList()
        beasyBenefit.add("Bank Beasy simplifies your life’s financial needs using state-of-the-art technology and guaranteed security.\n" +
                "\n" +
                "We help you:\n" +
                "- Allocate your money to saving that you can personalize to better manage your finances and gain interests.\n" +
                "- Enjoy benefits by collecting points from Daily Check In and Missions.\n" +
                "- Schedule and automate money transfers and savings so you don't forget paying bills or transfer money to your closest friends & relatives.\n" +
                "- Viewing your bank account mutation easily by getting your summary and detail report on expenditure and income visualised. ")

        val beasyRequirments : MutableList<String> = ArrayList()
        beasyRequirments.add("You can apply for Beasy account if you are of Indonesian Nationality which possesses an ID Card (e-KTP) and uses a smartphone with Indonesia SIM Card.")

        expandableListDetail["What is Beasy?"] = beasyDetail
        expandableListDetail["Is Beasy Protected by OJK/LPS?"] = beasyOjk
        expandableListDetail["What is the Benefit of Using Beasy?"] = beasyBenefit
        expandableListDetail["What are the main requirements to be Beasy Customer?"] = beasyRequirments
        return expandableListDetail
    }
}