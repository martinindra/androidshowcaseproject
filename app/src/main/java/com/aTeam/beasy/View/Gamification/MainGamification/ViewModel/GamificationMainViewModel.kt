package com.aTeam.beasy.View.Gamification.MainGamification.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationStart
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationStatusUser
import com.aTeam.beasy.Data.Model.Payment.ResponsePaymentLastTransaction
import com.aTeam.beasy.Data.Repository.Gamification.GamificationMainRepository
import com.aTeam.beasy.Data.Repository.Payment.PaymentMainRepository

class GamificationMainViewModel : ViewModel() {

    val gamificationMainRepository = GamificationMainRepository()

    val responseGamificationStatusUser = MutableLiveData<ResponseGamificationStatusUser>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getGamificationStatusUser(
        header : String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationUserMission(
            tokenUser = header,
            {
                isLoading.value = false
                responseGamificationStatusUser.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseGamificationStart = MutableLiveData<ResponseGamificationStart>()
    fun postGamificationStart(
        header : String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationUserStart(
            tokenUser = header,
            {
                isLoading.value = false
                responseGamificationStart.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

}