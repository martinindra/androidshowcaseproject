package com.aTeam.beasy.View.HomeFeature.Transfer

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.Adapter.TransferContactAdapter
import com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet.TransferAddContactBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.TransferMainContactViewModel
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.transfer_main_fragment.*


class TransferMainFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    val changeState = MutableLiveData<Boolean>()
    //token
    private lateinit var session: SessionManager

    //viewModel
    private lateinit var transferMainContactViewModel: TransferMainContactViewModel

    private lateinit var dataTransferContactAll : ArrayList<DataItemContact>
    private lateinit var dataTransferContactRecent : ArrayList<DataItemContact>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.transfer_main_fragment, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //get token
        session = SessionManager(context = requireContext())

        //viewmodel
        transferMainContactViewModel = ViewModelProviders.of(this).get(TransferMainContactViewModel::class.java)
        showAll()
        showRecent()

        dataTransferContactAll = arrayListOf()
        dataTransferContactRecent = arrayListOf()

        val transferBottomSheetFragment = TransferAddContactBottomSheetFragment {
            showAll()
            showRecent()
        }
        transfer_add_contact_button.setOnClickListener {
            transferBottomSheetFragment.show(parentFragmentManager, "BottomSheetDialog")
        }

        //swipe refresh data
        swipeTransferContact.setOnRefreshListener(this)

        btnSearchContact.setOnClickListener {
            val intent = Intent(requireContext(), TransferSearchContact::class.java)
            intent.putExtra(TransferSearchContact.EXTRA_DATA, dataTransferContactAll)
            Log.d("DataIntentContact", dataTransferContactAll.toString())
            startActivity(intent)
        }

        changeStateReload()

    }

    fun changeStateReload() {
        changeState.observe(viewLifecycleOwner, Observer {
            Log.d("disini", "disini ${it.toString()}")
            if (it == true){
                showAll()
                showRecent()
                changeState.value = false
            }
        })
    }

    fun showAll() {
        ivTransferNotification.setOnClickListener {
            val intent = Intent(requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }

        session.login?.let { transferMainContactViewModel.getContactData(header = it) }
        transferMainContactViewModel.responseData.observe(viewLifecycleOwner, Observer {
            dataTransferContactAll = it.data as ArrayList<DataItemContact>
            val adapterBaru = TransferContactAdapter(parentFragmentManager
            ) {
                showAll()
            }
            adapterBaru.setData(dataContact = dataTransferContactAll)
            rv_all_contact.apply {
                adapter = adapterBaru
                setHasFixedSize(true)
            }
        })

        transferMainContactViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Log.d("TransferContactError", it.localizedMessage)
            transferMainErrorFound.visibility = View.VISIBLE
        })

        transferMainContactViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if(it == true) refreshLayoutTransfer.visibility = View.VISIBLE
            else refreshLayoutTransfer.visibility = View.GONE
        })
    }

    fun showRecent() {
        session.login?.let { transferMainContactViewModel.getContactRecent(header = it) }
        transferMainContactViewModel.responseDataRecent.observe(viewLifecycleOwner, Observer {
            Log.d("DataRecentContact", it.data.toString())
            dataTransferContactRecent = it.data as ArrayList<DataItemContact>
            val adapterRecent = TransferContactAdapter(parentFragmentManager) {
                showRecent()
            }
            adapterRecent.setData(dataContact = dataTransferContactRecent)
            rv_recent_contact.apply {
                adapter = adapterRecent
                setHasFixedSize(true)
            }
        })

        transferMainContactViewModel.errorDataRecent.observe(viewLifecycleOwner, Observer {
            Log.d("TransferContactError", it.localizedMessage)
        })

        transferMainContactViewModel.isLoadingRecent.observe(viewLifecycleOwner, Observer {
            if(it == true) refreshLayoutTransfer.visibility = View.VISIBLE
            else refreshLayoutTransfer.visibility = View.GONE
        })
    }

    override fun onRefresh() {
        showAll()
        showRecent()
        swipeTransferContact.isRefreshing = false
    }

}