package com.aTeam.beasy.View.HomeFeature.Payment.Merchants.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.Merchant.DataItemMerchantList
import com.aTeam.beasy.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.payment_merchant_main_icon_list_item.view.*


class PaymentMerchantMainHorizontalListAdapter (var contextView : Context) :
    RecyclerView.Adapter<PaymentMerchantMainHorizontalListAdapter.ViewHolder>() {

    var dataMerchantList: ArrayList<DataItemMerchantList> = arrayListOf()
    fun setData(dataMerchant: ArrayList<DataItemMerchantList>) {
        this.dataMerchantList = dataMerchant
        notifyDataSetChanged()
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = itemView.civPaymentMerchantMainItemIconAvatar
        val title = itemView.tvPaymentMerchantMainItemTitle
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
        .inflate(R.layout.payment_merchant_main_icon_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataMerchantPosition = dataMerchantList.get(position)
        if (position <= 8) {
            val media = dataMerchantPosition.logo.toString()
            Glide
                .with(contextView)
                .load(media)
                .into(holder.image)

            holder.title.text = dataMerchantPosition.name
        }
    }

    override fun getItemCount(): Int = dataMerchantList.size
}