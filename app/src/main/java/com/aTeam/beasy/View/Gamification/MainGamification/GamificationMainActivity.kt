package com.aTeam.beasy.View.Gamification.MainGamification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.aTeam.beasy.Data.Model.Gamification.DataGamificationItemUserStatus
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailMissionGamification.GamificationDetailMissionActivity
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.GamificationDetailRewardAndMissionActivity
import com.aTeam.beasy.View.Gamification.MainGamification.Adapter.GamificationMainViewPagerAdapter
import com.aTeam.beasy.View.Gamification.MainGamification.ViewModel.GamificationMainViewModel
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gamification_main_activity.*

class GamificationMainActivity : AppCompatActivity() {
    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    private lateinit var gamificationViewModel : GamificationMainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gamification_main_activity)


        sessionManager = SessionManager(this)
        gamificationViewModel = ViewModelProviders.of(this).get(GamificationMainViewModel::class.java)

        gamificationBindData()

        ivGamificationBack.setOnClickListener {
            val intent = Intent(this, MainBaseActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun gamificationBindData() {
        sessionManager.login?.let {
            gamificationViewModel.getGamificationStatusUser(
                header = it
            )
        }
        responseGamification()
    }

    private fun responseGamification() {
        gamificationViewModel.responseGamificationStatusUser.observe(this, Observer {

            vpBind(it.data)
            tvGamificationWording.text = it.data?.planetWording
            tvGamificationMainPlanetName.text = "Planet " + it.data?.planetName
            Glide.with(this).load(it.data?.planetImage).into(ivGamificationMainPlanetImage)
        })

        gamificationViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to get Gamification Data", Toast.LENGTH_SHORT).show()
            gamificationMainErrorFound.visibility = View.VISIBLE
        })

        gamificationViewModel.isLoading.observe(this, Observer {
            if (it == true){
                gamificationMainLoading.visibility = View.VISIBLE
            }else{
                gamificationMainLoading.visibility = View.GONE
            }
        })
    }


    private fun vpBind(data: DataGamificationItemUserStatus?) {
        val adapter = GamificationMainViewPagerAdapter(supportFragmentManager, data?.planetSequence!!)
        viewpager_main.adapter = adapter

        viewpager_main?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

        btnGamificationMainPlanetNameExplore.setOnClickListener {
            val intent = Intent(this@GamificationMainActivity, GamificationDetailMissionActivity::class.java)
            intent.putExtra(GamificationDetailMissionActivity.IDPLANET, data.planetId)
            intent.putExtra(GamificationDetailMissionActivity.STATUSDELAY, data.isOnCompletionDelay.toString())
            startActivity(intent)
        }

        ivGamificationMainReward.setOnClickListener {
            startActivity(Intent(this@GamificationMainActivity, GamificationDetailRewardAndMissionActivity::class.java))
        }
    }

}