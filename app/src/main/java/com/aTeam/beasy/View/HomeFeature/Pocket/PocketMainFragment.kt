package com.aTeam.beasy.View.HomeFeature.Pocket

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aTeam.beasy.Data.Model.Pocket.DataItemPocketList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Base.ViewModel.BaseMainUserViewModel
import com.aTeam.beasy.View.HomeFeature.Pocket.Adapter.PocketMainListItemAdapter
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.aTeam.beasy.View.Maintenance.UnderConstructionActivity
import kotlinx.android.synthetic.main.pocket_main_fragment.*


class PocketMainFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    //token
    private lateinit var sessionManager: SessionManager

    //viewmodel
    //main user
    private lateinit var userMainBalance : BaseMainUserViewModel
    //pocket view model
    private lateinit var pocketViewModel: PocketMainViewModel

    //function resource
    private var functionResource : FunctionResource = FunctionResource()

    //data
    private lateinit var dataPocketList : ArrayList<DataItemPocketList>

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pocket_main_fragment, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(requireContext())
        userMainBalance = ViewModelProviders.of(this).get(BaseMainUserViewModel::class.java)
        pocketViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)

        ivFloatingButton.setOnClickListener(this)
        ibToggleEyeMainBalance.setOnClickListener(this)

        userMainBalanceView()
        pocketListProcess()
        bell()

        swipePocketBalance.setOnRefreshListener(this)

    }

    private fun pocketListProcess() {
        sessionManager.login?.let { pocketViewModel.getPocketList(header = it, with_primary = false) }
        responsePocket()
    }

    private fun responsePocket() {
        pocketViewModel.responsePocketList.observe(viewLifecycleOwner, Observer {
            bindData(it.data)
        })
        pocketViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get data pocket", Toast.LENGTH_SHORT).show()
        })
        pocketViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) pocketHomeLoading.visibility = View.VISIBLE
            else pocketHomeLoading.visibility = View.GONE
        })
    }

    private fun bindData(data: List<DataItemPocketList?>?) {
        dataPocketList = data as ArrayList<DataItemPocketList>
        val adapterPocket = PocketMainListItemAdapter(parentFragmentManager, requireContext())
        adapterPocket.setData(dataPocketList)

        rvMiniPocketInMainPocket.apply {
            adapter = adapterPocket
            layoutManager = GridLayoutManager(requireContext(), 2)
            setHasFixedSize(true)
        }
    }

    private fun userMainBalanceView() {
        sessionManager.login?.let { userMainBalance.getUserMainBalance(header = it) }
        userMainBalance.responseUserViewModel.observe(viewLifecycleOwner, Observer {
            Log.d("coba", it.data.toString())
            val userBalance = it.data?.balance
            tvPocketMainBalance.text = functionResource.rupiah(userBalance?.toDouble()!!).toString()
        })

        userMainBalance.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get user balance", Toast.LENGTH_SHORT).show()
            tvPocketMainBalance.text = "IDR -"
        })

        userMainBalance.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true) pocketHomeLoading.visibility = View.VISIBLE
            else pocketHomeLoading.visibility = View.GONE
        })
    }

    fun bell (){
        ivBell.setOnClickListener {
            val intent = Intent (requireContext(), UnderConstructionActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ivFloatingButton -> {
                val addPocket = Intent(requireContext(), PocketAddActivity::class.java)
                startActivity(addPocket)
            }
            R.id.ibToggleEyeMainBalance -> {
                if(tvPocketMainBalance.transformationMethod is PasswordTransformationMethod){
                    tvPocketMainBalance.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    ibToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_24)
                } else{
                    tvPocketMainBalance.transformationMethod = PasswordTransformationMethod.getInstance()
                    ibToggleEyeMainBalance.setImageResource(R.drawable.ic_outline_visibility_off_24)
                }
            }
        }
    }

    override fun onRefresh() {
        userMainBalanceView()
        pocketListProcess()
        swipePocketBalance.isRefreshing = false
    }

}