package com.aTeam.beasy.View.Login

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.NetworkConnection
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import com.aTeam.beasy.View.Login.RecoverPassword.RecoverPasswordActivity
import com.aTeam.beasy.View.Login.ViewModel.LoginMainViewModel
import com.aTeam.beasy.View.OnBoarding.OnBoardMainActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.all_resource_internet_checkd_dialog.view.*
import kotlinx.android.synthetic.main.loading_beasy_gif.view.*
import kotlinx.android.synthetic.main.login_dialog_recover_password.view.*
import kotlinx.android.synthetic.main.login_main_activity.*
import kotlinx.android.synthetic.main.payment_mobile_main_activity.*
import kotlinx.android.synthetic.main.transfer_delete_account_dialog.view.*
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import kotlin.math.log

class LoginMainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var btnClickHere : TextView
    private lateinit var btnSignIn: Button
    private lateinit var btnRegister : TextView

    //view model
    private lateinit var loginMainViewModel: LoginMainViewModel

    //session
    private lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_main_activity)
        checkConnectivity()

        // Button Text View Click Here
        btnClickHere = findViewById(R.id.tvClickHere)
        btnClickHere.setOnClickListener(this)

        // Button Sign In
        btnSignIn = findViewById(R.id.signInBtn)
        btnSignIn.setOnClickListener(this)

        // Button Text View Register
        btnRegister = findViewById(R.id.tvRegister)
        btnRegister.setOnClickListener(this)

        // Email Validation for Sign In Button Activation
        etEmail.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()) {
                    signInBtn.isEnabled = etPassword.text.toString().isNotEmpty()
                }else{
                    signInBtn.isEnabled = false
                }
            }
        })

        // Password Checker
        etPassword.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //For activation button
                if (etPassword.text.toString().isNotEmpty())
                    signInBtn.isEnabled = android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()
                else{
                    signInBtn.isEnabled = false
                }
            }
        })

        //view model
        loginMainViewModel = ViewModelProviders.of(this).get(LoginMainViewModel::class.java)

        //session
        sessionManager = SessionManager(this)
    }

    private fun checkConnectivity() {
        val networkConnection = NetworkConnection(applicationContext)
        networkConnection.observe(this, Observer { isConnected ->
            if (!isConnected) {
                val context = this
                val builder = AlertDialog.Builder(context)

                val view =
                    layoutInflater.inflate(R.layout.all_resource_internet_checkd_dialog, null)

                val dialog = builder?.setView(view)?.create()
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window?.setBackgroundDrawableResource(
                    R.drawable.all_resource_white_rectangle_radius
                )

                dialog?.show()
            }
        })
    }

    override fun onClick(v: View){
        when(v.id){
            R.id.signInBtn ->{
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()

                loginMainViewModel.loginViewModel(email, password)

                loginComponent()
            }
            R.id.tvClickHere -> {
                val forgotPassword = Intent(this@LoginMainActivity, RecoverPasswordActivity::class.java)
                startActivity(forgotPassword)
            }
            R.id.tvRegister -> {
                val register = Intent(this@LoginMainActivity, OnBoardMainActivity::class.java)
                startActivity(register)
            }
        }
    }

    private fun loginComponent() {
        loginMainViewModel.responseData.observe(this, Observer {
            sessionManager.login = "Bearer " + it.data?.token.toString()

            val signIn = Intent(this@LoginMainActivity, MainBaseActivity::class.java)
            startActivity(signIn)
            finish()
        })

        loginMainViewModel.errorData.observe(this, Observer {
            when(it){
                is HttpException -> {
                    tvMessageLogin.text = "You have entered wrong Email/Password"
                    etEmail.setBackgroundResource(R.drawable.bg_form_wrong)
                    etPassword.setBackgroundResource(R.drawable.bg_form_wrong)
                    Log.d("loginWow", it.toString())
                }
                is SocketTimeoutException -> {
                    tvMessageLogin.text = "Timeout Connection"
                    Log.d("loginWow", it.toString())
                }
                is IOException -> {
                    tvMessageLogin.text = "Please check your internet connection"
                    Log.d("loginWow", it.toString())
                }
            }
        })
        loginMainViewModel.isLoading.observe(this, Observer {
            showLoading(it)
        })
    }

    private fun showLoading(it: Boolean?) {
        if (it == true) {
            loadingWait.visibility = View.VISIBLE
        }
        else{
            loadingWait.visibility = View.GONE
        }
    }
}