package com.aTeam.beasy.View.HomeFeature.Pocket

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Pocket.DataPocketHistory
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import com.aTeam.beasy.View.HomeFeature.Pocket.Adapter.PocketByIdHistoryItemListAdapter
import com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet.PocketDeleteEditBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet.PocketFilterHistoryBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet.PocketMoveBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet.PocketTopUpBottomSheetFragment
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.pocket_add_activity.*
import kotlinx.android.synthetic.main.pocket_filter_history_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.pocket_planning_activity.*
import kotlinx.android.synthetic.main.pocket_upload_image_photo_dialog.view.*
import java.text.SimpleDateFormat
import java.util.*

class PocketPlanningActivity : AppCompatActivity(), View.OnClickListener {

    companion object{
        const val POCKETID = "pocketID"
        const val POCKETNAME = "pocketName"
        const val DUEDATE = "dueDate"
    }

    //token
    private lateinit var sessionmanager : SessionManager

    //view model
    private lateinit var pocketMainViewModel : PocketMainViewModel

    //function resource
    private var functionResource = FunctionResource()

    //history data
    private lateinit var dataHistoryPocket : ArrayList<DataPocketHistory>

    private lateinit var btnBack : ImageButton
    private lateinit var imageProfile: ImageView
    private lateinit var nameOfPocket : TextView
    private lateinit var balance : TextView
    private lateinit var dueDateDDMMYY : TextView
    private lateinit var image : String
    private var target : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pocket_planning_activity)

        sessionmanager = SessionManager(this)
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)
        val pocketId = intent.getStringExtra(POCKETID)
        val pocketName = intent.getStringExtra(POCKETNAME)
        val dueDateData = intent.getStringExtra(DUEDATE)

        ivToggleEyeMainPocketPage.setOnClickListener(this)
        ibToggleEyeInsidePocket.setOnClickListener(this)

        //Button Back//
        btnBack = findViewById(R.id.ibArrowPlanning)
        btnBack.setOnClickListener(this)

        //Intent Pass Data From Add Activity//
        imageProfile = findViewById(R.id.ivImageProfilePlanning)
        imageProfile.setOnClickListener(this)
        nameOfPocket = findViewById(R.id.tvNameOfPocket)
        balance = findViewById(R.id.tvBalancePocket)
        dueDateDDMMYY = findViewById(R.id.tvDueDateDDMMYY)

        //get data by viewModel
        getDataProcess(pocketId)
        getHistoryProcess(pocketId)
        buttonBottomSheet(pocketId, pocketName)


    }
    private fun dateFormat(dueDate: String?) : String {
        val tanggal = dueDate.toString()
        val parser =  SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val formatter = SimpleDateFormat("dd MMMM yyyy", Locale("en"))
        val formattedDate = formatter.format(parser.parse(tanggal))
        return formattedDate
    }
    private fun getHistoryProcess(pocketId: String?) {
        sessionmanager.login?.let {
            pocketMainViewModel.getPocketHistoryById(
                header = it,
                pocketId = pocketId!!
            )
        }

        pocketHistoryProcess()
    }

    private fun pocketHistoryProcess() {
        pocketMainViewModel.responsePocketByIdHistory.observe(this, Observer {
            dataHistoryPocket = arrayListOf()
            dataHistoryPocket = it.data as ArrayList<DataPocketHistory>
            rvBind(dataHistoryPocket)
        })

        pocketMainViewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(this, "Failed to get History of Pocket", Toast.LENGTH_SHORT).show()
        })

        pocketMainViewModel.isLoading.observe(this, Observer {
            if(it == true) pocketPlanningLoading.visibility = View.VISIBLE
            else pocketPlanningLoading.visibility = View.GONE
        })
    }

    private fun rvBind(dataHistoryPocket: ArrayList<DataPocketHistory>) {
        val adapterHistory = PocketByIdHistoryItemListAdapter()
        adapterHistory.setData(dataPocketHistory = dataHistoryPocket)

        rvHistoryPocket.apply {
            adapter = adapterHistory
            setHasFixedSize(true)
        }
    }

    private fun getDataProcess(pocketId: String?) {
        sessionmanager.login?.let {
            pocketMainViewModel.getPocketById(
                header = it,
                pocketId = pocketId!!
            )
        }

        responseData()
    }

    private fun responseData() {
        pocketMainViewModel.responsePocketById.observe(this, Observer {
            //<!-- Passing Data -->//
            //Intent Image Picker//
            image = it.data?.picture!!
            Glide.with(this).load(image).into(ivImageProfilePlanning)
            //End Intent Image Picker//
            nameOfPocket.text = it.data?.pocketName
            balance.text = functionResource.rupiah(it.data?.balance?.toDouble()!!).toString()
            val dateData = dateFormat(it.data.dueDate.toString())
            dueDateDDMMYY.text = dateData
            target = it.data.target!!

            tvTotalProgressInsidePocket.text = functionResource.rupiah(it.data.target?.toDouble()!!).toString()
            val percent : Double? = (it.data.balance.toDouble() / it.data.target.toDouble()) * 100
            pbInsidePocket.setProgress(percent?.toInt()!!)
            tvProgressBarInsidePocket.text = percent!!.toInt().toString() + "%"
        })

        pocketMainViewModel.responseErrorData.observe(this, Observer {
            pocketPlanningErrorData.visibility = View.VISIBLE
            Toast.makeText(this, "Failed to get Pocket Data", Toast.LENGTH_SHORT).show()
        })

        pocketMainViewModel.isLoading.observe(this, Observer {
            if(it == true) pocketPlanningLoading.visibility = View.VISIBLE
            else pocketPlanningLoading.visibility = View.GONE
        })
    }

    private fun buttonBottomSheet(pocketId: String?, pocketName: String?) {
        //Button Edit and Delete//
        ibDotVerticalPlanning.setOnClickListener{
            val pocketDeleteEditBottomSheetFragment = PocketDeleteEditBottomSheetFragment(
                pocketId ?: "",
                image?:"",
                pocketName?: "",
                dueDateDDMMYY.text.toString() ?: "",
                target ?: 0
            ){
                getDataProcess(pocketId)
                getHistoryProcess(pocketId)
            }
            pocketDeleteEditBottomSheetFragment.show(supportFragmentManager, "TAG")
        }

        //Button Top Up//
        ivTopUpButtonInsidePocket.setOnClickListener {
            val pocketTopUpBottomSheetFragment = PocketTopUpBottomSheetFragment(pocketId = pocketId
            ) {
                getDataProcess(pocketId)
                getHistoryProcess(pocketId)
            }
            pocketTopUpBottomSheetFragment.show(supportFragmentManager, "TAG")
        }

        //Button Move//
        ivMoveButtonInsidePocket.setOnClickListener {
            val pocketMoveBottomSheetFragment = PocketMoveBottomSheetFragment(pocketId = pocketId, pocketNames = pocketName) {
                getDataProcess(pocketId)
                getHistoryProcess(pocketId)
            }
            pocketMoveBottomSheetFragment.show(supportFragmentManager, "TAG")
        }

        //Short and Filter//
        tvShortByOnPocket.setOnClickListener {
            val pocketFilterHistoryBottomSheetFragment = PocketFilterHistoryBottomSheetFragment(pocketId = pocketId.toString()) {
                val data = it as ArrayList<DataPocketHistory>
                Log.d("datafilter", data.toString())
                rvBind(data)
            }
            pocketFilterHistoryBottomSheetFragment.show(supportFragmentManager, "TAG")
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            //Image Profile
            R.id.ivImageProfilePlanning -> {
                val view = View.inflate(this@PocketPlanningActivity, R.layout.pocket_upload_image_photo_dialog, null)

                val builder = AlertDialog.Builder(this@PocketPlanningActivity)
                builder.setView(view)

                val dialog = builder.create()
                dialog.show()
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                //If click Yes
                view.btnUploadImageDialogYes.setOnClickListener {
                    ImagePicker.with(this)
                        .galleryOnly()
                        .cropSquare()
                        .galleryMimeTypes(mimeTypes = arrayOf("image/png", "image/jpg", "image/jpeg"))
                        .maxResultSize(1080, 1080)
                        .saveDir(getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!) //For Save the picture
                        .start()
                    dialog.dismiss()
                }

                //If click No
                view.btnUploadImageDialogNo.setOnClickListener {
                    dialog.dismiss()
                }
            }
            //Toggle Eye Main Balance Planning Pocket
            R.id.ivToggleEyeMainPocketPage -> {
                if(tvBalancePocket.transformationMethod is PasswordTransformationMethod){
                    tvBalancePocket.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    ivToggleEyeMainPocketPage.setImageResource(R.drawable.ic_outline_visibility_24)
                } else{
                    tvBalancePocket.transformationMethod = PasswordTransformationMethod.getInstance()
                    ivToggleEyeMainPocketPage.setImageResource(R.drawable.ic_outline_visibility_off_24)
                }
            }
            //Toggle Eye Balance Proggress Planning Pocket
            R.id.ibToggleEyeInsidePocket -> {
                if (tvTotalProgressInsidePocket.transformationMethod is PasswordTransformationMethod) {
                    tvTotalProgressInsidePocket.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    ibToggleEyeInsidePocket.setImageResource(R.drawable.ic_outline_visibility_24)
                } else {
                    tvTotalProgressInsidePocket.transformationMethod = PasswordTransformationMethod.getInstance()
                    ibToggleEyeInsidePocket.setImageResource(R.drawable.ic_outline_visibility_off_24)
                }
            }
            //Button Back
            R.id.ibArrowPlanning -> {
                onBackPressed()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri: Uri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            ivImageProfilePlanning?.setImageURI(fileUri)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

}