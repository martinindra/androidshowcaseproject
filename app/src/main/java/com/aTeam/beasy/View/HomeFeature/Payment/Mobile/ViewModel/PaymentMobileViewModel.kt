package com.aTeam.beasy.View.HomeFeature.Payment.Mobile.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.ResponsePaymentMobileCreditAirtimeReload
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.ResponsePaymentMobileInternetData
import com.aTeam.beasy.Data.Model.Payment.MobileInternet.ResponsePaymentMobilePay
import com.aTeam.beasy.Data.Repository.Payment.PaymentMobileRepository

class PaymentMobileViewModel : ViewModel() {
    val paymentMobileRepository = PaymentMobileRepository()

    val responseAirtimeReloadData = MutableLiveData<ResponsePaymentMobileCreditAirtimeReload>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getPaymentAirtimeReloadData(
        header : String,
        phoneNumber : String
    ){
        isLoading.value = true
        paymentMobileRepository.paymentMobileAirtimeReload(
            tokenUser = header,
            phoneNumber = phoneNumber,
            {
                isLoading.value = false
                responseAirtimeReloadData.value = it
            },
            {
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseInternetData = MutableLiveData<ResponsePaymentMobileInternetData>()
    fun getPaymentInternetData(
        header: String,
        phoneNumber: String
    ){
        isLoading.value = true
        paymentMobileRepository.paymentMobileInternetData(
            tokenUser = header,
            phoneNumber = phoneNumber,
            {
                isLoading.value = false
                responseInternetData.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseMobilePay = MutableLiveData<ResponsePaymentMobilePay>()
    fun postMobilePay(
        header: String,
        id : String,
        pin : Int,
        phoneNumber: String
    ){
        isLoading.value = true
        paymentMobileRepository.postMobilePay(
            tokenUser = header,
            phoneNumber = phoneNumber,
            id = id,
            pin = pin,
            {
                isLoading.value = false
                responseMobilePay.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }
}