package com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Transfer.ResponseTransferContact
import com.aTeam.beasy.Data.Repository.Transfer.TransferContactRepository

class TransferAddGetContactByIdViewModel : ViewModel() {

    val transferContactRepository = TransferContactRepository()

    val responseData = MutableLiveData<ResponseTransferContact>()
    val errorData = MutableLiveData<Throwable>()

    fun getDataContactById(header:String, id : String){
        transferContactRepository.getContactById(
            tokenUser = header,
            idUser = id,
            {
                responseData.value = it
            },
            {
                errorData.value = it
            }
        )
    }
}