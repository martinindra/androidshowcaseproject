package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEditAccount
import com.aTeam.beasy.Data.Repository.Ewallet.WalletContactRepository

class WalletEditContactViewModel : ViewModel() {
    val walletContactRepository = WalletContactRepository()

    val responseData = MutableLiveData<ResponseEditAccount>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun editContact(header:String, name:String, eWalletId:String, accountNumberWallet:String, id:String){
        isLoading.value = true

        walletContactRepository.editAccount(
            tokenUser = header,
            idUser = id,
            name = name,
            eWalletId = eWalletId,
            accountNumberWallet = accountNumberWallet,
            {

                isLoading.value = false
                responseData.value = it
            },
            {
                isLoading.value = false
                errorData.value = it
            }
        )
    }
}