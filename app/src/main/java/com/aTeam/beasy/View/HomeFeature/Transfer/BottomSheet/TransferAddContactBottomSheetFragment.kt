package com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Bank.DataItemBank
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.TransferMainFragment
import com.aTeam.beasy.View.HomeFeature.Transfer.TransferPageActivity
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.transfer_add_contact_bottom_sheet_fragment.*
import kotlinx.android.synthetic.main.transfer_add_contact_bottom_sheet_fragment.view.*
import kotlinx.android.synthetic.main.transfer_main_fragment.*

class TransferAddContactBottomSheetFragment(var stateChange : () -> Unit) : BottomSheetDialogFragment() {
    //token
    private lateinit var session: SessionManager

    //parent fragment
    private lateinit var transferMainFragment : TransferMainFragment

    //viewModel
    private lateinit var transferBankListViewModel: TransferBankListViewModel
    private lateinit var transferAddContactViewModel: TransferAddContactViewModel
    private lateinit var transferAddGetContactByIdViewModel: TransferAddGetContactByIdViewModel

    private lateinit var bankData : ArrayList<DataItemBank>
    private lateinit var bankName : ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.transfer_add_contact_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewmodel
        transferBankListViewModel = ViewModelProviders.of(this).get(TransferBankListViewModel::class.java)
        transferAddContactViewModel = ViewModelProviders.of(this).get(TransferAddContactViewModel::class.java)
        transferAddGetContactByIdViewModel = ViewModelProviders.of(this).get(TransferAddGetContactByIdViewModel::class.java)


        //mainfragment
        transferMainFragment = TransferMainFragment()

        //bank
        bankName = arrayListOf()
        //get session data
        session = SessionManager(requireContext())
        //bank list
        bankList()

        var bankNameSelected : String? = null
        var bankIdSelectd : String? = null
        transfer_edt_select_bank_contact.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                bankNameSelected = bankData[p2].bankName.toString()
                bankIdSelectd = bankData[p2].bankId.toString()
            }

        }

        transfer_add_recipient_button.setOnClickListener {
            var errorAdd = false

            if (transfer_edt_name_contact.text.toString().trim().isEmpty()){
                tvMessageEdTransferAddAccountName.text = "Please fill this field!"
                tvMessageEdTransferAddAccountName.setTextColor(Color.parseColor("#FF0000"))
                transfer_edt_name_contact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }

            if (transfer_edt_account_number_contact.text.toString().trim().isEmpty()){
                tvMessageEdTransferAddAccountBeneficiaryNumber.text = "Please fill this field!"
                tvMessageEdTransferAddAccountBeneficiaryNumber.setTextColor(Color.parseColor("#FF0000"))
                transfer_edt_account_number_contact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }

            if (transfer_edt_account_number_contact.text.toString().length  < 12) {
                tvMessageEdTransferAddAccountBeneficiaryNumber.text = "Please enter correct number!"
                tvMessageEdTransferAddAccountBeneficiaryNumber.setTextColor(Color.parseColor("#FF0000"))
                transfer_edt_account_number_contact.setBackgroundResource(R.drawable.bg_form_wrong)
                errorAdd = true
            }

            if (!errorAdd){
                session.login?.let { it1 -> transferAddContactViewModel.addContactViewModel(
                    header = it1,
                    name = transfer_edt_name_contact.text.toString(),
                    bankId = bankIdSelectd.toString(),
                    accountNumber = transfer_edt_account_number_contact.text.toString()
                ) }

                transferAddContactViewModel.responseData.observe(viewLifecycleOwner, Observer {

                    Snackbar.make(requireActivity().findViewById(R.id.rlMainHomeBaseContent),transfer_edt_name_contact.text.toString() + " Success Added", Snackbar.LENGTH_LONG).show()
                    dismiss()
                    transfer_edt_name_contact.setText("")
                    transfer_edt_account_number_contact.setText("")
                    stateChange()
                })

                transferAddContactViewModel.errorData.observe(viewLifecycleOwner, Observer {
                    Toast.makeText(requireContext(), transfer_edt_name_contact.text.toString() + "Failed to add!", Toast.LENGTH_LONG).show()
                })

                transferAddContactViewModel.isLoading.observe(viewLifecycleOwner, Observer {
                    if (it == true) skAddEditLoading.visibility = View.VISIBLE
                    else skAddEditLoading.visibility = View.GONE
                })
            }
        }
    }

    private fun bankList() {
        session.login?.let { transferBankListViewModel.getBankData(header = it) }
        transferBankListViewModel.responseData.observe(viewLifecycleOwner, Observer {
            bankData = it.data as ArrayList<DataItemBank>
            for (i in bankData.indices){
                bankName.add(bankData.get(i).bankName.toString())
            }
            transfer_edt_select_bank_contact.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, bankName)
        })

        transferBankListViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Snackbar.make(requireActivity().findViewById(R.id.rlTransferAddBottomConten),  "Bank data is null", Snackbar.LENGTH_LONG).show()
        })

        transferBankListViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            transfer_add_recipient_button.isEnabled = it != true
        })
    }

    override fun dismiss() {
        super.dismiss()
    }
}
