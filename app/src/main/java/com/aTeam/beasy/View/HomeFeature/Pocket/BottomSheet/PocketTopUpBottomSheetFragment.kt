package com.aTeam.beasy.View.HomeFeature.Pocket.BottomSheet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Pocket.DataItemPocketList
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pocket_top_up_bottom_sheet_fragment.*

class PocketTopUpBottomSheetFragment(var pocketId : String?, var statChange : () -> Unit) : BottomSheetDialogFragment() {

    //session
    private lateinit var sessionManager: SessionManager

    // viewModel
    private lateinit var pocketMainViewModel: PocketMainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pocket_top_up_bottom_sheet_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)
        Log.d("pocketIdTopUp", pocketId.toString())
        btnDone(pocketId)
    }

    private fun btnDone(pocketId: String?) {
        buttonDoneOnTopUpBottomSheet.setOnClickListener {
            var errorAdd = false
            val amount = etEnterAmountTopUp.text.toString()
            if (amount.isEmpty()){
                errorAdd = true
                etEnterAmountTopUp.error = "Please Fill this field"
                etEnterAmountTopUp.setBackgroundResource(R.drawable.all_resource_bg_white_fillform_error)
            }

//            if (amount.toInt() < 1000){
//                etEnterAmountTopUp.error = "Amount must higher than IDR 1,000"
//                etEnterAmountTopUp.setBackgroundResource(R.drawable.all_resource_bg_white_fillform_error)
//                errorAdd = true
//            }

            if (!errorAdd){
                inputProcess(amount.toInt(), pocketId)
            }
        }
    }

    private fun inputProcess(amount: Int, pocketId: String?) {
        sessionManager.login?.let {
            pocketMainViewModel.postTopUpPocket(
                header = it,
                pocketId = pocketId.toString(),
                amount = amount
            )
        }

        responseTopUp()
    }

    private fun responseTopUp() {
        pocketMainViewModel.responsePocketTopUp.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Success Top Up", Toast.LENGTH_SHORT).show()
            dismiss()
            statChange()
        })

        pocketMainViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed top up", Toast.LENGTH_SHORT).show()
        })

    }

    override fun dismiss() {
        super.dismiss()
    }

}