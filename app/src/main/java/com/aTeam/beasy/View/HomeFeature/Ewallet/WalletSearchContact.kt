package com.aTeam.beasy.View.HomeFeature.Ewallet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.aTeam.beasy.Data.Model.Ewallet.DataItemAccount
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Ewallet.Adapter.WalletContactAdapter
import kotlinx.android.synthetic.main.wallet_search_contact_activity.*

class WalletSearchContact : AppCompatActivity() {
    companion object{
        const val EXTRA_DATA_WALLET = "extra_data-wallet"
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallet_search_contact_activity)

        ivBackWalletSearch.setOnClickListener {
            onBackPressed()
        }

        val dataAccount = intent.getParcelableArrayListExtra<DataItemAccount>(EXTRA_DATA_WALLET)

        val adapter = WalletContactAdapter(supportFragmentManager) {
        }
        adapter.setData(dataEwallet = dataAccount as ArrayList<DataItemAccount>)
        rvWalletSearchContact.adapter = adapter
        rvWalletSearchContact.setHasFixedSize(true)

        etWalletSearchSearch.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                adapter.filter.filter(s.toString())
            }
        })

    }
}