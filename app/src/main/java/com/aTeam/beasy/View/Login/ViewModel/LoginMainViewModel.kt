package com.aTeam.beasy.View.Login.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Auth.ResponseAuthLoginSuccess
import com.aTeam.beasy.Data.Repository.Auth.AuthLoginRepository
import com.aTeam.beasy.Util.ValidateResult

class LoginMainViewModel : ViewModel(){
    val authLoginRepository = AuthLoginRepository()

    val responseData = MutableLiveData<ResponseAuthLoginSuccess>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    val resultValidate = MutableLiveData<ValidateResult>()

    fun loginViewModel(email:String, password:String){
        isLoading.value = true
        if (email.isEmpty() && password.isEmpty()){
            resultValidate.postValue(ValidateResult.invalid(0, "Email & Password required"))
        }else if (email.isEmpty()){
            resultValidate.postValue(ValidateResult.invalid(1, "Email required"))
        }else if (password.isEmpty()){
            resultValidate.postValue(ValidateResult.invalid(2, "Password required"))
        }else{
            authLoginRepository.login(email,password,
                {
                    responseData.value = it
                    isLoading.value = false
                },
                {
                    errorData.value = it
                    isLoading.value = false
                }
            )
            resultValidate.postValue(ValidateResult.valid())
        }

    }
}