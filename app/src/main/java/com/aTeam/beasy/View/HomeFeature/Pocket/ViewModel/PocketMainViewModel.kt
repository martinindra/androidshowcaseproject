package com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Pocket.*
import com.aTeam.beasy.Data.Repository.Pocket.PocketMainRepository
import java.io.File

class PocketMainViewModel : ViewModel() {

    val pocketMainRepository = PocketMainRepository()

    val responsePocketList = MutableLiveData<ResponsePocketList>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getPocketList(
        header : String,
        with_primary : Boolean
    ){
        isLoading.value = true

        pocketMainRepository.pocketMainList(
            tokenUser = header,
            with_primary = with_primary,
            {
                isLoading.value = false
                responsePocketList.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketById = MutableLiveData<ResponsePocketById>()
    fun getPocketById(
        header: String,
        pocketId : String
    ){
        isLoading.value = true
        pocketMainRepository.pocketById(
            tokenUser = header,
            pocketId = pocketId,
            {
                isLoading.value = false
                responsePocketById.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketByIdHistory = MutableLiveData<ResponsePocketHistory>()
    fun getPocketHistoryById(
        header: String,
        pocketId : String
    ){
        isLoading.value = true
        pocketMainRepository.pocketByIdHistory(
            tokenUser = header,
            pocketId = pocketId,
            {
                isLoading.value = false
                responsePocketByIdHistory.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    fun getPocketHistoryByIdFilter(
        header: String,
        pocketId : String,
        sort : String,
        type : String
    ){
        isLoading.value = true
        pocketMainRepository.pocketByIdHistoryFilter(
            tokenUser = header,
            pocketId = pocketId,
            sort = sort,
            type = type,
            {
                isLoading.value = false
                responsePocketByIdHistory.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketTopUp = MutableLiveData<ResponsePocketTopUp>()
    fun postTopUpPocket(
        header: String,
        pocketId : String,
        amount : Int
    ){
        isLoading.value = true
        pocketMainRepository.pocketTopUp(
            tokenUser = header,
            pocketId = pocketId,
            amount = amount,
            {
                isLoading.value = false
                responsePocketTopUp.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketMove = MutableLiveData<ResponsePocketMove>()
    fun postMovePocket(
        header: String,
        pocketId : String,
        amount : Int,
        destination : String
    ){
        isLoading.value = true
        pocketMainRepository.pocketMove(
            tokenUser = header,
            pocketId = pocketId,
            destination = destination,
            amount = amount,
            {
                isLoading.value = false
                responsePocketMove.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }


    val responsePocketCreate = MutableLiveData<ResponsePocketCreate>()
    fun pocketCreate(
        header: String,
        dueDate : String,
        name : String,
        picture : File,
        target : Int
    ){
        isLoading.value = true
        pocketMainRepository.pocketCreate(
            tokenUser = header,
            dueDate = dueDate,
            namee = name,
            picture = picture,
            target = target,
            {
                isLoading.value = false
                responsePocketCreate.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketEdit = MutableLiveData<ResponsePocketEdit>()
    fun pocketEdit(
        header: String,
        dueDate: String,
        name: String,
        target: Int,
        pocketId: String
    ){
        isLoading.value = true
        pocketMainRepository.pocketEdit(
            tokenUser = header,
            pocketId = pocketId,
            dueDate = dueDate,
            namee =  name,
            target = target,
            {
                isLoading.value = false
                responsePocketEdit.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responsePocketDelete = MutableLiveData<ResponsePocketDelete>()
    fun pocketDelete(
        header: String,
        pocketId: String
    ){
        isLoading.value = true
        pocketMainRepository.pocketDelete(
            tokenUser = header,
            pocketId = pocketId,
            {
                isLoading.value = false
                responsePocketDelete.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }
}