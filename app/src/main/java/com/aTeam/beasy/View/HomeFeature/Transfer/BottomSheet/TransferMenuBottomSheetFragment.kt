package com.aTeam.beasy.View.HomeFeature.Transfer.BottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Transfer.ViewModel.TransferDeleteContactViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.transfer_delete_account_dialog.view.*
import kotlinx.android.synthetic.main.transfer_menu_bottom_sheet_fragment.*

class TransferMenuBottomSheetFragment(
    var arrayContactData: DataItemContact,
    var callbackMenu : () -> Unit
    ) : BottomSheetDialogFragment() {

    //session
    private lateinit var sessionManager: SessionManager

    //viewmodeldelete
    private lateinit var transferDeleteContactViewModel: TransferDeleteContactViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.transfer_menu_bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sessionManager = SessionManager(requireContext())
        transferDeleteContactViewModel = ViewModelProviders.of(this).get(TransferDeleteContactViewModel::class.java)

        tvTransferMenuDeleteAccount.setOnClickListener{
            showDeleteDialog()
        }

        tvTransferMenuEditAccount.setOnClickListener{
            val transferEditContactBottomSheetFragment =
                TransferEditContactBottomSheetFragment(arrayContactData,
                    this@TransferMenuBottomSheetFragment
                ) {
                    callbackMenu()
                }
            transferEditContactBottomSheetFragment.show(parentFragmentManager, "EditBottomSheetDialog")
        }
    }

    fun showDeleteDialog() {
        val context = getContext()
        val builder = context?.let { AlertDialog.Builder(it) }

        val view = layoutInflater.inflate(R.layout.transfer_delete_account_dialog, null)

        val dialog = builder?.setView(view)?.create()

        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawableResource(
            R.drawable.all_resource_white_rectangle_radius
        )

        view.btnTransferDeleteDialogCancel.setOnClickListener {
            dialog?.dismiss()
        }
        view.btnTransferDeleteDialogSubmit.setOnClickListener {
            sessionManager.login?.let { it1 -> transferDeleteContactViewModel.deleteData(
                header = it1,
                idUser = arrayContactData.id.toString()
            ) }
            deleteAction(view, dialog)
        }

        dialog?.show()
    }

    override fun dismiss() {
        super.dismiss()
    }
    private fun deleteAction(view: View, dialog: AlertDialog?) {
        transferDeleteContactViewModel.responseData.observe(
            viewLifecycleOwner, Observer {
                Toast.makeText(context, "${arrayContactData.name} Delete success!", Toast.LENGTH_SHORT).show()
                dialog?.dismiss()
                dismiss()
                callbackMenu()
            }
        )
        transferDeleteContactViewModel.errorData.observe(
            viewLifecycleOwner, Observer {
                Toast.makeText(context, "${it.localizedMessage}", Toast.LENGTH_LONG).show()
            }
        )
        transferDeleteContactViewModel.isLoading.observe(
            viewLifecycleOwner, Observer {
                if (it == true) view.skDeleteLoading.visibility = View.VISIBLE
                else view.skDeleteLoading.visibility = View.GONE
            }
        )
    }
}