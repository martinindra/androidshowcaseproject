package com.aTeam.beasy.View.SplashScreen

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Payment.Merchants.PaymentMerchantQrCodeActivity
import com.aTeam.beasy.View.OnBoarding.OnBoardMainActivity
import kotlinx.android.synthetic.main.splash_screen_activity.*

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)

        ivSplashBackground.animate().translationY(2500f).setDuration(1000).setStartDelay(3000)
        ivSplashLogo.animate().translationY(-1600f).setDuration(1000).setStartDelay(3000)
        laSplash.animate().translationY(-3000f).setDuration(1000).setStartDelay(2800)

        Handler().postDelayed(
            {
                val intent = Intent(this, OnBoardMainActivity::class.java)
                startActivity(intent)
                finish()
            }
        ,4500
        )
    }
}

