package com.aTeam.beasy.View.HomeFeature.Profile.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileData
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSetting
import com.aTeam.beasy.Data.Model.Profile.ResponseProfileSettingEdit
import com.aTeam.beasy.Data.Model.User.ResponseUserBalance
import com.aTeam.beasy.Data.Repository.Profile.ProfileMainRepository
import com.aTeam.beasy.Data.Repository.User.UserMainRepository

class ProfileMainViewModel : ViewModel() {
    val profileMainRepository = ProfileMainRepository()

    val responseProfile = MutableLiveData<ResponseProfileData>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getUserMainProfile(
        header : String
    ){
        isLoading.value = true
        profileMainRepository.getUserProfile(
            tokenUser = header,
            {
                isLoading.value = false
                responseProfile.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseProfileSetting = MutableLiveData<ResponseProfileSetting>()
    fun getUserProfileSetting(
        header : String
    ){
        isLoading.value = true
        profileMainRepository.getUserSetting(
            tokenUser = header,
            {
                isLoading.value = false
                responseProfileSetting.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseProfileSettingEdit = MutableLiveData<ResponseProfileSettingEdit>()
    fun putUserProfileSetting(
        header : String,
        email: String,
        pin : Int
    ){
        isLoading.value = true
        profileMainRepository.putUserSetting(
            email = email,
            pin = pin,
            tokenUser = header,
            {
                isLoading.value = false
                responseProfileSettingEdit.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }
}