package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletContact
import com.aTeam.beasy.Data.Repository.Ewallet.WalletContactRepository

class WalletAddGetContactByIdViewModel : ViewModel() {

    val walletContactRepository = WalletContactRepository()

    val responseData = MutableLiveData<ResponseEwalletContact>()
    val errorData = MutableLiveData<Throwable>()

    fun getDataAccountById(header:String, id : String){
        walletContactRepository.getAccountById(
            tokenUser = header,
            idUser = id,
            {
                responseData.value = it
            },
            {
                errorData.value = it
            }
        )
    }
}