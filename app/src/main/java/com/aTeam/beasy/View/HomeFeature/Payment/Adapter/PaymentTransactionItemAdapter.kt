package com.aTeam.beasy.View.HomeFeature.Transfer.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.aTeam.beasy.Data.Model.Payment.DataItemLastTransaction
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import kotlinx.android.synthetic.main.payment_transaction_item_list.view.*
import kotlinx.android.synthetic.main.transfer_contact_item.view.*

class PaymentTransactionItemAdapter(
    private var fragmentManager: FragmentManager
    )
    : RecyclerView.Adapter<PaymentTransactionItemAdapter.ViewHolder>()
{
    var dataPaymentTransaction : ArrayList<DataItemLastTransaction> = arrayListOf()
    private var functionResource : FunctionResource = FunctionResource()
    fun setData (dataPaymentTransaction : ArrayList<DataItemLastTransaction>){
        this.dataPaymentTransaction = dataPaymentTransaction
        notifyDataSetChanged()
    }
    inner class ViewHolder (view : View) : RecyclerView.ViewHolder(view) {
        var imagePayment = itemView.ivPaymentTransactionItemAvatar
        var titlePayment = itemView.tvPaymentTransactionItemTitle
        var descriptionPayment = itemView.tvPaymentTransactionItemDescription
        var datePayment = itemView.tvPaymentTransactionItemDate
        var totalPayment = itemView.tvPaymentTransactionItemTotal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.payment_transaction_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataPaymentTransactionBind = this.dataPaymentTransaction.get(position)
        when (dataPaymentTransactionBind.transactionType) {
            "Merchant" -> {
                holder.imagePayment.setImageResource(R.drawable.payment_merchants_icon_menu)
            }
            "Mobile" -> {
                holder.imagePayment.setImageResource(R.drawable.payment_mobile_icon_menu)
            }
            "Credit Card" -> {
                holder.imagePayment.setImageResource(R.drawable.payment_creditcard_icon_menu)
            }
        }

        holder.titlePayment.setText(dataPaymentTransactionBind.transactionType)
        holder.datePayment.setText(dataPaymentTransactionBind.on)
        holder.totalPayment.text =
            functionResource.rupiah(dataPaymentTransactionBind.amount?.toInt()?.toDouble()!!)
                .toString()

        if (dataPaymentTransactionBind.description == ""
            || dataPaymentTransactionBind.description.isNullOrBlank()
            || dataPaymentTransactionBind.description.isNullOrEmpty()
        ) {
            holder.descriptionPayment.visibility = View.GONE
        } else {
            holder.descriptionPayment.setText(dataPaymentTransactionBind.description)
        }
    }

    override fun getItemCount(): Int {
        if (dataPaymentTransaction.size < 5) {
            return dataPaymentTransaction.size
        }else {
            return 5
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 3){
            return 1
        }
        return super.getItemViewType(position)
    }

}