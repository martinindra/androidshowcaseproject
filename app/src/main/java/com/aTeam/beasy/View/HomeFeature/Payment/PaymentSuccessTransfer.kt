package com.aTeam.beasy.View.HomeFeature.Payment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import kotlinx.android.synthetic.main.payment_mobile_success_transaction_activity.*

class PaymentSuccessTransfer : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_mobile_success_transaction_activity)

        btnPaymentDone.setOnClickListener {
            val intent = Intent(this, MainBaseActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}