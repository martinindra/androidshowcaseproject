package com.aTeam.beasy.View.HomeFeature.Transfer

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aTeam.beasy.Data.Model.Transfer.DataItemContact
import com.aTeam.beasy.R
import com.aTeam.beasy.Resource.FunctionResource
import kotlinx.android.synthetic.main.transfer_confirmation_page_activity.*
import kotlinx.android.synthetic.main.transfer_page_activity.*
import javax.inject.Inject

class TransferPageActivity : AppCompatActivity() {

    //Inject Function Resource
    @Inject
    lateinit var functionResource : FunctionResource

    companion object{
        const val EXTRA_DATA = "extra_data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transfer_page_activity)
        supportActionBar?.hide()

        val window : Window = this@TransferPageActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@TransferPageActivity, R.color.status_bar_blue)

        functionResource = FunctionResource()

        val transferReference = intent.getParcelableExtra<DataItemContact>(EXTRA_DATA) as DataItemContact
        avTransferPageAvatar.setText(transferReference.name.toString())
        tvTransferPageName.text = transferReference.name.toString()
        tvTransferPageRekening.text = transferReference.bankName.toString() + " " + transferReference.accountNumber

        tvTransferAdminFee.text = transferReference.cost


        btnTransferPageNextButton.setOnClickListener {
            nextTransferConfirmation(transferReference)
        }

        backTransferPage.setOnClickListener {
            onBackPressed()
        }
    }

    private fun nextTransferConfirmation(transferReference: DataItemContact) {
        var isEmptyField = false
        var note : String? = null
        if (edTransferPageAmount.text!!.isEmpty() == true){
            isEmptyField = true
            edTransferPageAmount.setBackgroundResource(R.drawable.bg_form_wrong)
            tvMessageEdTransferPageAmount.text = "Please fill this form!"
            tvMessageEdTransferPageAmount.setTextColor(Color.parseColor("#FF0000"))
        }

        else if(edTransferPageAmount.text.toString().toInt() < 10000){
            isEmptyField = true
            edTransferPageAmount.setBackgroundResource(R.drawable.bg_form_wrong)
            tvMessageEdTransferPageAmount.setTextColor(Color.parseColor("#FF0000"))
        }

        note = if (edTransferPageNote.text.toString().isEmpty()){
            "-"
        }else{
            edTransferPageNote.text.toString()
        }

        if (!isEmptyField){
            val moveConfirmationIntent = Intent(this@TransferPageActivity, TransferConfirmationPageActivity::class.java)
            moveConfirmationIntent.putExtra(TransferConfirmationPageActivity.CONTACT_DATA, transferReference)
            moveConfirmationIntent.putExtra(TransferConfirmationPageActivity.TRANSFER_AMOUNT, edTransferPageAmount.text.toString())
            moveConfirmationIntent.putExtra(TransferConfirmationPageActivity.TRANSFER_NOTE, note.toString())
            startActivity(moveConfirmationIntent)
        }
    }

}