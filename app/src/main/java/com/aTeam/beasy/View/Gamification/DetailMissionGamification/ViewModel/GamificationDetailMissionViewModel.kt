package com.aTeam.beasy.View.Gamification.DetailMissionGamification.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationClaimReward
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationDetailPlanet
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationDetailRewardPlanet
import com.aTeam.beasy.Data.Repository.Gamification.GamificationMainRepository

class GamificationDetailMissionViewModel : ViewModel(){

    val gamificationMainRepository = GamificationMainRepository()

    val responseGamificationDetailPlanet = MutableLiveData<ResponseGamificationDetailPlanet>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getGamificationDetailPlanet(
        header : String,
        idPlanet : String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationDetailPlanet(
            tokenUser = header,
            idPlanet = idPlanet,
            {
                isLoading.value = false
                responseGamificationDetailPlanet.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }


    val responseGamificationDetailRewardPlanet = MutableLiveData<ResponseGamificationDetailRewardPlanet>()
    fun getGamificationDetailRewardPlanet(
        header: String,
        idReward: String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationDetailRewardPlanet(
            tokenUser = header,
            idReward = idReward,
            {
                isLoading.value = false
                responseGamificationDetailRewardPlanet.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseDataGamificationClaimReward = MutableLiveData<ResponseGamificationClaimReward>()
    fun postGamificationClaimReward(
        header: String,
        idReward: String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationPostClaimReward(
            tokenUser = header,
            rewardId = idReward,
            {
                isLoading.value = false
                responseDataGamificationClaimReward.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }
}