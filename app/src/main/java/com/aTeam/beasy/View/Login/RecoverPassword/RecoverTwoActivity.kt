package com.aTeam.beasy.View.Login.RecoverPassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Base.MainBaseActivity
import kotlinx.android.synthetic.main.login_after_recover_password_activity.*
import kotlinx.android.synthetic.main.login_dialog_recover_done.view.*
import kotlinx.android.synthetic.main.login_dialog_recover_password.view.*
import kotlinx.android.synthetic.main.login_recover_password_activity.*

class RecoverTwoActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var btnFinish : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_after_recover_password_activity)

        btnFinish = findViewById(R.id.finishBtn)
        btnFinish.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.finishBtn -> {
                    //Error handling
                    var finishAccount = false
                    val accountNumberOne = etPassword2.text.toString()
                    val accountNumberTwo = etPassword3.text.toString()

                    if (accountNumberOne.isEmpty()){
                        finishAccount = true
                        etEmailRecover.setBackgroundResource(R.drawable.bg_form_wrong)
                        tvMessageForgot.text = "Please fill the form"
                    }

                    if (accountNumberTwo.isEmpty()){
                        finishAccount = true
                        etAccountNumber.setBackgroundResource(R.drawable.bg_form_wrong)
                        tvMessageForgot.text = "Please fill the form"
                    }

                    //To show dialog box
                    val view =
                        View.inflate(this@RecoverTwoActivity, R.layout.login_dialog_recover_done, null)

                    val builder = AlertDialog.Builder(this@RecoverTwoActivity)
                    builder.setView(view)

                    val dialog = builder.create()
                    dialog.show()
                    dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                    val recoverPasswordDone =
                        Intent(this@RecoverTwoActivity, MainBaseActivity::class.java)
                    view.doneBtn.setOnClickListener {
                        startActivity(recoverPasswordDone)
                    }
                }
            }
        }
    }
}