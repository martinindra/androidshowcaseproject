package com.aTeam.beasy.View.HomeFeature.Pocket

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.HomeFeature.Pocket.ViewModel.PocketMainViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.login_dialog_recover_done.view.*
import kotlinx.android.synthetic.main.pocket_add_activity.*
import kotlinx.android.synthetic.main.pocket_add_activity.view.*
import kotlinx.android.synthetic.main.pocket_upload_image_photo_dialog.view.*
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class PocketAddActivity : AppCompatActivity(), View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private lateinit var btnBackAdd : ImageButton
    private lateinit var uploadImage: ImageView
    private var uploadImageUri: Uri? = null
    private lateinit var uploadPhoto: TextView
    private lateinit var pocketName: EditText
    private lateinit var dueDate: TextView
    private lateinit var amountTarget: EditText
    private lateinit var buttonAddPocket: Button


    private lateinit var sessionManager: SessionManager
    private lateinit var pocketMainViewModel : PocketMainViewModel

    companion object {
        internal const val EXTRA_FILE_PATH = "extra.file_path"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pocket_add_activity)

        sessionManager = SessionManager(this)
        pocketMainViewModel = ViewModelProviders.of(this).get(PocketMainViewModel::class.java)
        //Button Back//
        btnBackAdd = findViewById(R.id.ibArrowAdd)
        btnBackAdd.setOnClickListener(this)
        //Intent From Button Pocket Add Activity to Pocket Planning Activity//
        buttonAddPocket = findViewById(R.id.btnAddPocket)
        buttonAddPocket.setOnClickListener(this)

        //Upload Image//
        uploadImage = findViewById(R.id.ivCameraOrImagePocketMain)
        uploadImage.setOnClickListener(this)
        uploadPhoto = findViewById(R.id.tvCameraOrImagePocketMain)
        uploadPhoto.setOnClickListener(this)

        //Form//
        pocketName = findViewById(R.id.etPocketName)
        dueDate = findViewById(R.id.etDueDate)
        amountTarget = findViewById(R.id.etAmountTarget)

        //Date Picker//
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        val dpd = DatePickerDialog(this, this, year, month, day)

        dpd.datePicker.minDate = Calendar.getInstance().timeInMillis

        etDueDate.setOnClickListener{
            dpd.show()
        }

    }

    override fun onClick(v: View?) {

        when(v?.id){
            //Button Back//
            R.id.ibArrowAdd -> {
                onBackPressed()
            }

            //Upload Image With Image View//
            R.id.ivCameraOrImagePocketMain -> {
                pickImageGallery()
            }

            //Upload Image With Text View//
            R.id.tvCameraOrImagePocketMain -> {
               pickImageGallery()
            }

            //Add Pocket Button//
            R.id.btnAddPocket -> {
                var errorUpload = false
                if (pocketName.text.toString().trim() == "" || pocketName.text.toString().trim().isEmpty()){
                    tvMessageEdPocketAddActivity.text = "Please fill Pocket Name!"
                    tvMessageEdPocketAddActivity.setTextColor(Color.parseColor("#FF0000"))
                    etPocketName.setBackgroundResource(R.drawable.bg_form_wrong)
                    errorUpload = true
                }

                if (dueDate.text.toString().trim() == "" || dueDate.text.toString().trim().isEmpty()){
                    tvMessageEdPocketDate.text = "Please set the date!"
                    dueDate.setBackgroundResource(R.drawable.bg_form_wrong)
                    errorUpload = true
                }
                if (amountTarget.text.toString().trim() == "" || amountTarget.text.toString().trim().isEmpty()){
                    tvMessageEdPocketAmountTarget.text = "Please fill your Amount Target!"
                    tvMessageEdPocketAmountTarget.setTextColor(Color.parseColor("#FF0000"))
                    etAmountTarget.setBackgroundResource(R.drawable.bg_form_wrong)
                    errorUpload = true
                }

                if (uploadImageUri == null){
                    Toast.makeText(this, "Please choose image for your pocket!", Toast.LENGTH_SHORT).show()
                    errorUpload = true
                }
                if (!errorUpload){
                    val imageUri = uploadImageUri?.path
                    Log.d("imageUri", imageUri.toString())
                    val name = pocketName.text.toString().trim()
                    val dueDate = dueDate.text.toString().trim()
                    val amount = amountTarget.text.toString().trim()

                    sessionManager.login?.let {
                        pocketMainViewModel.pocketCreate(
                            header = it,
                            name = name,
                            dueDate = dueDate,
                            picture = File(imageUri),
                            target = amount.toInt()
                        )
                    }

                    responseProcess()
                }

            }
        }
    }

    private fun responseProcess() {
        pocketMainViewModel.responsePocketCreate.observe(this, androidx.lifecycle.Observer{
            Toast.makeText(this, "Success to add pocket", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, PocketPlanningActivity::class.java)
            intent.putExtra(PocketPlanningActivity.POCKETID, it.data?.id)
            intent.putExtra(PocketPlanningActivity.POCKETNAME, it.data?.pocketName)
            startActivity(intent)
            finish()
        })

        pocketMainViewModel.responseErrorData.observe(this, androidx.lifecycle.Observer {
            Toast.makeText(this, "Failed to add pocket", Toast.LENGTH_SHORT).show()
        })

        pocketMainViewModel.isLoading.observe(this, androidx.lifecycle.Observer {
            if (it == true){
                pocketAddMainLoading.visibility = View.VISIBLE
            }else{
                pocketAddMainLoading.visibility = View.GONE
            }
        })
    }

    //Image Picker
    private fun pickImageGallery (){
        val view =
            View.inflate(this@PocketAddActivity, R.layout.pocket_upload_image_photo_dialog, null)

        val builder = AlertDialog.Builder(this@PocketAddActivity)
        builder.setView(view)

        val dialog = builder.create()
        dialog.show()
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        //If click Yes
        view.btnUploadImageDialogYes.setOnClickListener {
            ImagePicker.with(this)
                .galleryOnly()
                .cropSquare()
                .galleryMimeTypes(mimeTypes = arrayOf("image/png", "image/jpg", "image/jpeg"))
                .maxResultSize(1080, 1080)
                .saveDir(getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!) //For Save the picture
                .start()
            dialog.dismiss()
        }

        //If click No
        view.btnUploadImageDialogNo.setOnClickListener {
            dialog.dismiss()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uploadImageUri = data?.data!!

//            uploadImageBitmap = intent.extras?.get("dat") as Bitmap

            // Use Uri object instead of File to avoid storage permissions
            ivCameraOrImagePocketMain?.setImageURI(uploadImageUri)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    //End of Image Picker//

    //Date Picker
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar[year, month] = dayOfMonth
        val sdf = SimpleDateFormat("dd MMMM yyyy", Locale("en"))
        val formattedDate = sdf.format(calendar.time)
        etDueDate.text = formattedDate
    }

    private suspend fun getBitmap(imageUri: String?) : Bitmap{
        val loading : ImageLoader = ImageLoader(this)
        val request : ImageRequest = ImageRequest.Builder(this)
            .data(File(imageUri))
            .build()

        val result : Drawable = (loading.execute(request) as SuccessResult).drawable
        return (result as BitmapDrawable).bitmap
    }


}