package com.aTeam.beasy.View.Gamification.DetailRewardGamification.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aTeam.beasy.Data.Model.Gamification.PlanetStatusDataItem
import com.aTeam.beasy.Data.Session.SessionManager
import com.aTeam.beasy.R
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.Adapter.GamificationDetailRewardMissionItemAdapter
import com.aTeam.beasy.View.Gamification.DetailRewardGamification.ViewModel.GamificationDetailRewardViewModel
import kotlinx.android.synthetic.main.gamification_detail_reward_mission_next_mission_fragment.*

class GamificationDetailRewardMissionNextMission : Fragment() {

    //token
    private lateinit var sessionManager: SessionManager

    //view model
    private lateinit var gamificationDetailRewardViewModel: GamificationDetailRewardViewModel

    private lateinit var dataGamication : ArrayList<PlanetStatusDataItem>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.gamification_detail_reward_mission_next_mission_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(requireContext())
        gamificationDetailRewardViewModel = ViewModelProviders.of(this).get(GamificationDetailRewardViewModel::class.java)

        gamificationProses()


    }

    private fun gamificationProses() {
        sessionManager.login?.let {
            gamificationDetailRewardViewModel.getGamificationDetailPlanet(
                header = it,
                queryStatus = "NEXT"
            )
        }

        responseProses()
    }

    private fun responseProses() {
        gamificationDetailRewardViewModel.responseGamificationListPlanetListStatus.observe(viewLifecycleOwner, Observer {
            bindData(it.data)

        })
        gamificationDetailRewardViewModel.responseErrorData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), "Failed to get Data", Toast.LENGTH_SHORT).show()
            gamificationListNextPlanetErrorFound.visibility = View.VISIBLE
        })

        gamificationDetailRewardViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it == true){
                gamificationListNextPlanetLoading.visibility = View.VISIBLE
            }else{
                gamificationListNextPlanetLoading.visibility = View.GONE
            }
        })
    }

    private fun bindData(data: List<PlanetStatusDataItem?>?) {
        dataGamication = arrayListOf()
        dataGamication = data as ArrayList<PlanetStatusDataItem>

        if (dataGamication.isNotEmpty()) {
            val dataPlanet = dataGamication
            val adapterGamifikasi = GamificationDetailRewardMissionItemAdapter(1)
            adapterGamifikasi.setData(dataGamification = dataGamication)
            rvGamificationDetailRewardMissionNextMission.apply {
                adapter = adapterGamifikasi
                setHasFixedSize(true)
            }
        }
        if (dataGamication.isEmpty()){
            llGamificationNextMission.visibility = View.VISIBLE
        }
    }
}