package com.aTeam.beasy.View.Gamification.MainGamification.Adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aTeam.beasy.View.Gamification.MainGamification.Fragment.GamificationMainFirstPathFragment
import com.aTeam.beasy.View.Gamification.MainGamification.Fragment.GamificationMainSecondPathFragment
import com.aTeam.beasy.View.Gamification.MainGamification.Fragment.GamificationMainThirdPathFragment

internal class GamificationMainViewPagerAdapter (fm : FragmentManager?, val planetSequences : Int) :
    FragmentPagerAdapter(fm!!)
{

    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                GamificationMainFirstPathFragment (planetSequence = planetSequences)
            }
            1 -> GamificationMainSecondPathFragment(planetSequence = planetSequences)
            2 -> GamificationMainThirdPathFragment(planetSequence = planetSequences)
            else -> GamificationMainFirstPathFragment(planetSequence = planetSequences)
        }
    }
    fun addFragment(fragment:Fragment){

    }
}