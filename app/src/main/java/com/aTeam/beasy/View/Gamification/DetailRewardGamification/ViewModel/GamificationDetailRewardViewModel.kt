package com.aTeam.beasy.View.Gamification.DetailRewardGamification.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationDetailPlanet
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationMyRewardList
import com.aTeam.beasy.Data.Model.Gamification.ResponseGamificationPlanetListStatus
import com.aTeam.beasy.Data.Repository.Gamification.GamificationMainRepository
import com.google.gson.reflect.TypeToken

class GamificationDetailRewardViewModel : ViewModel() {

    val gamificationMainRepository = GamificationMainRepository()

    val responseGamificationListPlanetListStatus = MutableLiveData<ResponseGamificationPlanetListStatus>()
    val responseErrorData = MutableLiveData<Throwable>()

    val isLoading = MutableLiveData<Boolean>()

    fun getGamificationDetailPlanet(
        header : String,
        queryStatus : String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationGetListPlanet(
            tokenUser = header,
            queryStatus = queryStatus,
            {
                isLoading.value = false
                responseGamificationListPlanetListStatus.value = it
            },{
                isLoading.value = false
                responseErrorData.value = it
            }
        )
    }

    val responseGamificationMyRewardList = MutableLiveData<ResponseGamificationMyRewardList>()
    fun getGamificationMyRewardList(
        header: String
    ){
        isLoading.value = true

        gamificationMainRepository.gamificationMyRewardList(
            tokenUser = header,
            { responseRewardList ->
                isLoading.value = false
                responseGamificationMyRewardList.value = responseRewardList
            },{ error ->
                isLoading.value = false
                responseErrorData.value = error
            }
        )
    }

}