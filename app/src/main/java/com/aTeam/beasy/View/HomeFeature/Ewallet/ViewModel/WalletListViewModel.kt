package com.aTeam.beasy.View.HomeFeature.Ewallet.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Ewallet.ResponseEwalletList
import com.aTeam.beasy.Data.Repository.Ewallet.WalletListRepository

class WalletListViewModel : ViewModel() {
    val walletListRepository = WalletListRepository()

    val responseData = MutableLiveData<ResponseEwalletList>()
    val errorData = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun getWalletData(header:String){
        isLoading.value = true
        walletListRepository.getWalletList(header,
            {
                responseData.value = it
                isLoading.value = false
            },
            {
                errorData.value = it
                isLoading.value = false
            }
        )
    }
}