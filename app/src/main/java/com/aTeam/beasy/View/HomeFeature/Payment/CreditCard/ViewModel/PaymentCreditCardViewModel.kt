package com.aTeam.beasy.View.HomeFeature.Payment.CreditCard.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBill
import com.aTeam.beasy.Data.Model.Payment.CreditCard.ResponsePaymentCreditCardBillPay
import com.aTeam.beasy.Data.Repository.Payment.PaymentCreditCardRepository

class PaymentCreditCardViewModel : ViewModel() {
    val creditCardBillRepository : PaymentCreditCardRepository = PaymentCreditCardRepository()

    val responseData = MutableLiveData<ResponsePaymentCreditCardBill>()
    val responseError = MutableLiveData<Throwable>()
    val isLoading = MutableLiveData<Boolean>()

    fun getPaymentCreditCardBill(header : String, ccNumber : String){
        isLoading.value = true

        creditCardBillRepository.creditCardGetBill(tokenUser = header, ccNumber = ccNumber,
            {
                isLoading.value = false
                responseData.value = it
            },
            {
                isLoading.value = false
                responseError.value = it
            }
        )
    }

    val responsePayData = MutableLiveData<ResponsePaymentCreditCardBillPay>()
    fun payPaymentCreditCardBill(header: String, ccNumber: String, amount : Int, pin : Int)
    {
        isLoading.value = true

        creditCardBillRepository.creditCardPayBill(tokenUser = header, creditCardNumber = ccNumber, amount = amount, pin = pin,
            {
                isLoading.value = false
                responsePayData.value = it
            }, {
                isLoading.value = false
                responseError.value = it
            }
        )

    }
}