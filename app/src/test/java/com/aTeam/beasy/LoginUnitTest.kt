package com.aTeam.beasy

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.aTeam.beasy.Util.ValidateResult
import com.aTeam.beasy.View.Login.ViewModel.LoginMainViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class LoginUnitTest {

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    lateinit var loginViewModel : LoginMainViewModel

    @Mock
    lateinit var observer : Observer<ValidateResult>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        loginViewModel = LoginMainViewModel()
        loginViewModel.resultValidate.observeForever(observer)
    }

    @Test
    fun `given empty string email and password when call validate then display invalid`(){
        //given
        val email = ""
        val password = ""
        //when
        loginViewModel.loginViewModel(email,password)
        //then
        verify(observer).onChanged(
            ValidateResult.invalid(0,"Email & Password required"))
    }

    @Test
    fun `given empty string email when call validate then display invalid`(){
        //given
        val email = ""
        val password = "123456"

        //when
        loginViewModel.loginViewModel(email, password)

        //then
        verify(observer).onChanged(
            ValidateResult.invalid(1, "Email required")
        )
    }

    @Test
    fun `given empty string password when call validate then display invalid`(){
        //given
        val email = "beasytester@gmail.com"
        val password = ""

        //when
        loginViewModel.loginViewModel(email, password)

        //then
        verify(observer).onChanged(
            ValidateResult.invalid(2, "Password required")
        )
    }

}