#Dependency that used in this project
`
dependencies {

    [...........................................................]

     //network

    //retrofit
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'

    //rx
    implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'
    implementation 'io.reactivex.rxjava3:rxjava:3.0.0'

    //retrofit rx java3
    implementation 'com.squareup.retrofit2:adapter-rxjava3:2.9.0'

    //okhttp
    implementation "com.squareup.okhttp3:logging-interceptor:4.9.1"

    
    // end network dependency


    // UI library

    //recyclerview
    implementation "androidx.recyclerview:recyclerview:1.2.0"

    //material design
    implementation "com.google.android.material:material:1.3.0"

    //glide
    implementation 'com.github.bumptech.glide:glide:4.12.0'

    //acronym avatar
    implementation 'com.redmadrobot:acronym-avatar:2.0'

    //pager indicator
    implementation 'com.tbuonomo:dotsindicator:4.2'

    //pinView
    implementation 'io.github.chaosleung:pinview:1.4.4'

    //spin kit progress bar
    implementation 'com.github.ybq:Android-SpinKit:1.4.0'

    //lottie
    implementation "com.airbnb.android:lottie:3.7.0"


    // end of UI Library


    //pattern or project structure

    //lifecycle
    implementation "androidx.lifecycle:lifecycle-extensions:2.2.0"

    //end of pattern

    //Image Picker
    implementation "com.github.dhaval2404:imagepicker:2.1"

}


`

